/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <cmath>
#include <tinyxml.h>
#include "Configuration/ConfReader.hpp" //Hikou no mizu's ConfReader, see ../src

#define FRAMES_GAP 4 //The number of horizontal/vertical pixels between each frame.

/// Struct to contain a <animation> tag, and its associated <hits> tag if any
struct AnimNode
{
    AnimNode() : anim(NULL), hits(NULL)
    {
        
    }
    
    AnimNode(TiXmlElement *animElement, TiXmlElement *hitsElement) :
    anim(animElement), hits(hitsElement)
    {
        
    }
    
    TiXmlElement *anim, *hits;
};

std::vector<AnimNode> parseXML(TiXmlDocument&, const std::string&);
void preprocessAnimNode(TiXmlElement*);
void updateAnimNode(TiXmlElement*, int, int, int, int);
void updateHitsNode(TiXmlElement*, int, int);
std::string roundAsString(float number);

///descbuild: Converts a single abstract character animation description tag
///into a game-readable format (absolute values for frame positions and sizes)
int main(int argc, char **argv)
{
    if (argc <= 2)
    {
        std::cerr << "Usage: " << argv[0] << " [animation_metadata_file] [dest_xml_description_file]" << std::endl;
        return 1;
    }
    
    std::string animInfoPath(argv[1]);
    std::string xmlDescriptionPath(argv[2]);
    
    //Read the metadata of the generated animation.
    ConfReader animInfoReader;
    if (!animInfoReader.load(animInfoPath))
    {
        std::cerr << "Error while loading the file: " << animInfoPath << std::endl;
        return 1;
    }
    
    std::string fullAnimName;
    int framesAmount, frameWidth, frameHeight, tiles;
    
    bool readingSuccess = animInfoReader.getItem("animation_name", fullAnimName)
                       && animInfoReader.getIntItem("frames_amount", framesAmount)
                       && animInfoReader.getIntItem("frame_width", frameWidth)
                       && animInfoReader.getIntItem("frame_height", frameHeight)
                       && animInfoReader.getIntItem("tiles_per_column", tiles);
    if (!readingSuccess)
    {
        std::cerr << "Error while parsing the file: " << animInfoPath << std::endl;
        return 1;
    }
    else if (framesAmount <= 0 || frameWidth <= 0 || frameHeight <= 0 || tiles <= 0)
    {
        std::cerr << "Malformed file, at least one value is not strictly positive: " << animInfoPath << std::endl;
        return 1;
    }
    
    //Extract character and animation name (without the .png extension).
    std::string::size_type delimiterPos = fullAnimName.find('/');
    if (delimiterPos == std::string::npos)
    {
        std::cerr << "Error while parsing the animation name: " << fullAnimName << std::endl;
        return 1;
    }
    
    std::string animName = fullAnimName.substr(delimiterPos+1, fullAnimName.length()-delimiterPos-5);
    
    //Open the xml animations' description file for filling in.
    TiXmlDocument xmlFile;
    if (!xmlFile.LoadFile(xmlDescriptionPath.c_str()))
    {
        std::cerr << "Error while loading the file: " << xmlDescriptionPath << std::endl;
        return 1;
    }
    
    //Look for the matching <animation> tag.
    std::vector<AnimNode> animNodes = parseXML(xmlFile, animName);
    if (animNodes.empty())
    {
        std::cerr << "Could not find an animation with name: " << animName << std::endl;
        return 1;
    }
    
    //Update the tags.
    std::vector<AnimNode>::iterator it;
    for (it = animNodes.begin(); it != animNodes.end(); it++)
    {
        preprocessAnimNode(it->anim); //Unfold <frameSet> elements
        updateAnimNode(it->anim, framesAmount, frameWidth, frameHeight, tiles);
        if (it->hits != NULL)
            updateHitsNode(it->hits, frameWidth, frameHeight);
    }
    
    //Save the updated file and exit.
    xmlFile.SaveFile(xmlDescriptionPath);
    
    return 0;
}

///Parses the input 'xmlFile' looking for the description of the requested animation
std::vector<AnimNode> parseXML(TiXmlDocument &xmlFile, const std::string &animName)
{
    std::vector<AnimNode> animNodes;
        
    TiXmlHandle hdl(&xmlFile);
    hdl = hdl.FirstChildElement().FirstChildElement();
    
    TiXmlElement *xmlElement = hdl.FirstChildElement().ToElement();
    unsigned int nodeIx = 0;
    while (xmlElement)
    {
        std::string nodePathDst;
        
        //Main animation ?
        TiXmlElement *animNode = xmlElement->FirstChildElement("animation");
        if (animNode->QueryStringAttribute("path", &nodePathDst) == TIXML_SUCCESS && nodePathDst == animName)
            animNodes.push_back( AnimNode(animNode, NULL) ); //No associated <hits> tag.
        else
        {
            //Attacks ?
            TiXmlHandle attacksHdl = hdl.Child(nodeIx).FirstChildElement("attacks");
            TiXmlElement *xmlAttacks = attacksHdl.FirstChildElement().ToElement();
            while (xmlAttacks)
            {
                TiXmlElement *attackNode = xmlAttacks->FirstChildElement("animation"); //Attack animation
                TiXmlElement *hitsNode = xmlAttacks->FirstChildElement("hits"); //Attack hits
                
                if (attackNode->QueryStringAttribute("path", &nodePathDst) == TIXML_SUCCESS && nodePathDst == animName)
                    animNodes.push_back( AnimNode(attackNode, hitsNode) );
                
                xmlAttacks = xmlAttacks->NextSiblingElement();
            }
        }
        
        nodeIx++;
        xmlElement = xmlElement->NextSiblingElement();
    }
    
    return animNodes;
}

///Preprocesses a <animation> tag to unfold the <frameSet> children into sets of <frame> children
void preprocessAnimNode(TiXmlElement *xmlElement)
{
    TiXmlElement *frameSetElement = xmlElement->FirstChildElement("frameSet");
    while (frameSetElement)
    {
        //Extract the frame range of the <frameSet> element
        std::string rangeStr;
        frameSetElement->QueryStringAttribute("id_range", &rangeStr);
        
        std::string relBodyX , relBodyY, relBodyWidth, relBodyHeight;
        frameSetElement->QueryStringAttribute("bodyX_rel", &relBodyX);
        frameSetElement->QueryStringAttribute("bodyY_rel", &relBodyY);
        frameSetElement->QueryStringAttribute("bodyWidth_rel", &relBodyWidth);
        frameSetElement->QueryStringAttribute("bodyHeight_rel", &relBodyHeight);
        
        std::string::size_type delimiterPos = rangeStr.find(':');
        if (delimiterPos != std::string::npos)
        {
            int startFrame, endFrame;
            std::istringstream(rangeStr.substr(0, delimiterPos)) >> startFrame;
            std::istringstream(rangeStr.substr(delimiterPos+1, rangeStr.length()-delimiterPos-1)) >> endFrame;
            
            //Unfold the <frameSet> element
            for (int frameId = ((startFrame <= endFrame) ? startFrame : endFrame);
                frameId <= ((startFrame <= endFrame) ? endFrame : startFrame);
                frameId++)
            {
                //Append the <frame> element
                TiXmlElement frame("frame");
                frame.SetAttribute("id", frameId);
                frame.SetAttribute("bodyX_rel", relBodyX);
                frame.SetAttribute("bodyY_rel", relBodyY);
                frame.SetAttribute("bodyWidth_rel", relBodyWidth);
                frame.SetAttribute("bodyHeight_rel", relBodyHeight);
                
                //Insert before or after the <frameSet> element depending on the wanted order
                if (startFrame <= endFrame) xmlElement->InsertBeforeChild(frameSetElement, frame);
                else xmlElement->InsertAfterChild(frameSetElement, frame);
            }
        }
        
        //Remove the <frameSet> element and select next one if any
        xmlElement->RemoveChild(frameSetElement);
        frameSetElement = xmlElement->FirstChildElement("frameSet");
    }
}

///Updates a <animation> tag from relative values to absolute, game-readable values
void updateAnimNode(TiXmlElement *xmlElement, int framesAmount, int frameWidth, int frameHeight, int tiles)
{
    //Compute the position of each frame in the spritesheet.
    std::vector< std::pair<int, int> > frames;
    for (int i = 0; i < framesAmount; i++)
    {
        int colIx = i%tiles, rowIx = static_cast<int>(i/tiles); // tiles > 0
        int frameX = colIx * (frameWidth + FRAMES_GAP), frameY = rowIx * (frameHeight + FRAMES_GAP);
        
        frames.push_back(std::make_pair(frameX, frameY));
    }
    
    TiXmlElement *frameElement = xmlElement->FirstChildElement("frame");
    while (frameElement)
    {
        //Query abstract frame description attributes
        int frameId = -1;
        frameElement->QueryIntAttribute("id", &frameId);
        
        float relBodyX = 0.f, relBodyY = 0.f, relBodyWidth = 1.f, relBodyHeight = 1.f;
        frameElement->QueryFloatAttribute("bodyX_rel", &relBodyX);
        frameElement->QueryFloatAttribute("bodyY_rel", &relBodyY);
        frameElement->QueryFloatAttribute("bodyWidth_rel", &relBodyWidth);
        frameElement->QueryFloatAttribute("bodyHeight_rel", &relBodyHeight);
        
        //Add static frame data
        if (frameId >= 0 && frameId < framesAmount)
        {
            //Frame absolute position and size
            frameElement->SetAttribute("x", frames[frameId].first);
            frameElement->SetAttribute("y", frames[frameId].second);
            frameElement->SetAttribute("width", frameWidth);
            frameElement->SetAttribute("height", frameHeight);
            
            //Absolute values for body x, y, width and height
            frameElement->SetAttribute("bodyX", roundAsString(relBodyX * frameWidth));
            frameElement->SetAttribute("bodyY", roundAsString(relBodyY * frameHeight));
            frameElement->SetAttribute("bodyWidth", roundAsString(relBodyWidth * frameWidth));
            frameElement->SetAttribute("bodyHeight", roundAsString(relBodyHeight * frameHeight));
            
            //Remove abstract frame description attributes
            frameElement->RemoveAttribute("id");
            frameElement->RemoveAttribute("bodyX_rel");
            frameElement->RemoveAttribute("bodyY_rel");
            frameElement->RemoveAttribute("bodyWidth_rel");
            frameElement->RemoveAttribute("bodyHeight_rel");
        }
        
        frameElement = frameElement->NextSiblingElement();
    }
}

///Updates a <hits> tag from relative values to absolute, game-readable values
void updateHitsNode(TiXmlElement *xmlElement, int frameWidth, int frameHeight)
{
    TiXmlElement *hitElement = xmlElement->FirstChildElement("hit");
    while (hitElement)
    {
        float relX = -1.f, relY = -1.f, relWidth = -1.f, relHeight = -1.f;
        hitElement->QueryFloatAttribute("x_rel", &relX);
        hitElement->QueryFloatAttribute("y_rel", &relY);
        hitElement->QueryFloatAttribute("width_rel", &relWidth);
        hitElement->QueryFloatAttribute("height_rel", &relHeight);
        
        if (relX > 0.f && relY > 0.f)
        {
            hitElement->SetAttribute("x", roundAsString(relX * frameWidth));
            hitElement->SetAttribute("y", roundAsString(relY * frameHeight));
            hitElement->RemoveAttribute("x_rel");
            hitElement->RemoveAttribute("y_rel");
            
            if (relWidth > 0.f && relHeight > 0.f)
            {
                hitElement->SetAttribute("width", roundAsString(relWidth * frameWidth));
                hitElement->SetAttribute("height", roundAsString(relHeight * frameHeight));
                hitElement->RemoveAttribute("width_rel");
                hitElement->RemoveAttribute("height_rel");
            }
        }
        
        hitElement = hitElement->NextSiblingElement();
    }
}

///Rounds 'number' to 0.1 precision and returns the result as a string
std::string roundAsString(float number)
{
    std::ostringstream ss;
    ss << roundf(number * 10.f) / 10.f;
    
    return ss.str();
}
