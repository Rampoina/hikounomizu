#!/bin/sh
# Dependencies: synfig, imagemagick

# Copyright (C) 2010-2021 Duncan Deveaux
# 
# This file is part of Hikou no mizu.
# 
# Hikou no mizu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Hikou no mizu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.


# animbuild.sh: Builds a spritesheet png file from a sifz animation.
base_srcdir="$1"    # datasrc dir
base_outputdir="$2" # generated data dir
output_name="$3"    # relative name of output spritesheet (e.g. Hikou/default.png)

workingdir="$4" # Temporary working directory
metadatadir="$5"

inputs="$6" # Input sifz animations description
pngdest="$base_outputdir$output_name" # Output png spritesheet

scale_mult="$7"
scale_div="$8"


# Compute the size of the overall spritesheet based on the frame size and amount
compute_spritesheet_size()
{
    _frame_separator=4
    _sizes="256 512 1024 2048 4096"
    
    _frame_width="$(expr "$1" + "$_frame_separator")" #Frame full width including separator (4px)
    _frame_height="$(expr "$2" + "$_frame_separator")" #Frame full height including separator (4px)
    _frames_amount="$3"
    
    for imgwidth in $_sizes
    do
        for imgheight in $_sizes
        do
            tiles=$(expr "$imgwidth" / "$_frame_width")
            _rows=$(expr "$imgheight" / "$_frame_height")
            
            if [ "$(expr "$tiles" \* "$_rows")" -ge "$_frames_amount" ]; then
                break 2
            fi
            
            # Break the loop to switch to increment image_width
            if [ "$imgheight" -ge "$imgwidth" ]; then
                break
            fi
        done
    done
}


mkdir -p "$workingdir"
mkdir -p "$(dirname $pngdest)"

# Render animation frames
i=1
input_i=$(printf '%s;' "$inputs" | cut -d\; -f1)
input_i=$(printf '%s' "$input_i" | tr -d ' \n[]}') #Trim
while ! [ -z "$input_i" ] ; do

    # Browse input sifz descriptions
    printf '%s\n' "$input_i" |
    while IFS='-' read -r sifz_file start end ; do
        mkdir "${workingdir}/tmp${i}"
        
        # Scaling of the animation ?
        if [ "$scale_mult" = 0 -o "$scale_div" = 0 ]; then # No scaling
            synfig "${base_srcdir}${sifz_file}" -t png -o "${workingdir}/tmp${i}/render.png" > /dev/null 2>&1
        else
            original_width="$(synfig "${base_srcdir}${sifz_file}" --canvas-info w 2> /dev/null | tail -n1 | cut -d '=' -f 2)"
            updated_width="$(expr "$original_width" \* "$scale_mult")"
            updated_width="$(expr "$updated_width" / "$scale_div")"
            
            synfig "${base_srcdir}${sifz_file}" -t png -w "$updated_width" -o "${workingdir}/tmp${i}/render.png" > /dev/null 2>&1
        fi
            
        # Register the needed frames
        if [ "$end" = ":" ] ; then
            # Select from 'start' to the last frame
            added_frames=$(ls ${workingdir}/tmp${i}/* | sed -n "${start}~1p")
        else
            # Select from 'start' to 'end'
            added_frames=$(ls ${workingdir}/tmp${i}/* | sed -n "${start},${end}p")
        fi
        
        printf '%s\n' "$added_frames" >> "${workingdir}/frames"
    done
    
    # Next input
    i=$(($i+1))
    input_i=$(printf '%s;' "$inputs" | cut -d\; -f$i)
    input_i=$(printf '%s' "$input_i" | tr -d ' \n[]}') #Trim
done

# Compute required image size
frames_amount=$(< "${workingdir}/frames" wc -l) #Number of frames in the animation

first_frame=$(head -1 "${workingdir}/frames") #To extract width & height (constants to all frames)
frame_width=$(identify -ping -format "%[fx:w]" "$first_frame")
frame_height=$(identify -ping -format "%[fx:h]" "$first_frame")

compute_spritesheet_size "$frame_width" "$frame_height" "$frames_amount" # Fills in $imgwidth, $imgheight and $tiles

# Print needed frames
input_frames=$(cat "${workingdir}/frames")
montage $input_frames -background none -geometry +2+2 -tile ${tiles}x "${workingdir}/tiles.png"

# Resize canvas
convert "${workingdir}/tiles.png" -background none -gravity NorthWest -chop 2x2+0+0 -extent ${imgwidth}x${imgheight} "$pngdest"

# Save animation metadata (for subsequent animations' description generation)
metadata_name=$(printf '%s' "$output_name" | tr '/' '_') #Replace '/' by '_'
metadata_name=$(printf '%s' "$metadata_name" | cut -d '.' -f 1) #Remove extension

printf 'animation_name %s\nframes_amount %d\nframe_width %d\nframe_height %d\ntiles_per_column %d\n' \
"$output_name" "$frames_amount" "$frame_width" "$frame_height" "$tiles" >> "${metadatadir}/${metadata_name}"

printf '  %-48sOK\n' "$output_name"
rm -rf "$workingdir"
