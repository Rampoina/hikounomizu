#!/bin/sh
# Dependencies: synfig, inkscape, imagemagick, xz

# Copyright (C) 2010-2021 Duncan Deveaux
# 
# This file is part of Hikou no mizu.
# 
# Hikou no mizu is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Hikou no mizu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.


datasrcdir=.            #Input data source dir
datadir=../data         #Output built data dir
tmpdir=hnm_tmp          #Temporary working dir
descbuild="./descbuild" #Path to the descbuild utility (see datasrc/descbuild.cpp)

# Clean up if the script is terminated
cleanup()
{
    printf 'Cleaning up...\n'
    rm -rf "$tmpdir"
}
trap cleanup 1 2 3 9 15


# labelprint: Print a label with fixed length
# printok: Print OK when a task is done
labelprint()
{
    printf '%-50s' "$1"
}
printok()
{
    printf "OK\n"
}

# Parse options
simultaneous_tasks=1 # Number of simultaneous tasks of animation building
conf_only=1          # Only generate configuration files

# Show help and usage message
help()
{
    printf 'Usage: %s [options]\n' "$(basename $0)"
    printf '  %-25s %s\n' "-h, --help" "Print a help message."
    printf '  %-25s %s\n' "-j [jobs], --jobs[=jobs]" "Allow jobs>=0 simultaneous tasks of animation building ; infinite number if jobs=0. Default is jobs=1."
    printf '  %-25s %s\n' "-c, --conf_only" "Rebuild only the configuration files located in datasrc/cfg/"
    printf '  %-25s %s\n' "--descbuild=file" "Specify the location of the descbuild utility (distributed with datasrc/)."
    printf '  %-25s %s\n' "--datasrc=dir" "Speficy the location of the input datasrc/ directory."
    printf '  %-25s %s\n' "--output=dir" "Specify the location of the output data/ directory."
}

# Message to show when j is malformed
j_malformed()
{
    printf 'Malformed j parameter, please set j>=0.\n'
    help
    exit 1
}

arguments=$(getopt -o hj:c -l help,jobs:,conf_only,descbuild:,datasrc:,output: -- "$@")
if [ "$?" != "0" ]; then
    help
    exit 1
fi

eval set -- "$arguments"
while :
do
    case "$1" in
        -h | --help) help ; exit 0 ; shift ;;
        
        -j | --jobs) printf '%s' "$2" | egrep -q '^[0-9]+$' && simultaneous_tasks=$2 || j_malformed ; shift 2 ;;
        
        -c | --conf_only) conf_only=0 ; shift ;;
        
        --descbuild) descbuild="$2" ; shift 2 ;;
        --datasrc)   datasrcdir="$2" ; shift 2 ;;
        --output)    datadir="$2" ; shift 2 ;;
        
        --) shift ; break ;;
    esac
done


# Welcome message
printf 'Hikou no mizu data builder\n'

if [ ! -d "$datasrcdir" ] ; then # Data source directory not found
    printf 'The source directory "%s" does not exist.\n' "$datasrcdir"
    exit 1
fi

# Create output directory
labelprint "Creating ${datadir}/..."
mkdir -p "$datadir" ; printok

# ============================== #
# = CONFIGURATION FILES, FONTS = #
# ============================== #

labelprint 'Copying configuration files and fonts...'
rm -rf "${datadir}/fonts" "${datadir}/cfg" # Clean any pre-existing content
cp -r "${datasrcdir}/fonts" "${datasrcdir}/cfg" "$datadir" ; printok

if [ "$conf_only" = "0" ] ; then
    exit 0
fi

# ================ #
# = SPRITESHEETs = #
# ================ #

# Create temporary working directory
mkdir "$tmpdir"

# Retrieve base path to sifz animation files
animationpath=$(grep ':ANIMATIONPATH=' "${datasrcdir}/buildinfo.cfg" | cut -d\= -f2)

# Clean any pre-existing content
rm -rf "${datadir}/${animationpath}"

# Create temporary metadata and animation description directories,
# as well as each character's data directory
metadata_path="${tmpdir}/anim_metadata"
description_path="${tmpdir}/anim_description"
mkdir -p "$metadata_path"
mkdir -p "$description_path"

characters_list=$(ls -d "${datasrcdir}/${animationpath}"/*/ | xargs -L 1 -d '\n' basename) # Extract characters list
for character in $characters_list
do
    # Create data directory
    mkdir -p "${datadir}/${animationpath}/${character}"
    
    # Populate temporary animation description directory
    lowercase_name=$(printf '%s' "$character" | tr [:upper:] [:lower:])
    cp "${datasrcdir}/${animationpath}/${character}/${lowercase_name}.xml" "$description_path"
done


# Browse sifz animations' list
total_animations=$(grep "^[^#:]" "${datasrcdir}/buildinfo.cfg" | wc -l) # Number of animations to render
ixanim=1 # Current animation being rendered, index

printf 'Rendering %d animations spritesheets...\n' "$total_animations"

grep "^[^#:]" "${datasrcdir}/buildinfo.cfg" |
while IFS=',' read -r rel_outputpath inputlist
do
    printf '"%s" "%s" "%s" "%s" "%d" "%d"\n' "$rel_outputpath" "${tmpdir}/anim${ixanim}" "$metadata_path" "$inputlist" 15 8 >> "${tmpdir}/commands"
    ixanim=$(($ixanim+1))
done

# Launching spritesheet rendering commands in parallel
< "${tmpdir}/commands" xargs -P $simultaneous_tasks -L 1 "${datasrcdir}/animbuild.sh" "${datasrcdir}/${animationpath}/" "${datadir}/${animationpath}/"

# Generate static game-readable animation description file
labelprint 'Building static animation description files...'

# 1. Update the description files to make them game-readable
metadatas_list=$(ls "$metadata_path")
for metadata_file in $metadatas_list
do
    character_lowercase=$(printf '%s' $metadata_file | cut -d '_' -f 1 | tr [:upper:] [:lower:])
    "${descbuild}" "${metadata_path}/${metadata_file}" "${description_path}/${character_lowercase}.xml"
done

# 2. Move the generated game-readable description files to the output data directory
for character in $characters_list
do
    lowercase_name=$(printf '%s' "$character" | tr [:upper:] [:lower:])
    cp "${description_path}/${lowercase_name}.xml" "${datadir}/${animationpath}/${character}/data.xml"
done
printok


# =============== #
# = UI GRAPHICS = #
# =============== #

# Uses inkscape to render svg files, optionally resizes the canvas
svg_render()
{
    output_path="${datadir}/${1}.png"
    if ! [ -z "$2" ] ; then
        output_path="${2}.png"
    fi
    
    inkscape --export-filename="$output_path" "${datasrcdir}/${1}.svgz" > /dev/null 2>&1
}

canvas_resize()
{
    output_path="${1}"
    if ! [ -z "$4" ] ; then
        output_path="${4}"
    fi
    
    convert "$1" -background none -gravity NorthWest -extent ${2}x${3} "$output_path"
}

# Decompresses and renders xcf files, and resizes the canvas
xcf_render()
{
    output_path="${datadir}/${1}.png"
    if ! [ -z "$4" ] ; then
        output_path="${4}.png"
    fi
    
    
    cp "${datasrcdir}/${1}.xcf.xz" "$tmpdir"
    
    filename=$(basename "$1")
    xz --decompress "${tmpdir}/${filename}.xcf.xz"
    
    convert "${tmpdir}/${filename}.xcf"[1] -background none -gravity NorthWest -extent ${2}x${3} "$output_path"
}

# Clean any pre-existing content
rm -rf "${datadir}/gfx/ui" "${datadir}/gfx/icon.png" "${datadir}/gfx/characters/reduced.png"

# Generate UI graphics
labelprint 'Generating ui backgrounds...'

mkdir -p "${datadir}/gfx/ui"
cp "${datasrcdir}/gfx/ui/ui.png" "${datadir}/gfx/ui"

svg_render "gfx/ui/title" && canvas_resize "${datadir}/gfx/ui/title.png" 2048 2048
svg_render "gfx/ui/banner" && canvas_resize "${datadir}/gfx/ui/banner.png" 2048 512

svg_render "gfx/icon"
svg_render "gfx/characters/reduced"

xcf_render "gfx/ui/nikko" 2048 2048
xcf_render "gfx/ui/nagano" 2048 2048

printok


# ========== #
# = ARENAS = #
# ========== #

rm -rf "${datadir}/gfx/arenas" # Clean any pre-existing content
mkdir -p "${datadir}/gfx/arenas"

labelprint 'Generating arenas...'

svg_render "gfx/arenas/platform" && canvas_resize "${datadir}/gfx/arenas/platform.png" 2048 2048
svg_render "gfx/arenas/sandbox" && canvas_resize "${datadir}/gfx/arenas/sandbox.png" 2048 2048
svg_render "gfx/arenas/stairs" && canvas_resize "${datadir}/gfx/arenas/stairs.png" 2048 2048
xcf_render "gfx/arenas/konomachi" 2048 2048
xcf_render "gfx/arenas/jarvi" 2048 2048
xcf_render "gfx/arenas/isui" 2048 2048

# Longyear
xcf_render "gfx/arenas/longyear/longyear_background" 1920 1080 "${tmpdir}/longyear_background"
svg_render "gfx/arenas/longyear/longyear_box" "${tmpdir}/longyear_box"
convert "${tmpdir}/longyear_background.png" "${tmpdir}/longyear_box.png" -background none -bordercolor none -border 0x2 -append "${tmpdir}/longyear.png"
convert "${tmpdir}/longyear.png" -background none -gravity NorthWest -chop 0x2+0+0 -extent 2048x2048 "${datadir}/gfx/arenas/longyear.png"

printok


# ========== #
# = WEAPONS = #
# ========== #

rm -rf "${datadir}/gfx/weapons" # Clean any pre-existing content
mkdir -p "${datadir}/gfx/weapons"

labelprint 'Generating weapons...'
svg_render "gfx/weapons/shuriken"
printok


# ========= #
# = AUDIO = #
# ========= #

# Copy sounds
rm -rf "${datadir}/audio" # Clean any pre-existing content

labelprint 'Copying audio...'
cp -r "$datasrcdir/audio" "$datadir" ; printok


# Cleanup temporary working directory
cleanup
