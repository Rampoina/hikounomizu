/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYLIST
#define DEF_PLAYLIST

#include "Audio/Music.hpp"

#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>

///A song data (title, author & path to the music file)
struct SongData
{
    SongData()
    {
        
    }
    
    SongData(const std::string &songTitle, const std::string &songAuthor, const std::string &songPath) :
    title(songTitle), author(songAuthor), path(songPath)
    {
        
    }
    
    std::string title, author;
    std::string path; //Path to the file, key to differentiate the songs
};

///Playlist : stores a playlist data (list, volume) and interfaces
///the Music class methods.
///Designed to be used in PlaylistPlayer.
class Playlist
{
    public:
        Playlist();
        ~Playlist();
        
        void update(); //Updates m_playingMusic
        
        void nextSong(bool shouldBeDifferent=true); //Switch to next song and play it (randomly chosen)
        bool songAboutToEnd(); //Is the current song about to end?
        
        void destroy(); //Destroy m_playingMusic if needed
        
        bool getData(SongData &dst) const; //Returns the data from the current song
        
        //Playlist handling methods
        void addSong(const SongData &songData);
        void removeSong(const std::string &songPath);
        
        //Get and set methods
        void setVolume(float volume);
        float getVolume() const;
        
        void setLooping(bool looping);
        bool isLooping() const;
    
    private:
        void startMusic(unsigned int ix);
        
        Music *m_playingMusic; //The currently playing music
        unsigned int m_playingIx; //Index of the currently playing music
        
        std::vector<SongData> m_playlist; //List of the paths to the songs
        
        float m_volume;
        bool m_looping;
};

#endif
