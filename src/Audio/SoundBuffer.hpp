/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_BUFFER
#define DEF_SOUND_BUFFER

#include "Audio/AL.hpp"

#include <vorbis/vorbisfile.h>
#include <cstdio>

#include <vector>
#include <string>
#include <iostream>

#include "Tools/BuildValues.hpp" ///Generated at build time

class SoundBuffer
{
    public:
        SoundBuffer();
        ~SoundBuffer();
        
        //Get and set methods
        ALuint getALBuffer() const;
        
        ALenum getFormat() const;
        ALsizei getFrequency() const;
        
        //Load methods
        bool loadFromOGG(const std::string &relPath);
        bool loadFromMemory(const ALvoid *samples, ALsizei size, ALenum format, ALsizei frequency);
    
    private:
        SoundBuffer(const SoundBuffer &copied);
        void operator=(const SoundBuffer &copied);
        
        ALuint m_alBuffer;
        
        ALenum m_format;
        ALsizei m_frequency;
};

#endif
