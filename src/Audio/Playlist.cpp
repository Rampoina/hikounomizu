/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Playlist.hpp"

Playlist::Playlist() : m_playingMusic(NULL), m_playingIx(0), m_volume(100.f), m_looping(false)
{
    //Initialize the random number generator.
    srand( time(NULL) );
}

Playlist::~Playlist()
{
    destroy();
}

void Playlist::update()
{
    if (m_playingMusic != NULL)
        m_playingMusic->update();
}

//Switch to next song (randomly chosen) and play it
void Playlist::nextSong(bool shouldBeDifferent)
{
    if (m_playlist.size() > 0)
    {
        if (m_looping)
            startMusic(m_playingIx);
        else
        {
            //Randomly choose the next song & update
            unsigned int nextSong = rand() % m_playlist.size();
            if (shouldBeDifferent && m_playlist.size() >= 2)
            {
                while (nextSong == m_playingIx)
                    nextSong = rand() % m_playlist.size();
            }
            
            startMusic(nextSong);
        }
        
        //m_playingMusic was initialized (play it)
        m_playingMusic->play();
        
        std::cout << "Playlist : next song launched" << std::endl;
    }
}

bool Playlist::songAboutToEnd()
{
    if (m_playingMusic != NULL)
        return (m_playingMusic->getRemainingDuration() <= 2.f);
    
    return true;
}

void Playlist::destroy()
{
    if (m_playingMusic != NULL)
    {
        delete m_playingMusic;
        m_playingMusic = NULL;
        
        std::cout << "Playlist : song destroyed" << std::endl;
    }
}

bool Playlist::getData(SongData &dst) const
{
    if (m_playingIx < m_playlist.size())
    {
        dst = m_playlist[m_playingIx];
        return true;
    }
    
    return false;
}

//Playlist handling methods
void Playlist::addSong(const SongData &songData)
{
    m_playlist.push_back(songData);
}

void Playlist::removeSong(const std::string &songPath)
{
    //Remove the song from the playlist
    std::vector<SongData>::iterator it = m_playlist.begin();
    while (it != m_playlist.end())
    {
        if (it->path == songPath)
            it = m_playlist.erase(it);
        else
            it++;
    }
}

//void Playlist::load(const std::string &playlistPath)

void Playlist::startMusic(unsigned int ix)
{
    if (ix >= m_playlist.size())
        return;
    
    //Destroy m_playingMusic if needed
    destroy();
    
    //Reinit it
    m_playingMusic = new Music(m_playlist[ix].path);
    m_playingMusic->setVolume(m_volume);
    
    //Update the data
    m_playingIx = ix;
}

//Get and set methods
void Playlist::setVolume(float volume)
{
    if (m_volume >= 0 && m_volume <= 100)
    {
        m_volume = volume;
        
        if (m_playingMusic != NULL)
            m_playingMusic->setVolume(m_volume);
    }
        
}

float Playlist::getVolume() const
{
    return m_volume;
}

void Playlist::setLooping(bool looping)
{
    m_looping = looping;
}

bool Playlist::isLooping() const
{
    return m_looping;
}
