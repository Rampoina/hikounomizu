/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Audio.hpp"

bool Audio::init()
{
    ALCdevice *device = alcOpenDevice(NULL);
    if (device == NULL)
        return false;
    
    ALCcontext *context = alcCreateContext(device, NULL);
    if (context == NULL)
        return false;
    
    ALCboolean result = alcMakeContextCurrent(context);
    return (result == ALC_TRUE);
}

void Audio::close()
{
    ALCcontext *context = alcGetCurrentContext();
    ALCdevice *device = alcGetContextsDevice(context);
    
    alcMakeContextCurrent(NULL);
    
    alcDestroyContext(context);
    alcCloseDevice(device);
}
