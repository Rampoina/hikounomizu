/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND
#define DEF_SOUND

#include "SoundBuffer.hpp"
#include "Structs/Vector.hpp"

class Sound
{
    public:
        Sound();
        Sound(const Sound &copied);
        Sound &operator=(const Sound &copied);
        
        ~Sound();
        
        void play();
        void pause();
        
        void stop();
        
        //Get and set methods
        bool isPlaying() const;
        
        void setBuffer(SoundBuffer &buffer);
        const SoundBuffer &getBuffer() const;
        
        void setPosition(const Vector &position);
        const Vector &getPosition() const;
        
        void setVelocity(const Vector &velocity);
        const Vector &getVelocity() const;
        
        void setVolume(float volume);
        float getVolume() const;
        
        void setPitch(float pitch);
        float getPitch() const;
    
    private:        
        ALuint m_alSource;
        SoundBuffer *m_buffer;
        
        Vector m_position, m_velocity;
        float m_volume, m_pitch;
};

#endif
