/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MUSIC
#define DEF_MUSIC

#include "Audio/AL.hpp"

#include <vorbis/vorbisfile.h>
#include <iostream>
#include <cstdio>
#include <string>

#include "Tools/BuildValues.hpp" ///Generated at build time

class Music
{
    public:
        Music(const std::string &relPath);
        ~Music();
        
        void update();
        
        void play();
        void pause();
        
        void stop();
        
        //Get and set methods
        void setLooping(bool loop);
        bool isLooping() const;
        
        void setVolume(float volume);
        float getVolume() const;
        
        bool isPlaying() const;
        double getRemainingDuration();
        
        ALenum getFormat() const;
        ALsizei getFrequency() const;
    
    private:
        Music(const Music &copied);
        void operator=(const Music &copied);
        
        bool loadBuffer(ALuint buffer);
        
        FILE *m_file;
        OggVorbis_File m_oggFile;
        
        ALuint m_alSource; //Sound source
        ALuint m_alBuffers[2]; //Streaming buffers
        
        ALenum m_format;
        ALsizei m_frequency;
        double m_duration;
        
        float m_volume;
        bool m_loop;
};

#endif
