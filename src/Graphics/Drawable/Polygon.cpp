/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Polygon.hpp"

Polygon::Polygon() : Drawable(), m_borderSize(1)
{
    
}

void Polygon::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    //Enable needed modules
    glEnable(GL_MULTISAMPLE);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    //Draw
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    
    glVertexPointer(2, GL_FLOAT, 0, &m_vertexArray[0]);
    
    //Background
    glColorPointer(4, GL_UNSIGNED_BYTE, 8 * sizeof(GLubyte), &m_colorsArray[0]);
    glDrawArrays(GL_POLYGON, 0, m_pointsData.size());
    
    //Border
    if (m_borderSize > 0)
    {
        glLineWidth(m_borderSize);
        
        glColorPointer(4, GL_UNSIGNED_BYTE, 8 * sizeof(GLubyte), &m_colorsArray[4]);
        glDrawArrays(GL_LINE_LOOP, 0, m_pointsData.size());
        
        glLineWidth(1);
    }
    
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    
    //Disable loaded modules
    glDisable(GL_BLEND);
    glDisable(GL_MULTISAMPLE);
    
    Drawable::popMatrix();
}

void Polygon::addPoint(float x, float y, const Color &color, const Color &borderColor)
{
    m_pointsData.push_back( Point(x, y, color, borderColor) );
    
    updateVertexArray();
    updateColorsArray();
}

void Polygon::removePoint(int pointIx)
{
    if (pointIx >= 0 && pointIx <= static_cast<int>(m_pointsData.size()) - 1)
        m_pointsData.erase(m_pointsData.begin() + pointIx);
    
    updateVertexArray();
    updateColorsArray();
}

//Get and set methods
void Polygon::setCoords(float x, float y, int pointIx)
{
    if (pointIx >= 0 && pointIx <= static_cast<int>(m_pointsData.size()) - 1)
    {
        m_pointsData[pointIx].x = x;
        m_pointsData[pointIx].y = y;
    }
    
    updateVertexArray();
}

void Polygon::setColor(const Color &color, int pointIx)
{
    if (pointIx == -1)
    {
        for (unsigned int i = 0; i < m_pointsData.size(); i++)
            m_pointsData[i].color = color;
    }
    else if (pointIx >= 0 && pointIx <= static_cast<int>(m_pointsData.size()) - 1)
        m_pointsData[pointIx].color = color;
    
    updateColorsArray();
}

void Polygon::setBorderColor(const Color &borderColor, int pointIx)
{
    if (pointIx >= 0 && pointIx <= static_cast<int>(m_pointsData.size()) - 1)
        m_pointsData[pointIx].borderColor = borderColor;
    
    updateColorsArray();
}

const std::vector<Point> &Polygon::getPoints() const
{
    return m_pointsData;
}

void Polygon::setBorderSize(float borderSize)
{
    if (borderSize >= 0)
        m_borderSize = borderSize;
}

float Polygon::getBorderSize() const
{
    return m_borderSize;
}

void Polygon::updateVertexArray()
{
    m_vertexArray.clear();
    
    std::vector<Point>::iterator it;
    for (it = m_pointsData.begin(); it != m_pointsData.end(); it++)
    {
        m_vertexArray.push_back(it->x);
        m_vertexArray.push_back(it->y);
    }
}

void Polygon::updateColorsArray()
{
    m_colorsArray.clear();
    
    std::vector<Point>::iterator it;
    for (it = m_pointsData.begin(); it != m_pointsData.end(); it++)
    {
        m_colorsArray.push_back(it->color.red);
        m_colorsArray.push_back(it->color.green);
        m_colorsArray.push_back(it->color.blue);
        m_colorsArray.push_back(it->color.alpha);
        
        m_colorsArray.push_back(it->borderColor.red);
        m_colorsArray.push_back(it->borderColor.green);
        m_colorsArray.push_back(it->borderColor.blue);
        m_colorsArray.push_back(it->borderColor.alpha);
    }
}

//Static methods
Polygon Polygon::rectangle(float x, float y, float width, float height, const Color &color, const Color &borderColor)
{
    Polygon polygon;
    polygon.addPoint(x, y, color, borderColor);
    polygon.addPoint(x + width, y, color, borderColor);
    polygon.addPoint(x + width, y + height, color, borderColor);
    polygon.addPoint(x, y + height, color, borderColor);
    
    return polygon;
}

Polygon Polygon::circle(float radius, unsigned int precision, const Color &color, const Color &borderColor)
{
    Polygon polygon;
    
    float angle = 0.f, step=2*PI/precision;
    for (unsigned int i = 0; i < precision; i++)
    {
        polygon.addPoint( radius*static_cast<float>( cos(angle) ) + radius,
                          radius*static_cast<float>( sin(angle) ) + radius,
                          color, borderColor );
        angle += step;
    }
    
    return polygon;
}
