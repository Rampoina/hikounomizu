/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Sprite.hpp"

Sprite::Sprite() : Drawable(), m_texture(NULL),
m_subRect( Box(0.f, 0.f, 1.f, 1.f) )
{
    
}

Sprite::Sprite(Texture &texture, const Box &subRect) :
m_texture(&texture), m_subRect(subRect)
{
    
}

void Sprite::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    if (m_texture != NULL)
    {
        float x1 = m_subRect.left, y1 = m_subRect.top;
        float x2 = m_subRect.left + m_subRect.width, y2 = m_subRect.top + m_subRect.height;
        
        m_texture->draw(x1, y1, x2, y2);
    }
    
    Drawable::popMatrix();
}

void Sprite::setTexture(Texture &texture)
{
    m_texture = &texture;
}

const Texture &Sprite::getTexture() const
{
    return *m_texture;
}

void Sprite::setSubRect(const Box &subRect)
{
    m_subRect = subRect;
}

const Box &Sprite::getSubRect() const
{
    return m_subRect;
}

float Sprite::getWidth() const
{
    if (m_texture != NULL)
        return m_subRect.width * m_texture->getWidth();
    
    return 0;
}

float Sprite::getHeight() const
{
    if (m_texture != NULL)
        return m_subRect.height * m_texture->getHeight();
    
    return 0;
}
