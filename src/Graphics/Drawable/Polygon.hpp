/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_POLYGON
#define DEF_POLYGON

#define PI 3.14159265

#include "Drawable.hpp"
#include "Structs/Color.hpp"

#include <vector>
#include <cmath>

struct Point
{
    Point() : x(0), y(0)
    {
        
    }
    
    Point(float xCoords, float yCoords, const Color &background, const Color &border) :
    x(xCoords), y(yCoords), color(background), borderColor(border)
    {
        
    }
    
    float x, y;
    Color color, borderColor;
};

class Polygon : public Drawable
{
    public:
        Polygon();
        void draw();
        
        void addPoint(float x, float y, const Color &color, const Color &borderColor);
        void removePoint(int pointIx);
        
        //Get and set methods
        void setCoords(float x, float y, int pointIx);
        void setColor(const Color &color, int pointIx = -1);
        void setBorderColor(const Color &borderColor, int pointIx);
        
        const std::vector<Point> &getPoints() const;
        
        void setBorderSize(float borderSize);
        float getBorderSize() const;
        
        //Static methods
        static Polygon rectangle(float x, float y, float width, float height, const Color &color = Color(), const Color &borderColor = Color());
        static Polygon circle(float radius, unsigned int precision = 360, const Color &color = Color(), const Color &borderColor = Color());
    
    private:
        void updateVertexArray();
        void updateColorsArray();
        
        std::vector<float> m_vertexArray;
        std::vector<GLubyte> m_colorsArray;
        
        std::vector<Point> m_pointsData;
        
        float m_borderSize;
};

#endif
