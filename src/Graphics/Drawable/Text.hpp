/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXT
#define DEF_TEXT

#include "Drawable.hpp"
#include "Graphics/Resources/Font.hpp"

#include "Structs/Color.hpp"
#include <vector>
#include <string>

#define UTF32_OFFSET_ERROR 1000

/// Struct to pair a glyph and its associated kerning value.
struct GlyphKerning
{
    GlyphKerning() : glyph(NULL), kerning(0.f) {}
    GlyphKerning(const Glyph *glyphPtr, float kerningVal) : glyph(glyphPtr), kerning(kerningVal) {}
    
    const Glyph *glyph;
    float kerning;
};

class Text : public Drawable
{
    public:
        Text();
        Text(const std::string &text, Font &font);
        void draw();
        
        void setFont(Font &font);
        const Font *getFont() const;
        
        void setText(const std::string &text);
        const std::string &getText() const;
        
        void setColor(const Color &color);
        const Color &getColor() const;
        
        float getWidth() const;
        float getHeight() const;
        
        static unsigned int toUTF32(const std::string &utf8str, unsigned int position, FT_ULong &target);
    
    private:
        void loadGlyphs();
        
        Font *m_font;
        
        std::string m_text;
        std::vector<GlyphKerning> m_glyphs; ///Glyphs and kerning values.
        
        Color m_color;
        float m_width, m_height;
};

#endif
