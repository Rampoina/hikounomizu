/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_DRAWABLE
#define DEF_DRAWABLE

#include "Graphics/GL.hpp"

class Drawable
{
    public:
        Drawable();
        virtual ~Drawable();
        
        virtual void draw() = 0;
        
        //Get and set methods
            //Position
        void setPosition(float x, float y);
        
        void setXPosition(float position);
        float getXPosition() const;
        
        void setYPosition(float position);
        float getYPosition() const;
        
            //Origin
        void setOrigin(float x, float y);
        
        void setXOrigin(float origin);
        float getXOrigin() const;
        
        void setYOrigin(float origin);
        float getYOrigin() const;
        
            //Scale
        void setScale(float x, float y);
        void setScale(float scale);
        
        void setXScale(float scale);
        float getXScale() const;
        
        void setYScale(float scale);
        float getYScale() const;
        
            //Rotation
        void setRotation(float angle);
        float getRotation() const;
        
            //Flip
        void setFlip(bool x, bool y);
        
        void setXFlip(bool flip);
        bool getXFlip() const;
        
        void setYFlip(bool flip);
        bool getYFlip() const;
    
    protected:
        void pushMatrix();
        void updateMatrix();
        void popMatrix();
        
        float m_xPosition, m_yPosition;
        float m_xOrigin, m_yOrigin;
        float m_xScale, m_yScale;
        float m_rotationAngle;
        bool m_xFlip, m_yFlip;
};

#endif
