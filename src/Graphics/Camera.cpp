/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Camera.hpp"

Camera::Camera()
{
    
}

Camera::Camera(const Box &sceneBox) : m_sceneBox(sceneBox)
{
    
}

void Camera::look(float viewWidth, float viewHeight)
{
    Camera::look(m_sceneBox, viewWidth, viewHeight);
}

void Camera::setBox(const Box &sceneBox)
{
    m_sceneBox = sceneBox;
}

const Box &Camera::getBox() const
{
    return m_sceneBox;
}

void Camera::look(const Box &sceneBox, float viewWidth, float viewHeight)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glScalef(viewWidth / sceneBox.width, viewHeight / sceneBox.height, 1);
    glTranslatef(-sceneBox.left, -sceneBox.top, 0);
}

void Camera::look(const Box &sceneBox, const Box &targetBox)
{
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glTranslatef(targetBox.left, targetBox.top, 0);
    
    glScalef(targetBox.width / sceneBox.width, targetBox.height / sceneBox.height, 1);
    glTranslatef(-sceneBox.left, -sceneBox.top, 0);
}
