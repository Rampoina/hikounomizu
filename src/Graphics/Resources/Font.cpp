/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Font.hpp"

Font::Font(const FT_Library library, const std::string &relPath, int charSize) :
m_charSize(charSize), m_kerningAvailable(false)
{
    std::string path = BuildValues::data(relPath); //Locate absolute path to data
    
    if (FT_New_Face(library, path.c_str(), 0, &m_face) == 0)
    {
        FT_Select_Charmap(m_face, ft_encoding_unicode); // Big-Endian UTF-32.
        FT_Set_Char_Size(m_face, charSize << 6, charSize << 6, 72, 72);
        
        if (FT_HAS_KERNING(m_face)) //returns a FT_Bool
            m_kerningAvailable = true;
    }
    else
        m_face = NULL;
}

Font::~Font()
{
    std::map<FT_ULong, Glyph*>::iterator it;
    for (it = m_glyphsList.begin(); it != m_glyphsList.end(); it++)
        delete it->second;
    
    if (m_face != NULL)
        FT_Done_Face(m_face);
}

float Font::getKerning(FT_ULong char1, FT_ULong char2)
{
    if (!m_kerningAvailable)
        return 0.f;
    
    FT_UInt glyph1 = FT_Get_Char_Index(m_face, char1),
            glyph2 = FT_Get_Char_Index(m_face, char2);
    
    if (glyph1 == 0 || glyph2 == 0)
        return 0.f;
    
    FT_Vector kerning;
    FT_Get_Kerning( m_face, glyph1, glyph2, FT_KERNING_DEFAULT, &kerning );
    
    return static_cast<float>( kerning.x >> 6 );
}


const Glyph *Font::getGlyph(FT_ULong character)
{
    if (m_face == NULL)
        return NULL;
    
    if (!isGlyphLoaded(character))
        loadGlyph(character);
    
    return m_glyphsList[character];
}

int Font::getCharSize() const
{
    return m_charSize;
}

void Font::loadGlyph(FT_ULong character)
{
    //Load character
    FT_Load_Char(m_face, character, FT_LOAD_DEFAULT);
    
    //Retrieve glyph & information
    FT_Glyph rawGlyph;
    FT_Get_Glyph(m_face->glyph, &rawGlyph);
    
    FT_Glyph_To_Bitmap(&rawGlyph, FT_RENDER_MODE_NORMAL, 0, 1);
    FT_BitmapGlyph bitmapGlyph = (FT_BitmapGlyph)rawGlyph;
    
    FT_GlyphSlot slot = (FT_GlyphSlot)m_face->glyph;
    
    //Create a matching Glyph*
    Glyph *glyph = new Glyph();
    glyph->load(bitmapGlyph, Vector(slot->advance.x >> 6, slot->advance.y >> 6));
    
    m_glyphsList[character] = glyph;
    
    //Unload glyph
    FT_Done_Glyph(rawGlyph);
}

bool Font::isGlyphLoaded(FT_ULong character)
{
    std::map<FT_ULong, Glyph*>::iterator it;
    it = m_glyphsList.find(character);
    
    if (it != m_glyphsList.end())
        return true;
    
    return false;
}
