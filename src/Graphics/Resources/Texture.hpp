/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXTURE
#define DEF_TEXTURE

#include "Graphics/GL.hpp"

#include <SDL2/SDL_image.h>
#include <string>
#include <iostream>

#include "Tools/BuildValues.hpp" ///Generated at build time

#define MINMAG_LINEAR 0
#define MINMAG_NEAREST 1

class Texture
{
    public:
        Texture();
        ~Texture();
        
        void draw(float x1 = 0.f, float y1 = 0.f, float x2 = 1.f, float y2 = 1.f) const;
        
        //Get and set methods
        GLsizei getWidth() const;
        GLsizei getHeight() const;
        
        //Load methods
        bool loadFromFile(const std::string &relPath, int filter);
        bool loadFromMemory(const GLvoid *pixels, GLint format, GLenum dataFormat, GLsizei width, GLsizei height, int filter);
        
        //Static method
        static unsigned int nextPow2(unsigned int number);
    
    private:
        Texture(const Texture &copied);
        void operator=(const Texture &copied);
        
        GLuint m_glTexture;
        
        GLsizei m_width;
        GLsizei m_height;
};

#endif
