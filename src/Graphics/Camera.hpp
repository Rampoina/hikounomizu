/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CAMERA
#define DEF_CAMERA

#include "Graphics/GL.hpp"
#include "Structs/Box.hpp"

///Camera : OpenGL camera setting a box to look at.
class Camera
{
    public:
        Camera();
        Camera(const Box &sceneBox);
        
        void look(float viewWidth, float viewHeight);
        
        //Get and set methods
        void setBox(const Box &sceneBox);
        const Box &getBox() const;
        
        static void look(const Box &sceneBox, float viewWidth, float viewHeight);
        static void look(const Box &sceneBox, const Box &targetBox);
    
    private:
        Box m_sceneBox; ///< Scene box to be looked by the camera.
};

#endif
