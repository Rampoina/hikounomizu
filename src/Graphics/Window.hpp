/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WINDOW
#define DEF_WINDOW

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Graphics/GL.hpp"

#include "Tools/Timer.hpp"

#include "Structs/Vector.hpp"
#include "Structs/Box.hpp"
#include "Structs/Color.hpp"

#include <iostream>
#include <string>
#include <vector>

class Window
{
    public:
        Window(const std::string &title);
        
        bool create(int width, int height, bool fullscreen, int msBuffers = 1, int msSamples = 4);
        void destroy();
        
        void resizeWindow(int windowWidth, int windowHeight, bool fullscreen);
        
        void initView(float viewWidth, float viewHeight);
        void resizeView();
        
        void clear(const Color &color = Color());
        void flush();
        
        bool pollEvent(SDL_Event &event);
        bool waitEvent(SDL_Event &event);
        
        Vector viewCoords(const Vector &windowCoords) const;
        Vector windowCoords(const Vector &viewCoords) const;
        
        //Get and set methods
        void setIcon(const std::string &iconPath);
        void setTitle(const std::string &title);
        const std::string &getTitle() const;
        
        void setFrameRate(float frameRate);
        float getFrameRate() const;
        
        int getWidth() const;
        int getHeight() const;
        
        float getViewWidth() const;
        float getViewHeight() const;
        
        std::vector<Vector> getAvailableModes();
    
    private:
        std::string m_title;
        
        SDL_Window *m_window;
        SDL_GLContext m_glContext;
        
        float m_viewWidth, m_viewHeight;
        Box m_viewPort;
        
        float m_latency;
        Timer m_time;
};

#endif
