/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Platform.hpp"

Platform::Platform() : PhysicsObject(), m_soundEngine(NULL),
m_graphicsWidth(0.f), m_graphicsHeight(0.f), m_hitable(false)
{
    
}

void Platform::draw()
{
    resizeSprite();
    
    m_sprite.setXOrigin(m_bodyBox.left);
    m_sprite.setYOrigin(m_bodyBox.top + m_bodyBox.height);
    
    m_sprite.setXPosition(m_box.left);
    m_sprite.setYPosition(m_box.top + m_box.height);
    
    m_sprite.draw();
}

//Get and set methods
void Platform::setBodyBox(const Box &bodyBox)
{
    m_bodyBox = bodyBox;
}

const Box &Platform::getBodyBox() const
{
    return m_bodyBox;
}

void Platform::setSprite(const Sprite &sprite)
{
    m_sprite = sprite;
}

const Sprite &Platform::getSprite() const
{
    return m_sprite;
}

void Platform::setGraphicsSize(float width, float height)
{
    if (width > 0 && height > 0)
    {
        m_graphicsWidth = width;
        m_graphicsHeight = height;
    }
}

void Platform::setSoundEngine(SoundEngine &soundEngine)
{
    m_soundEngine = &soundEngine;
}

void Platform::collideWorld(bool /*ground*/)
{
    //Nothing to do :)
}

void Platform::collide(PhysicsObject& /*obj*/)
{
    //Nothing to do :)
}

void Platform::takeAHit(float strenght, int side, const PhysicsObject* /*source*/)
{
    if (m_hitable)
    {
        //Play impact sound
        if (m_soundEngine != NULL)
        {
            std::string material = getMaterial();
            if (material != MATERIAL_NONE)
                m_soundEngine->playSoundEffect("impact_" + material, CHARACTER_AGNOSTIC);
        }
        
        //Pulse
        float pulse = 40 * strenght;
        if (side == 1) pulse *= -1;
        
        if (m_physicsWorld != NULL)
            m_physicsWorld->pulseObject((*this), pulse, 0);
    }
}

void Platform::setHitable(bool hitable)
{
    m_hitable = hitable;
}

bool Platform::isHitable() const
{
    return m_hitable;
}

void Platform::resizeSprite()
{
    //Resize sprite to platform size
    if (m_sprite.getWidth() > 0 && m_sprite.getHeight() > 0)
    {
        m_sprite.setXScale(m_graphicsWidth / m_sprite.getWidth());
        m_sprite.setYScale(m_graphicsHeight / m_sprite.getHeight());
    }
}

// Platform should intercept shadows of objects over them
bool Platform::interceptsShadows() const
{
    return true;
}
