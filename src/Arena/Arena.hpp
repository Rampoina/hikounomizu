/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA
#define DEF_ARENA

#include "Engines/Physics/PhysicsWorld.hpp"
#include "Platform.hpp"

///Static loadFromXML method
#include "Tools/BuildValues.hpp" ///Generated at build time
#include "Engines/Resources/TextureManager.hpp"
#include <tinyxml.h>

///Arena: Class representing an arena.
///It can contain platforms, specify players spawn position...
class Arena
{
    public:
        Arena();
        Arena(const Sprite &background, float width, float height, float ground, float gravity, float airFriction);
        ~Arena();
        
        void draw();
        
        void addPlatform(const Box &graphicsBox, const Box &bodyBox, const Sprite &sprite,
                         const std::string &material, bool hitable=false, bool fixed=false);
        void removePlatform(int platformIx);
        void removePlatforms();
        
        void applyToPhysics(PhysicsWorld &physicsWorld);
        void subscribeObjects(PhysicsWorld &physicsWorld, SoundEngine *soundEngine=NULL);
        
        void loadFromXML(const std::string &name, const std::string &xmlRelPath, TextureManager &textureManager);
        
        //Get and set methods
            //Graphics
        void setBackground(const Sprite &background);
        const Sprite &getBackground() const;
        
            //Physics
        void setWidth(float width);
        float getWidth() const;
        
        void setHeight(float height);
        float getHeight() const;
        
        void setGround(float ground);
        float getGround() const;
        
        void setGroundMaterial(const std::string &material);
        const std::string &getGroundMaterial() const;
        
        void setGravity(float gravity);
        float getGravity() const;
        
        void setAirFriction(float airFriction);
        float getAirFriction() const;
        
        void setMinCameraWidth(float minWidth);
        float getMinCameraWidth() const;
        
            //Other
        void setPlayerSpawn(unsigned int ix, const Vector &playerSpawn);
        bool getPlayerSpawn(unsigned int ix, Vector &target) const;
    
    private:
        void resizeBackground();
        
        Sprite m_background;
        
        float m_width, m_height, m_ground;
        std::string m_groundMaterial;
        
        float m_gravity, m_airFriction;
        float m_minCameraWidth;
        
        std::vector< std::pair<Platform*, Box> > m_platformsList;
        std::map<unsigned int, Vector> m_playerSpawns;
};

#endif
