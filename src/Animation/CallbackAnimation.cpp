/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CallbackAnimation.hpp"

CallbackAnimation::CallbackAnimation() :
Animation(), m_receiver(NULL)
{
    
}

void CallbackAnimation::setReceiver(AnimationCallbackReceiver *receiver)
{
    m_receiver = receiver;
}

void CallbackAnimation::addStepImpactCallback(unsigned int frameIx)
{
    if (frameIx < m_framesList.size())
        m_stepImpactCallbacks.push_back(frameIx);
}

void CallbackAnimation::goForward(float distance)
{
    unsigned int frameBegin = m_frameIx;
    Animation::goForward(distance);
    
    if (m_receiver != NULL)
    {
        for (unsigned int i = 0; i < m_stepImpactCallbacks.size(); i++)
        {
            unsigned int impact = m_stepImpactCallbacks[i];
            if (frameBegin <= impact && impact < m_frameIx)
                m_receiver->stepImpactCallback();
        }
    }
}
