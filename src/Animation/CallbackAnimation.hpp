/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CALLBACK_ANIMATION
#define DEF_CALLBACK_ANIMATION

#include "Animation.hpp"

///AnimationCallbackReceiver: Abstract class to
///extend in order to catch animation-related callbacks.
class AnimationCallbackReceiver
{
    public:
        virtual ~AnimationCallbackReceiver() {}
        virtual void stepImpactCallback() = 0;
};

///CallbackAnimation: An animation which
///produces callback at pre-defined times.
class CallbackAnimation : public Animation
{
    public:
        CallbackAnimation();
        
        void setReceiver(AnimationCallbackReceiver *receiver);
        void addStepImpactCallback(unsigned int frameIx);
        
        void goForward(float distance); //Animation method
    
    private:
        AnimationCallbackReceiver *m_receiver;
        std::vector<unsigned int> m_stepImpactCallbacks;
};

#endif
