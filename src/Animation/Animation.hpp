/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ANIMATION
#define DEF_ANIMATION

#include "Graphics/Drawable/Drawable.hpp"
#include "Graphics/Resources/Texture.hpp"

#include "Structs/Box.hpp"
#include <vector>

struct Frame
{
    Frame()
    {
        
    }
    
    Frame(const Box &source, const Box &body) : srcBox(source), bodyBox(body)
    {
        
    }
    
    Box srcBox, bodyBox;
};

class Animation : public Drawable
{
    public:
        Animation();
        void draw();
        
        void addFrame(const Frame &frame, int frameIx = -1);
        void removeFrame(int frameIx);
        
        void goToTime(float time);
        
        void goForward(float distance);
        void goBack(float distance);
        
        //Get and set methods
        unsigned int getFrameSize() const;
        
        void setTexture(Texture &texture);
        const Texture &getTexture() const;
        
        void setDuration(float duration);
        float getDuration() const;
        
        float getCurrentTime() const;
        bool getBodyBox(Box &target) const;
        
        float getWidth() const;
        float getHeight() const;
    
    protected:
        Texture *m_texture;
        std::vector<Frame> m_framesList;
        
        unsigned int m_frameIx;
        
        float m_duration; //Duration in seconds
        float m_time; //Time in animation
};

#endif
