/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_BOX
#define DEF_BOX

#include "Vector.hpp"

struct Box
{
    Box() : left(0.f), top(0.f), width(0.f), height(0.f)
    {
        
    }
    
    Box(float x, float y, float w, float h) :
    left(x), top(y), width(w), height(h)
    {
        
    }
    
    void fadeInto(const Box &target, float fadeRatio)
    {
        float leftOffset = (target.left - left) * fadeRatio;
        float topOffset = (target.top - top) * fadeRatio;
        float rightOffset = ((target.left+target.width) - (left+width)) * fadeRatio;
        float bottomOffset = ((target.top+target.height) - (top+height)) * fadeRatio;
        
        left += leftOffset;
        top += topOffset;
        width += rightOffset - leftOffset;
        height += bottomOffset - topOffset;
    }
    
    bool contains(const Vector &point) const
    {
        if ((point.x > left && point.x < left + width) && (point.y > top && point.y < top + height))
            return true;
        
        return false;
    }
    
    bool contains(const Box &box) const
    {
        if ((left <= box.left && left + width >= box.left + box.width) && (top <= box.top && top + height >= box.top + box.height))
            return true;
        
        return false;
    }
    
    bool hits(const Box &box) const
    {
        if ((left + width > box.left && left < box.left + box.width) && (top + height > box.top && top < box.top + box.height))
            return true;
        
        return false;
    }
    
    Vector getCenter() const
    {
        return Vector((left + width) / 2, (top + height) / 2);
    }
    
    float left, top, width, height;
};

#endif
