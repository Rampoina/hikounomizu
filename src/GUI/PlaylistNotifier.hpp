/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYLIST_NOTIFIER
#define DEF_PLAYLIST_NOTIFIER

#define NOTIFIER_WIDTH 300
#define NOTIFIER_HEIGHT 65

#include "Graphics/Drawable/Text.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Tools/Timer.hpp"

#include <string>

///PlaylistNotifier : Drawable used to notify the user that a new song
///was launched (e.g.: in a playlist)
///Shows the title of the new song / the author name / ...
///Shared between the main thread that draws it and some PlaylistPlayer
///thread that calls notify() : must be protected by mutexes.
class PlaylistNotifier : public Drawable
{
    public:
        PlaylistNotifier();
        void draw();
        void update();
        
        //notify :
        //Called from the thread playing the playlist.
        //Only sets the string information to be drawed,
        //The Texts will be set in the main thread.
        //(A single Freetype's FT_Face can't be called from two threads:
        //cf. freetype.org/freetype2/docs/reference/)
        void notify(const std::string &author, const std::string &title);
        
        //Get and set methods
        void setNotifyTime(float notifyTime); //Time in ms
        float getNotifyTime() const;
        
        void setFonts(Font &titleFont, Font &authorFont);
        const Font *getTitleFont() const;
        const Font *getAuthorFont() const;
        
        float getWidth() const;
        float getHeight() const;
    
    private:
        //Starts a graphical notification from notify()'s data
        //(called in the main thread)
        void launchNotification();
        
        //Raw data
        std::string m_author, m_title;
        bool m_notifyRequested; //Has a notification been requested by a PlaylistPlayer thread ?
        
        //Drawable objects
        Text m_authorText, m_titleText;
        Polygon m_background;
        
        float m_notifyTime; //The time during which the notification will be drawn (in ms)
        Timer m_notifyTimer; //The timer to manage it
        
        bool m_notifying; //Is the notification being drawn ?
};

#endif
