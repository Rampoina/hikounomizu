/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlaylistNotifier.hpp"

PlaylistNotifier::PlaylistNotifier() : m_notifyRequested(false), m_notifyTime(1000), m_notifying(false)
{
    m_background = Polygon::rectangle(0, 0, NOTIFIER_WIDTH, NOTIFIER_HEIGHT, Color(66, 112, 174), Color(84, 126, 181));
    m_background.setBorderSize(15);
    
    m_titleText.setColor( Color(255, 255, 255) );
    m_authorText.setColor( Color(255, 255, 255, 200) );
    
    m_titleText.setPosition(20, 15);
    m_authorText.setPosition(20, 35);
}

void PlaylistNotifier::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    update();
    
    if (m_notifying)
    {
        m_background.draw();
        
        m_authorText.draw();
        m_titleText.draw();
    }
    
    Drawable::popMatrix();
}

void PlaylistNotifier::update()
{
    if (m_notifyRequested)
    {
        launchNotification();
        m_notifyRequested = false;
    }
    
    if (m_notifyTimer.getTime() >= m_notifyTime)
        m_notifying = false;
}

void PlaylistNotifier::notify(const std::string &author, const std::string &title)
{
    m_author = author;
    m_title = title;
    
    m_notifyRequested = true;
}

void PlaylistNotifier::launchNotification()
{
    m_authorText.setText(m_author);
    m_titleText.setText(m_title);
    
    m_notifyTimer.reset();
    m_notifying = true;
}

void PlaylistNotifier::setNotifyTime(float notifyTime) //Time in ms
{
    if (notifyTime > 0)
        m_notifyTime = notifyTime;
}

float PlaylistNotifier::getNotifyTime() const
{
    return m_notifyTime;
}

void PlaylistNotifier::setFonts(Font &titleFont, Font &authorFont)
{
    m_titleText.setFont(titleFont);
    m_authorText.setFont(authorFont);
}

const Font *PlaylistNotifier::getTitleFont() const
{
    return m_titleText.getFont();
}

const Font *PlaylistNotifier::getAuthorFont() const
{
    return m_authorText.getFont();
}

float PlaylistNotifier::getWidth() const
{
    return NOTIFIER_WIDTH;
}

float PlaylistNotifier::getHeight() const
{
    return NOTIFIER_HEIGHT;
}
