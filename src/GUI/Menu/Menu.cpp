/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Menu.hpp"

Menu::Menu() : Tab(), m_currentElement(NULL)
{
    
}

Menu::Menu(float width, float height, const Color &color) :
Tab(width, height, color), m_currentElement(NULL)
{
    
}

bool Menu::loadData(const std::string &xmlRelPath)
{
    std::string xmlPath = BuildValues::data(xmlRelPath); //Locate absolute path to xml data
    
    if (m_xmlFile.LoadFile(xmlPath.c_str()))
    {
        resetCurrentElement();
        return true;
    }
    
    return false;
}

void Menu::resetCurrentElement()
{
    TiXmlHandle hdl(&m_xmlFile);
    m_currentElement = hdl.FirstChildElement().FirstChildElement().Element();
}
