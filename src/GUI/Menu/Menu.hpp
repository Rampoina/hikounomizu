/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MENU
#define DEF_MENU

#include "GUI/Widget/Tab.hpp"
#include "Structs/Vector.hpp"

#include <string>
#include <vector>
#include <tinyxml.h>

#include "Tools/BuildValues.hpp" ///Generated at build time

//Class managing a list of Buttons to make a menu.
class Menu : public Tab
{
    public:
        Menu();
        Menu(float width, float height, const Color &color);
        bool loadData(const std::string &xmlRelPath);
        
        virtual void launch() = 0;
        virtual bool mouseRelease(const Vector &mouseCoords) = 0;
        virtual bool mouseClick(const Vector &mouseCoords) = 0;
        virtual bool mouseMove(const Vector &mouseCoords) = 0;
        
        virtual bool browsed() const = 0;
        virtual const std::vector<std::string> &result() const = 0;
        
    protected:
        void resetCurrentElement();
        
        TiXmlDocument m_xmlFile;
        TiXmlElement *m_currentElement;
};

#endif
