/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXT_MENU
#define DEF_TEXT_MENU

#include "Menu.hpp"
#include "GUI/Widget/TextButton.hpp"

class TextMenu : public Menu
{
    public:
        TextMenu();
        TextMenu(const TextButton &button, float space, int orientation);
        TextMenu(const TextButton &button, float space, int orientation, float width, float height, const Color &color);
        void draw();
        
        //Menu virtual methods
        void launch();
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        bool browsed() const;
        const std::vector<std::string> &result() const;
        
        //Get and set methods
        void setButton(const TextButton &button);
        
        void setOrientation(int orientation);
        int getOrientation() const;
        
        void setSpace(float space);
        float getSpace() const;
    
    private:
        void draw(TiXmlElement *itemsList);
        
        float contentWidth() const;
        float contentHeight() const;
        
        std::vector<TextButton> m_buttonsList;
        std::vector<std::string> m_selections;
        
        const TextButton *m_button;
        
        float m_space;
        int m_orientation;
        
        bool m_browsed;
};

#endif
