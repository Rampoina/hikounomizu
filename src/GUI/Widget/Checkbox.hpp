/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHECKBOX
#define DEF_CHECKBOX

#define NO_SOUND ""

#include "Widget.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Engines/Sound/SoundEngine.hpp"
#include <string>

class Checkbox : public Widget
{
    public:
        Checkbox();
        Checkbox(bool checked);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        void setFocus(bool focused);
        
        void check(bool checked);
        bool isChecked() const;
        
        void setSoundEngine(SoundEngine &soundEngine);
        const SoundEngine &getSoundEngine() const;
        
        void setClickSound(const std::string &clickSound);
        const std::string &getClickSound() const;
        
        void setSize(float size);
        void setColor(const Color &color);
        
        float getWidth() const;
        float getHeight() const;
    
    private:
        void updateBox();
        
        Polygon m_checkbox;
        bool m_checked;
        
        float m_size;
        Color m_color;
        
        SoundEngine *m_soundEngine; //To handle sounds on events
        std::string m_clickSound;
};

#endif
