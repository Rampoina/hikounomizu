/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WIDGET
#define DEF_WIDGET

#include "Graphics/Drawable/Drawable.hpp"

#include "Structs/Box.hpp"
#include "Structs/Vector.hpp"

class Widget : public Drawable
{
    public:
        //Called on mouse events :
        //Updates the widget and returns whether the mouse hit it or not.
        virtual bool mouseRelease(const Vector &mouseCoords) = 0;
        virtual bool mouseClick(const Vector &mouseCoords) = 0;
        virtual bool mouseMove(const Vector &mouseCoords) = 0;
        
        virtual void setFocus(bool focused)
        {
            m_focused = focused;
        }
        
        virtual bool isFocused() const
        {
            return m_focused;
        }
        
        virtual float getWidth() const = 0;
        virtual float getHeight() const = 0;
    
    protected:
        
        Vector localCoords(const Vector &global) const
        {
            Vector local(global.x - getXPosition(), global.y - getYPosition());
            if (getXScale() > 0 && getYScale() > 0)
            {
                local.x /= getXScale();
                local.y /= getYScale();
            }
            
            return local;
        }
        
        bool contains(const Vector &localCoords) const
        {
            Box box(0, 0, getWidth(), getHeight());
            if (box.contains(localCoords))
                return true;
            
            return false;
        }
        
        bool m_focused;
};

#endif
