/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHOOSER
#define DEF_CHOOSER

#include "Widget.hpp"

#include "TextButton.hpp"
#include <vector>

struct ChooserSkin
{
    ChooserSkin() : font(NULL)
    {
        
    }
    
    ChooserSkin(Font &textFont, const Color &color, const SkinBox &skinBox, const SkinSound &skinSound) :
    font(&textFont), textColor(color), skin(skinBox), sounds(skinSound)
    {
        
    }
    
    Font *font; //Font for choice elements
    Color textColor;
    SkinBox skin;
    SkinSound sounds;
};

class Chooser : public Widget
{
    public:
        Chooser();
        Chooser(const std::vector<std::string> &data, unsigned int selection = 0);
        Chooser(const ChooserSkin &skin, const std::vector<std::string> &data, unsigned int selection = 0);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        void setData(const std::vector<std::string> &data, unsigned int selection = 0);
        void setSkin(const ChooserSkin &skin);
        
        void setSelection(unsigned int selection);
        unsigned int getSelection() const;

        std::string getSelectionStr() const;
        
        float getWidth() const;
        float getHeight() const;
    
    private:
        ChooserSkin m_choiceSkin;
        std::vector<TextButton> m_choicesList;
        
        unsigned int m_selection;
        bool m_opened;
};

#endif
