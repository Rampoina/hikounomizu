/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Tab.hpp"

Tab::Tab() : Widget(), m_width(350), m_height(200)
{
    m_background = Polygon::rectangle(0, 0, m_width, m_height);
    m_background.setBorderSize(0);
}

Tab::Tab(float width, float height, const Color &color) :
Widget(), m_width(width), m_height(height)
{
    m_background = Polygon::rectangle(0, 0, m_width, m_height, color);
    m_background.setBorderSize(0);
}

void Tab::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    Drawable::popMatrix();
}

float Tab::getWidth() const
{
    return m_width;
}

float Tab::getHeight() const
{
    return m_height;
}
