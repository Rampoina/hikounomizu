/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "KeyBinder.hpp"

KeyBinder::KeyBinder() : Widget(),
m_soundEngine(NULL), m_focusSound(NO_SOUND), m_keySetSound(NO_SOUND)
{
    m_keyText.setColor(Color(200, 200, 200));
    setKey(Key(KeyboardKey(SDL_SCANCODE_A)));
}

KeyBinder::KeyBinder(const Key &key) : Widget(),
m_soundEngine(NULL), m_focusSound(NO_SOUND), m_keySetSound(NO_SOUND)
{
    m_keyText.setColor(Color(200, 200, 200));
    setKey(key);
}

void KeyBinder::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    if (m_focused) m_focusedBack.draw();
    else m_inactiveBack.draw();
    
    m_keyText.draw();
    
    Drawable::popMatrix();
}

bool KeyBinder::mouseRelease(const Vector &mouseCoords)
{
    if ( Widget::contains( Widget::localCoords(mouseCoords) ) )
    {
        setFocus( !isFocused() );
        return true;
    }
    
    return false;
}

bool KeyBinder::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool KeyBinder::mouseMove(const Vector &mouseCoords)
{
    return Widget::contains( Widget::localCoords(mouseCoords) );
}

void KeyBinder::keyDown(const Key &key)
{
    //If focused then set the key
    if (m_focused)
    {
        //Launch key set sound
        if (m_soundEngine != NULL && m_keySetSound != NO_SOUND)
            m_soundEngine->playSound(m_keySetSound);
        
        setKey(key);
        setFocus(false);
    }
}

const Key &KeyBinder::getKey() const
{
    return m_key;
}

void KeyBinder::setFocus(bool focused)
{
    if (!isFocused() && focused)
    {
        //Launch focus sound
        if (m_soundEngine != NULL && m_focusSound != NO_SOUND)
            m_soundEngine->playSound(m_focusSound);
    }
    
    Widget::setFocus(focused);
}

void KeyBinder::setTextFont(Font &textFont)
{
    m_keyText.setFont(textFont);
    centerText();
}

const Font *KeyBinder::getTextFont() const
{
    return m_keyText.getFont();
}

void KeyBinder::setInactiveBack(const Sprite &inactive)
{
    m_inactiveBack = inactive;
    centerText();
}

const Sprite &KeyBinder::getInactiveBack() const
{
    return m_inactiveBack;
}

void KeyBinder::setFocusedBack(const Sprite &focused)
{
    m_focusedBack = focused;
    centerText();
}

const Sprite &KeyBinder::getFocusedBack() const
{
    return m_focusedBack;
}

void KeyBinder::setSoundEngine(SoundEngine &soundEngine)
{
    m_soundEngine = &soundEngine;
}

const SoundEngine &KeyBinder::getSoundEngine() const
{
    return *m_soundEngine;
}

void KeyBinder::setFocusSound(const std::string &focusSound)
{
    m_focusSound = focusSound;
}

const std::string &KeyBinder::getFocusSound() const
{
    return m_focusSound;
}

void KeyBinder::setKeySetSound(const std::string &keySetSound)
{
    m_keySetSound = keySetSound;
}

const std::string &KeyBinder::getKeySetSound() const
{
    return m_keySetSound;
}

void KeyBinder::setKey(const Key &key)
{
    m_key = key;
    m_keyText.setText( key.getName() );
    
    centerText();
}

void KeyBinder::centerText()
{
    m_keyText.setXPosition((getWidth() - m_keyText.getWidth()) / 2);
    m_keyText.setYPosition((getHeight() - m_keyText.getHeight()) / 2);
}

float KeyBinder::getWidth() const
{
    if (m_focused)
        return m_focusedBack.getWidth() * m_focusedBack.getXScale();
    
    return m_inactiveBack.getWidth() * m_inactiveBack.getXScale();
}

float KeyBinder::getHeight() const
{
    if (m_focused)
        return m_focusedBack.getHeight() * m_focusedBack.getYScale();
    
    return m_inactiveBack.getHeight() * m_inactiveBack.getYScale();
}
