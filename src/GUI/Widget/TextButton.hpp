/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXT_BUTTON
#define DEF_TEXT_BUTTON

#include "Button.hpp"
#include "Graphics/Drawable/Text.hpp"

class TextButton : public Button
{
    public:
        TextButton(const SkinBox &skin = SkinBox(), const SkinSound &sounds = SkinSound());
        TextButton(const std::string &text, const SkinBox &skin = SkinBox(), const SkinSound &sounds = SkinSound());
        void draw();
        
        void setFocus(bool focused);
        void setSkin(const SkinBox &skin);
        
        void setText(const std::string &text);
        const std::string &getText() const;
        
        void setTextFont(Font &textFont);
        const Font *getTextFont() const;
        
        void setTextColor(const Color &textColor);
        const Color &getTextColor() const;
    
    private:
        void centerText();
        
        Text m_textField;
};

#endif
