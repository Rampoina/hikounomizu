/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_BUTTON
#define DEF_BUTTON

#define NO_SOUND ""

#include "Widget.hpp"
#include "Graphics/Drawable/Sprite.hpp"

#include "Engines/Sound/SoundEngine.hpp"

struct SkinBox
{
    SkinBox() : box( Box(0, 0, 100, 50) )
    {
        
    }
    
    SkinBox(float width, float height) : box( Box(0, 0, width, height) )
    {
        
    }
    
    SkinBox(float width, float height, const Sprite &inactive, const Sprite &focused) :
    box( Box(0, 0, width, height) ), inactive(inactive), focused(focused)
    {
        
    }
    
    SkinBox(float width, float height, const Sprite &inactive, const Sprite &focused, const Sprite &selected) :
    box( Box(0, 0, width, height) ), inactive(inactive), focused(focused), selected(selected)
    {
        
    }
    
    Box box;
    Sprite inactive, focused, selected;
};

struct SkinSound
{
    SkinSound() : engine(NULL), focus(NO_SOUND), click(NO_SOUND)
    {
        
    }
    
    SkinSound(SoundEngine &soundEngine, const std::string &focusSound, const std::string &clickSound) :
    engine(&soundEngine), focus(focusSound), click(clickSound)
    {
        
    }
    
    SoundEngine *engine; //To handle sounds on events
    std::string focus, click;
};

class Button : public Widget
{
    public:
        Button(const SkinBox &skin = SkinBox(), const SkinSound &sounds = SkinSound());
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        void select(bool select);
        bool isSelected() const;
        
        void setSkin(const SkinBox &skin);
        const SkinBox &getSkin() const;
        
        void setSoundSkin(const SkinSound &soundSkin);
        const SkinSound &getSoundSkin() const;
        
        float getWidth() const;
        float getHeight() const;
    
    protected:
        Sprite m_inactiveBack, m_focusedBack, m_selectedBack;
        SkinBox m_skin;
        
        bool m_selected;
        SkinSound m_sounds;
};

#endif
