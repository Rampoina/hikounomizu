/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TAB
#define DEF_TAB

#include "Widget.hpp"

#include "Graphics/Drawable/Polygon.hpp"
#include <vector>

//Tab : abstract class representing a fixed-size frame.
//Used as a container for other widgets (menus, option buttons...)
//in order to build more complex interfaces (options, end of a fight...)
class Tab : public Widget
{
    public:
        Tab();
        Tab(float width, float height, const Color &color);
        void draw();
        
        virtual bool mouseRelease(const Vector &mouseCoords) = 0;
        virtual bool mouseClick(const Vector &mouseCoords) = 0;
        virtual bool mouseMove(const Vector &mouseCoords) = 0;
        
        float getWidth() const;
        float getHeight() const;
    
    protected:
        Polygon m_background;
        float m_width, m_height;
};

#endif
