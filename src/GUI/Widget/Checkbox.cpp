/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Checkbox.hpp"

Checkbox::Checkbox() : Widget(), m_checked(false), m_size(20), m_soundEngine(NULL), m_clickSound(NO_SOUND)
{
    m_checkbox = Polygon::rectangle(0, 0, m_size, m_size, Color(), Color());
    m_checkbox.setBorderSize(2);
    
    updateBox();
}

Checkbox::Checkbox(bool checked) : Widget(), m_checked(checked), m_size(20), m_soundEngine(NULL), m_clickSound(NO_SOUND)
{
    m_checkbox = Polygon::rectangle(0, 0, m_size, m_size, Color(), Color());
    m_checkbox.setBorderSize(2);
    
    updateBox();
}

void Checkbox::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_checkbox.draw();
    
    Drawable::popMatrix();
}

bool Checkbox::mouseRelease(const Vector &mouseCoords)
{
    if ( Widget::contains( Widget::localCoords(mouseCoords) ) )
    {
        //Launch click sound
        if (m_soundEngine != NULL && m_clickSound != NO_SOUND)
            m_soundEngine->playSound(m_clickSound);
        
        m_checked = !m_checked;
        updateBox();
        
        return true;
    }
    
    return false;
}

bool Checkbox::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool Checkbox::mouseMove(const Vector &mouseCoords)
{
    if ( Widget::contains( Widget::localCoords(mouseCoords) ) )
    {
        setFocus(true);
        return true;
    }
    
    setFocus(false);
    return false;
}

void Checkbox::setFocus(bool focused)
{
    if (m_focused != focused)
    {
        Widget::setFocus(focused);
        
        //Update widget
        if (m_focused)
        {
            m_checkbox.setScale(1.1);
            m_checkbox.setOrigin(.05 * m_size, .05 * m_size);
        }
        else
        {
            m_checkbox.setScale(1);
            m_checkbox.setOrigin(0, 0);
        }
    }
}

void Checkbox::check(bool checked)
{
    m_checked = checked;
    updateBox();
}

bool Checkbox::isChecked() const
{
    return m_checked;
}

void Checkbox::setSoundEngine(SoundEngine &soundEngine)
{
    m_soundEngine = &soundEngine;
}

const SoundEngine &Checkbox::getSoundEngine() const
{
    return *m_soundEngine;
}

void Checkbox::setClickSound(const std::string &clickSound)
{
    m_clickSound = clickSound;
}

const std::string &Checkbox::getClickSound() const
{
    return m_clickSound;
}

void Checkbox::setSize(float size)
{
    if (size > 0)
    {
        m_size = size;
        
        m_checkbox.setCoords(m_size, 0, 1);
        m_checkbox.setCoords(m_size, m_size, 2);
        m_checkbox.setCoords(0, m_size, 3);
    }
}

void Checkbox::setColor(const Color &color)
{
    m_color = color;
    updateBox();
}

float Checkbox::getWidth() const
{
    return m_size;
}

float Checkbox::getHeight() const
{
    return m_size;
}

void Checkbox::updateBox()
{
    Color boxColor = m_color;
    if (!m_checked) boxColor = Color(0, 0, 0, 0);
    
    for (unsigned int i = 0; i < m_checkbox.getPoints().size(); i++)
    {
        m_checkbox.setColor(boxColor, i);
        m_checkbox.setBorderColor(Color(), i);
    }
}
