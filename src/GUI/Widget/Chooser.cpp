/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Chooser.hpp"

Chooser::Chooser() : Widget(), m_selection(0), m_opened(false)
{
    
}

Chooser::Chooser(const std::vector<std::string> &data, unsigned int selection) :
Widget(), m_selection(0), m_opened(false)
{
    setData(data, selection);
}

Chooser::Chooser(const ChooserSkin &skin, const std::vector<std::string> &data, unsigned int selection) :
Widget(), m_selection(0), m_opened(false)
{
    setSkin(skin);
    setData(data, selection);
}

void Chooser::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    if (!m_opened && m_choicesList.size() > m_selection)
        m_choicesList[m_selection].draw();
    else
    {
        for (unsigned int i = 0; i < m_choicesList.size(); i++)
            m_choicesList[i].draw();
    }
    
    Drawable::popMatrix();
}

bool Chooser::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    bool released = Widget::contains(localMouse);
    
    if (m_choicesList.size() > m_selection && m_choicesList[m_selection].mouseRelease(localMouse))
        m_opened = !m_opened;
    else if (m_opened)
    {
        for (unsigned int i = 0; i < m_choicesList.size(); i++)
        {
            if (i != m_selection && m_choicesList[i].mouseRelease(localMouse))
            {
                setSelection(i);
                break;
            }
        }
    }
    
    return released;
}

bool Chooser::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool Chooser::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (!m_opened && m_choicesList.size() > m_selection)
        m_choicesList[m_selection].mouseMove(localMouse);
    else
    {
        for (unsigned int i = 0; i < m_choicesList.size(); i++)
            m_choicesList[i].mouseMove(localMouse);
    }
    
    //Chooser focus handling
    if ( Widget::contains(localMouse) )
    {
        setFocus(true);
        return true;
    }
    
    setFocus(false);
    return false;
}

void Chooser::setData(const std::vector<std::string> &data, unsigned int selection)
{
    if (data.size() > selection)
    {
        m_choicesList.clear();
        for (unsigned int i = 0; i < data.size(); i++)
        {
            TextButton choice(data[i], m_choiceSkin.skin, m_choiceSkin.sounds);
            choice.setTextFont((*m_choiceSkin.font));
            choice.setTextColor(m_choiceSkin.textColor);
            
            m_choicesList.push_back(choice);
        }
        
        setSelection(selection);
    }
}

void Chooser::setSkin(const ChooserSkin &skin)
{
    m_choiceSkin = skin;
}

void Chooser::setSelection(unsigned int selection)
{
    if (m_choicesList.size() > selection)
    {
        m_selection = selection;
        
        m_choicesList[m_selection].select(true);
        m_choicesList[m_selection].setYPosition(0);
        float yPosition = m_choicesList[m_selection].getHeight();
        
        for (unsigned int i = 0; i < m_choicesList.size(); i++)
        {
            if (i != m_selection)
            {
                m_choicesList[i].select(false);
                m_choicesList[i].setYPosition(yPosition);
                yPosition += m_choicesList[i].getHeight();
            }
        }
        
        //Close the Chooser
        m_opened = false;
    }
}

unsigned int Chooser::getSelection() const
{
    return m_selection;
}

std::string Chooser::getSelectionStr() const
{
    return (m_choicesList.size() > m_selection) ? m_choicesList[m_selection].getText() : "?";
}

float Chooser::getWidth() const
{
    return m_choiceSkin.skin.box.width;
}

float Chooser::getHeight() const
{
    return (!m_opened) ? m_choiceSkin.skin.box.height :
                        m_choiceSkin.skin.box.height * m_choicesList.size();
}
