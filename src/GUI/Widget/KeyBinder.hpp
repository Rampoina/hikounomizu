/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEY_BINDER
#define DEF_KEY_BINDER

#define NO_SOUND ""

#include "Widget.hpp"

#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Text.hpp"

#include "Engines/Sound/SoundEngine.hpp"
#include "Tools/Input/Key.hpp"

///KeyBinder : Widget to bind a key.
///Shows the current binded key and allows
///to change it using focus.
class KeyBinder : public Widget
{
    public:
        KeyBinder();
        KeyBinder(const Key &key);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        void keyDown(const Key &key);
        const Key &getKey() const;
        
        void setFocus(bool focused);
        
        void setTextFont(Font &textFont);
        const Font *getTextFont() const;
        
        void setInactiveBack(const Sprite &inactive);
        const Sprite &getInactiveBack() const;
        
        void setFocusedBack(const Sprite &focused);
        const Sprite &getFocusedBack() const;
        
        void setSoundEngine(SoundEngine &soundEngine);
        const SoundEngine &getSoundEngine() const;
        
        void setFocusSound(const std::string &focusSound);
        const std::string &getFocusSound() const;
        
        void setKeySetSound(const std::string &keySetSound);
        const std::string &getKeySetSound() const;
        
        float getWidth() const;
        float getHeight() const;
        
    private:
        void setKey(const Key &key);
        void centerText();
        
        Text m_keyText; //Key name text
        Sprite m_inactiveBack, m_focusedBack;
        
        Key m_key; //Represented key
        
        SoundEngine *m_soundEngine; //To handle sounds on events
        std::string m_focusSound, m_keySetSound;
};

#endif
