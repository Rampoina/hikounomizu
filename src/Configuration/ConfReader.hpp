/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CONF_READER
#define DEF_CONF_READER

#include <fstream>
#include <sstream>
#include <map>

#ifndef CONF_READER_DISABLE_PLAYERKEYS
    #include "Player/Human/PlayerKeys.hpp"
#endif

//Class allowing to read conf items from a file.
class ConfReader
{
    public:
        ConfReader();
        bool load(const std::string &filePath);
        
        bool getItem(const std::string &elementKey, std::string &target);
        
        bool getIntItem(const std::string &elementKey, int &target);
        bool getBoolItem(const std::string &elementKey, bool &target);

#ifndef CONF_READER_DISABLE_PLAYERKEYS
        void getPlayerKeysList(PlayerKeys &target, const std::string &deviceName);
#endif

    private:
        std::map<std::string, std::string> m_confItems;
};

#endif
