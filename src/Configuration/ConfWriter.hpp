/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CONF_WRITER
#define DEF_CONF_WRITER

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "Player/Human/PlayerKeys.hpp"

//Class allowing to write conf items into a file.
class ConfWriter
{
    public:
        ConfWriter();
        
        void addItem(const std::string &name, const std::string &value);
        
        void addIntItem(const std::string &name, int value);
        void addBoolItem(const std::string &name, bool value);
        void addPlayerKeysList(const PlayerKeys &value);
        
        void writeFile(const std::string &filePath);
    
    private:
        std::vector< std::pair<std::string, std::string> > m_confItems;
};

#endif
