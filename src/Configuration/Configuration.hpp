/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CONFIGURATION
#define DEF_CONFIGURATION

#include <sstream>
#include <map>

#include "Configuration/ConfWriter.hpp"
#include "Configuration/ConfReader.hpp"

#include "Tools/Directory.hpp"
#include "Player/Human/PlayerKeys.hpp"

//Class managing configuration.
//It can edit, load and save conf items
class Configuration
{
    public:
        Configuration();
        void load();
        void save();
        
        //Get and set methods
            //Video
        void setWindowWidth(int windowWidth);
        int getWindowWidth() const;
        
        void setWindowHeight(int windowHeight);
        int getWindowHeight() const;
        
        void setFullscreen(bool fullscreen);
        bool getFullscreen() const;
        
        void setFrameRate(int frameRate);
        int getFrameRate() const;
        
            //Audio
        void setMasterVolume(int masterVolume);
        int getMasterVolume() const;
        
        void setMusicVolume(int musicVolume);
        int getMusicVolume() const;
        
        void setSoundVolume(int soundVolume);
        int getSoundVolume() const;
        
            //Misc
        void setDebugMode(bool debugMode);
        bool getDebugMode() const;
        
        void setCameraDynamic(bool dynamicCamera);
        bool isCameraDynamic() const;
        
            //Keys binding
        void setPlayerKeys(unsigned int ix, const PlayerKeys &playerKeys);
        bool getPlayerKeys(unsigned int ix, PlayerKeys &target) const;
        
        void setPlayerJoystickKeys(unsigned int ix, std::string joystickName, const PlayerKeys &playerKeys);
        int getPlayerJoystickKeys(unsigned int ix, std::string joystickName, PlayerKeys &target) const;
        
        void updatePlayerDevices(int removedDevice);
        void setPlayerDevice(unsigned int ix, int deviceID);
        int getPlayerDevice(unsigned int ix) const;
        
    private:        
        std::string m_cfgDirectory;
        
        //Parameters
        int m_windowWidth, m_windowHeight, m_frameRate;
        bool m_fullscreen, m_debugMode, m_dynamicCamera;
        
        int m_masterVolume, m_musicVolume, m_soundVolume;
        
        std::map<unsigned int, int> m_playerDevices; //Keyboard ? Joystick ?
        std::map<unsigned int, PlayerKeys> m_playerKeys;
        std::map<unsigned int, std::map<std::string, PlayerKeys> > m_playerJoystickKeys;
};

#endif
