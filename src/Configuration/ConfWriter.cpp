/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ConfWriter.hpp"

ConfWriter::ConfWriter()
{
    
}

void ConfWriter::addItem(const std::string &name, const std::string &value)
{
    m_confItems.push_back( make_pair(name, value) );
}

void ConfWriter::addIntItem(const std::string &name, int value)
{
    std::ostringstream stringStream;
    stringStream << value;
    
    addItem(name, stringStream.str());
}

void ConfWriter::addBoolItem(const std::string &name, bool value)
{
    std::ostringstream stringStream;
    stringStream << value;
    
    addItem(name, stringStream.str());
}

void ConfWriter::addPlayerKeysList(const PlayerKeys &value)
{
    //Set elements
    addItem("punch", value.getPunchKey().getKeyCode());
    addItem("kick", value.getKickKey().getKeyCode());
    addItem("throwWeapon", value.getThrowWeaponKey().getKeyCode());
    addItem("jump", value.getJumpKey().getKeyCode());
    addItem("moveLeft", value.getMoveLeftKey().getKeyCode());
    addItem("moveRight", value.getMoveRightKey().getKeyCode());
    addItem("crouch", value.getCrouchKey().getKeyCode());
}

void ConfWriter::writeFile(const std::string &filePath)
{
    //Open file
    std::ofstream file;
    file.open(filePath.c_str(), std::ios::out | std::ios::trunc);
    
    if (!file.fail())
    {
        std::vector< std::pair<std::string, std::string> >::iterator it;
        
        for (it = m_confItems.begin(); it != m_confItems.end(); it++)
            file << it->first << " " << it->second << std::endl;
        
        file.close();
    }
}
