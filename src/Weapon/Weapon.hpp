/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON
#define DEF_WEAPON

#include "Engines/Physics/PhysicsObject.hpp"
#include "Engines/Sound/SoundEngine.hpp"

class WeaponMedium;

///PhysicsObject's inheriting class representing a throwable weapon.
class Weapon : public PhysicsObject
{
    public:
        Weapon(WeaponMedium &medium, int id);
        virtual ~Weapon();
        
        void queueForRemoval();
        void setSoundEngine(SoundEngine &soundEngine);
        
        virtual void draw() = 0;
        virtual void updateGraphics(float frameTime) = 0;
        
        void collideWorld(bool ground);
        void collide(PhysicsObject &obj);
        virtual void takeAHit(float strength, int side, const PhysicsObject *source=NULL) = 0;
    
    protected:
        WeaponMedium *m_medium;
        int m_inMediumID;
        
        SoundEngine *m_soundEngine;
        bool m_disabled;
};

#endif
