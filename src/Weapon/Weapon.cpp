/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Weapon.hpp"
#include "Weapon/WeaponMedium.hpp"

/// The medium is responsible for providing a valid and unique ID.
Weapon::Weapon(WeaponMedium &medium, int id) :
PhysicsObject(), m_soundEngine(NULL), m_disabled(false)
{
    m_medium = &medium;
    m_inMediumID = id;
}

Weapon::~Weapon()
{
    
}

void Weapon::collideWorld(bool ground)
{
    //Play ground impact sound
    if (ground && !m_disabled && m_physicsWorld != NULL && m_soundEngine != NULL)
    {
        std::string groundMaterial = m_physicsWorld->getGroundMaterial();
        if (groundMaterial != MATERIAL_NONE)
            m_soundEngine->playSoundEffect("impact_" + groundMaterial, CHARACTER_AGNOSTIC);
    }
    
    setVelocity(Vector(0, 0));
    m_disabled = true;
    
    queueForRemoval();
}

void Weapon::collide(PhysicsObject &obj)
{
    if (!m_disabled)
    {
        obj.takeAHit(5, (getVelocity().x > 0) ? 0 : 1, this);
        
        setVelocity(Vector(0, 0));
        m_disabled = true;
    }
    
    queueForRemoval();
}

void Weapon::queueForRemoval()
{
    //Tell the weapons' tracker that this weapon is ready to be removed next frame.
    m_medium->queueForRemoval(m_inMediumID);
}

void Weapon::setSoundEngine(SoundEngine &soundEngine)
{
    m_soundEngine = &soundEngine;
}
