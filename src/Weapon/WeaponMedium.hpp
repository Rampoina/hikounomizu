/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_WEAPON_MEDIUM
#define DEF_WEAPON_MEDIUM

#include "Engines/Physics/PhysicsWorld.hpp"
#include "Engines/Resources/TextureManager.hpp"

#include "Weapon/Weapon.hpp"
#include "Weapon/Shuriken.hpp"

#include <map>
#include <list>

#define WEAPON_SHURIKEN 0

#define WEAPON_THROWING_DISTANCE_X 50
#define WEAPON_THROWING_DISTANCE_Y 20

class Player;

struct WeaponSlots
{
    WeaponSlots()
    {
        slots[WEAPON_SHURIKEN] = 0;
    }
    
    WeaponSlots(unsigned int shurikens)
    {
        slots[WEAPON_SHURIKEN] = shurikens;
    }
    
    std::map<int, unsigned int> slots; /// By weapon type, how many weapons available.
};


///WeaponMedium : Medium keeping track of weapons in game.
///Allows to throw and delete weapons properly.
class WeaponMedium
{
    public:
        WeaponMedium(const std::vector<const Player*> &players, TextureManager &textureManager, SoundEngine &soundEngine);
        ~WeaponMedium();
        
        void update(float frameTime);
        void drawWeapons();
        
        void queueForRemoval(int weaponID);
        void requestWeapon(int weaponID, const Player &player);
        
        void throwShuriken(PhysicsWorld &world, const Player &player);
        
        bool getSlots(const Player &player, WeaponSlots &dest) const;
    
    private:
        std::map<const Player*, WeaponSlots> m_playerSlots;
        std::map<int, Weapon*> m_weaponsList;
        
        /// Weapons to be removed and the potential new player getting it.
        std::map<int, const Player*> m_toBeRemoved;
        
        int m_weaponIx; ///Always a free weapon index
        
        TextureManager *m_textureManager;
        SoundEngine *m_soundEngine;
};

#endif
