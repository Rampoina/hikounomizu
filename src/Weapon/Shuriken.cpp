/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Shuriken.hpp"

Shuriken::Shuriken(WeaponMedium &medium, int id, TextureManager &textureManager) : Weapon(medium, id), m_rotationRate(3.f)
{
    m_shuriken.setTexture( (*textureManager.getTexture("gfx/weapons/shuriken.png")) );
    PhysicsObject::setAirPenetration(.9f);
}

void Shuriken::setPhysicsWorld(PhysicsWorld &physicsWorld)
{
    PhysicsObject::setPhysicsWorld(physicsWorld);
    
    //Init physics box
    setWidth(SHURIKEN_WIDTH);
    setHeight(SHURIKEN_HEIGHT);
}

void Shuriken::draw()
{
    m_shuriken.setXPosition(m_box.left + m_box.width / 2.f);
    m_shuriken.setYPosition(m_box.top + m_box.height / 2.f);
    
    m_shuriken.draw();
}

void Shuriken::updateGraphics(float frameTime)
{
    float rotationDiff = 360.f * m_rotationRate * frameTime/1000.f;
    if (getVelocity().x < 0) rotationDiff = -rotationDiff;
    
    m_shuriken.setRotation( m_shuriken.getRotation() + rotationDiff );
}

void Shuriken::setWidth(float width, bool reverseSide)
{
    PhysicsObject::setWidth(width, reverseSide);
    resizeSprite();
}

void Shuriken::setHeight(float height, bool reverseSide)
{
    PhysicsObject::setHeight(height, reverseSide);
    resizeSprite();
}

void Shuriken::takeAHit(float /*strenght*/, int /*side*/, const PhysicsObject* /*source*/)
{
    //Nothing to do :)
}

void Shuriken::resizeSprite()
{
    //Resize sprite to platform size
    m_shuriken.setXScale(m_box.width / m_shuriken.getWidth());
    m_shuriken.setYScale(m_box.height / m_shuriken.getHeight());
    
    m_shuriken.setOrigin(m_box.width / 2.f, m_box.height / 2.f);
}
