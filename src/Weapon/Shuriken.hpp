/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SHURIKEN
#define DEF_SHURIKEN

#define SHURIKEN_WIDTH 30
#define SHURIKEN_HEIGHT 30

#include "Weapon.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include "Engines/Resources/TextureManager.hpp"

//PhysicsObject's inheriting class representing a throwable weapon.
class Shuriken : public Weapon
{
    public:
        Shuriken(WeaponMedium &medium, int id, TextureManager &textureManager);
        void setPhysicsWorld(PhysicsWorld &physicsWorld);
        
        void draw();
        void updateGraphics(float frameTime);
        
        void setWidth(float width, bool reverseSide = false);
        void setHeight(float height, bool reverseSide = false);
        
        void takeAHit(float /*strength*/, int /*side*/, const PhysicsObject* /*source*/);
    
    private:
        Sprite m_shuriken;
        float m_rotationRate; /// Number of rotations per second
        
        void resizeSprite();
};

#endif
