/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FONT_MANAGER
#define DEF_FONT_MANAGER

#include "Graphics/Resources/Font.hpp"

#include <map>
#include <string>

//Class storing requested fonts so as to load them only once.
class FontManager
{
    public:
        FontManager();
        ~FontManager();
        
        Font *getFont(const std::string &path, unsigned int size);
    
    private:
        void loadFont(const std::string &path, unsigned int size);
        bool isFontLoaded(const std::string &path, unsigned int size);
        
        std::map< std::pair<std::string, unsigned int>, Font* > m_fontsList;
        FT_Library m_library;
};

#endif
