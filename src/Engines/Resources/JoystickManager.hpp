/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYSTICK_MANAGER
#define DEF_JOYSTICK_MANAGER

#define JOYSTICK_LOAD_ERROR -1

#include <SDL2/SDL.h>

#include <vector>
#include <map>
#include <string>
#include <iostream>

//Class storing requested joysticks so as to load them only once.
class JoystickManager
{
    public:
        JoystickManager();
        ~JoystickManager();
        void free();
        
        int loadJoystick(int joyIndexID);
        void removeJoystick(int joyInstanceID);
        
        bool isLoaded(int joyInstanceID) const;
        std::string getName(int joyInstanceID) const;
        
        std::vector< std::pair<int, std::string> > getLoadedJoysticksData() const;
        void loadJoysticks();
    
    private:
        std::map<int, SDL_Joystick*> m_joysticksList;
};

#endif
