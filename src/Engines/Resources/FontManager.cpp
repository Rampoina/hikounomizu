/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FontManager.hpp"

FontManager::FontManager()
{
    FT_Init_FreeType(&m_library);
}

FontManager::~FontManager()
{
    std::map< std::pair<std::string, unsigned int>, Font* >::iterator it;
    for (it = m_fontsList.begin(); it != m_fontsList.end(); it++)
        delete it->second;
    
    FT_Done_FreeType(m_library);
}

Font *FontManager::getFont(const std::string &path, unsigned int size)
{
    if (!isFontLoaded(path, size))
        loadFont(path, size);
    
    return m_fontsList[ std::make_pair(path, size) ];
}

void FontManager::loadFont(const std::string &path, unsigned int size)
{
    Font *loadedFont = new Font(m_library, path, size);
    m_fontsList[ std::make_pair(path, size) ] = loadedFont;
}

bool FontManager::isFontLoaded(const std::string &path, unsigned int size)
{
    std::map< std::pair<std::string, unsigned int>, Font* >::iterator it;
    it = m_fontsList.find( std::make_pair(path, size) );
    
    if (it != m_fontsList.end())
        return true;
    
    return false;
}
