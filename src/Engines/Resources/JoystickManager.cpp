/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JoystickManager.hpp"

JoystickManager::JoystickManager()
{
    
}

JoystickManager::~JoystickManager()
{
    free();
}

void JoystickManager::free()
{
    std::map<int, SDL_Joystick*>::iterator it;
    for (it = m_joysticksList.begin(); it != m_joysticksList.end(); it++)
        SDL_JoystickClose(it->second);
    
    m_joysticksList.clear();
}

int JoystickManager::loadJoystick(int joyIndexID)
{    
    //Loading joystick.
    SDL_JoystickEventState(SDL_ENABLE);
    SDL_Joystick *sdlJoy = SDL_JoystickOpen(joyIndexID);
    if (sdlJoy == NULL)
        return JOYSTICK_LOAD_ERROR;
    
    int instanceID = SDL_JoystickInstanceID(sdlJoy);
    
    //Joystick already loaded.
    if (isLoaded(instanceID))
        return JOYSTICK_LOAD_ERROR;
    
    m_joysticksList[instanceID] = sdlJoy;
    
    std::cout << "Loaded joystick: " << joyIndexID << " instance: " << instanceID << std::endl;
    
    return instanceID;
}

void JoystickManager::removeJoystick(int joyInstanceID)
{
    if (isLoaded(joyInstanceID))
    {
        SDL_JoystickClose(m_joysticksList[joyInstanceID]);
        m_joysticksList.erase(joyInstanceID);
        
        std::cout << "Removed joystick: " << joyInstanceID << std::endl;
    }
}

bool JoystickManager::isLoaded(int joyInstanceID) const
{
    std::map<int, SDL_Joystick*>::const_iterator it;
    it = m_joysticksList.find(joyInstanceID);
    
    if (it != m_joysticksList.end())
        return true;
    
    return false;
}

std::string JoystickManager::getName(int joyInstanceID) const
{
    std::map<int, SDL_Joystick*>::const_iterator it;
    it = m_joysticksList.find(joyInstanceID);
    
    if (it != m_joysticksList.end())
    {
        const char* name = SDL_JoystickName(it->second);
        return (name != NULL) ? std::string(name) : "undefined";
    }
    
    return "undefined";
}

std::vector< std::pair<int, std::string> > JoystickManager::getLoadedJoysticksData() const
{
    std::vector< std::pair<int, std::string> > joystickData;
    
    std::map<int, SDL_Joystick*>::const_iterator it;
    for (it = m_joysticksList.begin(); it != m_joysticksList.end(); it++)
        joystickData.push_back( std::make_pair(it->first, getName(it->first)) );
    
    return joystickData;
}

void JoystickManager::loadJoysticks()
{
    for (int i = 0; i < SDL_NumJoysticks(); i++)
        loadJoystick(i);
}
