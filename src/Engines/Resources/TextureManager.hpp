/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_TEXTURE_MANAGER
#define DEF_TEXTURE_MANAGER

#include "Graphics/Resources/Texture.hpp"

#include <map>
#include <string>

//Class storing requested textures so as to load them only once.
class TextureManager
{
    public:
        TextureManager();
        ~TextureManager();
        
        Texture *getTexture(const std::string &texturePath, int filter=MINMAG_LINEAR);
    
    private:
        void loadTexture(const std::string &texturePath, int filter);
        bool isTextureLoaded(const std::string &texturePath);
        
        std::map<std::string, Texture*> m_texturesList;
};

#endif
