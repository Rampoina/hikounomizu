/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_GAME_ENGINE
#define DEF_GAME_ENGINE

#include <cstdlib>
#include <ctime>

#include "Graphics/Window.hpp"
#include "Configuration/Configuration.hpp"

#include "Engines/Sound/PlaylistPlayer.hpp"

#include "Screens/MainMenu/MainMenu.hpp"
#include "Screens/Options/Options.hpp"
#include "Screens/CharactersMenu/CharactersMenu.hpp"
#include "Screens/ArenaMenu/ArenaMenu.hpp"
#include "Screens/Fight/Fight.hpp"

//Class allowing to manage and ordain the screens.
class GameEngine
{
    public:
        GameEngine(Window &window, Configuration &configuration, JoystickManager &joyManager);
        ~GameEngine();
        
        //Main menu
        void initMainMenu();
        
        //Options
        void initOptions();
        
        //Characters menu
        void confCharactersMenu_playersNo(int playersNo);
        void initCharactersMenu();
        
        //Arena menu
        void initArenaMenu();
        
        //Fight screen
        void confFight_arena(const std::string &arenaName);
        void confFight_players(const std::vector<std::string> &playersName, const std::vector<int> &playersType);
        void confFight_mode(int mode);
        void initFight();
        
        void launchNextScreen();
        
        void exit();
        bool wasExited() const;
        
        Window *getWindow() const;
        Configuration *getConfiguration() const;
        JoystickManager &getJoystickManager() const;
    
    private:
        void setNextScreen(Screen *nextScreen);
        
        Window *m_window;
        Configuration *m_configuration;
        JoystickManager *m_joyManager;
        
        Screen *m_nextScreen;
        
        MainMenu *m_mainMenu;
        Options *m_options;
        CharactersMenu *m_charactersMenu;
        ArenaMenu *m_arenaMenu;
        Fight *m_fight;
        
        bool m_exited;
};

#endif
