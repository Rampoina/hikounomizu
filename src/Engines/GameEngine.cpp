/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GameEngine.hpp"

GameEngine::GameEngine(Window &window, Configuration &configuration, JoystickManager &joyManager) :
m_window(&window), m_configuration(&configuration), m_joyManager(&joyManager), m_nextScreen(NULL), m_exited(false)
{
    //Initialize screens
    m_mainMenu = new MainMenu(this);
    m_options = new Options(this);
    m_charactersMenu = new CharactersMenu(this);
    m_arenaMenu = new ArenaMenu(this);
    m_fight = new Fight(this);
    
    //Initialize random number generation
    srand(time(NULL));
}

GameEngine::~GameEngine()
{
    delete m_mainMenu;
    delete m_options;
    delete m_charactersMenu;
    delete m_arenaMenu;
    delete m_fight;
}

//Main menu
void GameEngine::initMainMenu()
{
    setNextScreen(m_mainMenu);
}

//Options
void GameEngine::initOptions()
{
    setNextScreen(m_options);
}

//Characters menu
void GameEngine::confCharactersMenu_playersNo(int playersNo)
{
    m_charactersMenu->setPlayersNo(playersNo);
}

void GameEngine::initCharactersMenu()
{
    setNextScreen(m_charactersMenu);
}

//Arena menu
void GameEngine::initArenaMenu()
{
    setNextScreen(m_arenaMenu);    
}

//Fight screen
void GameEngine::confFight_arena(const std::string &arenaName)
{
    m_fight->setArena(arenaName);
}

void GameEngine::confFight_players(const std::vector<std::string> &playersName, const std::vector<int> &playersType)
{
    m_fight->clearPlayers();
    
    for (unsigned int i = 0; i < playersName.size(); i++)
        m_fight->addPlayer(playersName[i], playersType[i]);
}

void GameEngine::confFight_mode(int mode)
{
    m_fight->setMode(mode);
}

void GameEngine::initFight()
{
    setNextScreen(m_fight);
}

void GameEngine::setNextScreen(Screen *nextScreen)
{
    m_nextScreen = nextScreen;
}

void GameEngine::launchNextScreen()
{
    m_nextScreen->run();
}

void GameEngine::exit()
{
    m_exited = true;
}

bool GameEngine::wasExited() const
{
    return m_exited;
}

//Get and set methods
Window *GameEngine::getWindow() const
{
    return m_window;
}

Configuration *GameEngine::getConfiguration() const
{
    return m_configuration;
}

JoystickManager &GameEngine::getJoystickManager() const
{
    return (*m_joyManager);
}
