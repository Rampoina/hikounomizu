/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PHYSICS_WORLD
#define DEF_PHYSICS_WORLD

#define MATERIAL_NONE "none"

#include <vector>
#include <string>

#include "Structs/Box.hpp"
#include "Structs/Vector.hpp"

#include "Graphics/Drawable/Polygon.hpp" //Used for debugging

class PhysicsObject;

class PhysicsWorld
{
    public:
        PhysicsWorld();
        PhysicsWorld(const Box &worldBox, float gravity, float airFriction);
        
        void update(float frameTime);
        void debug();
        
        void addObject(PhysicsObject &object);
        void removeObject(PhysicsObject &object);
        
        void pulseObject(PhysicsObject &object, float xPulse, float yPulse);
        const std::vector<PhysicsObject*> &getObjects() const;
        
        std::vector<PhysicsObject*> hitTest(const PhysicsObject &object) const;
        std::vector<PhysicsObject*> hitTest(const PhysicsObject &object, const Box &customBox) const;
        bool isFree(const Box &box) const;
        
        void setWorldBox(const Box &worldBox);
        const Box &getWorldBox() const;
        
        void setGroundMaterial(const std::string &material);
        const std::string &getGroundMaterial() const;
        
        void setGravityForce(float gravity);
        float getGravityForce() const;
        
        void setAirFriction(float airFriction);
        float getAirFriction() const;
    
    private:
        std::vector<PhysicsObject*> m_objectsList;
        
        Box m_worldBox; ///World box
        float m_gravity; ///Gravity force in pixels / second
        float m_airFriction; ///Air friction in pixels / second
        std::string m_groundMaterial; ///Name of the ground material
};

#endif
