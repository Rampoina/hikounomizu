/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PhysicsWorld.hpp"
#include "PhysicsObject.hpp"

PhysicsWorld::PhysicsWorld() : m_gravity(0), m_airFriction(0), m_groundMaterial(MATERIAL_NONE)
{
    
}

PhysicsWorld::PhysicsWorld(const Box &worldBox, float gravity, float airFriction) :
m_worldBox(worldBox), m_gravity(gravity), m_airFriction(airFriction), m_groundMaterial(MATERIAL_NONE)
{
    
}

void PhysicsWorld::update(float frameTime)
{
    float elapsedTime = frameTime / 1000.f;
    
    for (unsigned int i = 0; i < m_objectsList.size(); i++)
    {
        PhysicsObject *object = m_objectsList[i];
        if (object->isFixed())
            continue;
        
        //Alterations
        Vector altered = object->getVelocity();
        
        float airFriction = m_airFriction * (1.f - object->getAirPenetration()) * elapsedTime;
        if (altered.x > 0)
        {
            if (altered.x - airFriction < 0) altered.x = 0;
            else altered.x -= airFriction;
        }
        else if (altered.x < 0)
        {
            if (altered.x + airFriction > 0) altered.x = 0;
            else altered.x += airFriction;
        }
        
        altered.y += m_gravity * (1.f - object->getAirPenetration()/2.f) * elapsedTime;
        
        if (altered.x != 0) object->moveXPosition(altered.x * elapsedTime);
        if (altered.y != 0) object->moveYPosition(altered.y * elapsedTime);
        
        if (object->isOnGround()) altered.y = 0;
        object->setVelocity(altered);
    }
}

void PhysicsWorld::debug()
{
    std::vector<PhysicsObject*>::iterator it;
    
    for (it = m_objectsList.begin(); it != m_objectsList.end(); it++)
    {
        Box box = (*it)->getBox();
        
        Polygon debug = Polygon::rectangle(box.left, box.top, box.width, box.height, Color(0, 0, 0, 0), Color(122, 122, 122));
        debug.draw();
    }
}

void PhysicsWorld::addObject(PhysicsObject &object)
{
    m_objectsList.push_back(&object);
}

void PhysicsWorld::removeObject(PhysicsObject &object)
{
    for (unsigned int i = 0; i < m_objectsList.size(); i++)
    {
        if (m_objectsList[i] == &object)
        {
            m_objectsList.erase(m_objectsList.begin() + i);
            i--;
        }
    }
}

void PhysicsWorld::pulseObject(PhysicsObject &object, float xPulse, float yPulse)
{
    for (unsigned int i = 0; i < m_objectsList.size(); i++)
    {
        if (m_objectsList[i] == &object)
        {
            Vector pulse = m_objectsList[i]->getVelocity();
            pulse.x += xPulse;
            pulse.y += yPulse;
            
            m_objectsList[i]->setVelocity(pulse);
        }
    }
}

const std::vector<PhysicsObject*> &PhysicsWorld::getObjects() const
{
    return m_objectsList;
}

std::vector<PhysicsObject*> PhysicsWorld::hitTest(const PhysicsObject &object) const
{
    return hitTest(object, object.getBox());
}

std::vector<PhysicsObject*> PhysicsWorld::hitTest(const PhysicsObject &object, const Box &customBox) const
{
    std::vector<PhysicsObject*> hitObjects;
    
    for (std::vector<PhysicsObject*>::const_iterator it = m_objectsList.begin(); it != m_objectsList.end(); it++)
    {
        if ((*it) != &object)
        {
            if (customBox.hits( (*it)->getBox() ))
                hitObjects.push_back((*it));
        }
    }
    
    return hitObjects;
}

bool PhysicsWorld::isFree(const Box &box) const
{
    if (!m_worldBox.contains(box))
        return false;
    
    for (std::vector<PhysicsObject*>::const_iterator it = m_objectsList.begin(); it != m_objectsList.end(); it++)
    {
        if (box.hits( (*it)->getBox() ))
            return false;
    }
    
    return true;
}

void PhysicsWorld::setWorldBox(const Box &worldBox)
{
    m_worldBox = worldBox;
}

const Box &PhysicsWorld::getWorldBox() const
{
    return m_worldBox;
}

void PhysicsWorld::setGroundMaterial(const std::string &material)
{
    m_groundMaterial = material;
}

const std::string &PhysicsWorld::getGroundMaterial() const
{
    return m_groundMaterial;
}

void PhysicsWorld::setGravityForce(float gravity)
{
    m_gravity = gravity;
}

float PhysicsWorld::getGravityForce() const
{
    return m_gravity;
}

void PhysicsWorld::setAirFriction(float airFriction)
{
    m_airFriction = airFriction;
}

float PhysicsWorld::getAirFriction() const
{
    return m_airFriction;
}
