/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PHYSICS_OBJECT
#define DEF_PHYSICS_OBJECT

#define FLOAT_ERROR .1f

#include "PhysicsWorld.hpp"
#include <iostream>
#include <cmath>

class PhysicsRoutines;

class PhysicsObject
{
    public:
        PhysicsObject();
        virtual ~PhysicsObject();
        
        void setPhysicsWorld(PhysicsWorld &physicsWorld);
        PhysicsWorld &getPhysicsWorld() const;
        
        void setPosition(float xPosition, float yPosition);
        void moveXPosition(float distance, bool push = false);
        void moveYPosition(float distance, bool push = false);
        
        virtual void setWidth(float width, bool reverseSide = false);
        virtual void setHeight(float height, bool reverseSide = false);
        
        void setXVelocity(float xVelocity);
        void setYVelocity(float yVelocity);
        void setVelocity(const Vector &velocity);
        const Vector &getVelocity() const;
        
        void setAirPenetration(float airPenetration);
        float getAirPenetration() const;
        
        void setMaterial(const std::string &material);
        const std::string &getMaterial() const;
        
        void setFixed(bool fixed);
        bool isFixed() const;
        
        float shadowProjection() const;
        virtual bool interceptsShadows() const;
        
        bool isOnGround(std::string *material=NULL) const;
        const Box &getBox() const;
        
        virtual void collideWorld(bool ground) = 0;
        virtual void collide(PhysicsObject &obj) = 0;
        virtual void takeAHit(float strength, int side, const PhysicsObject *source=NULL) = 0;
    
    protected:
        std::vector<PhysicsObject*> hitTest() const;
        std::vector<PhysicsObject*> hitTest(const Box &box) const;
        
        PhysicsWorld *m_physicsWorld;
        
        Box m_box;
        Vector m_velocity;
        float m_airPenetration; ///From 0.f: regular impact of air friction to 1.f: no impact of air friction
        std::string m_material; ///Name of the material of the object (for step sounds)
        
        bool m_fixed;
    
    private:
        bool attempt_moveXPosition(float distance, bool push = false);
        bool attempt_moveYPosition(float distance, bool push = false);
};

#endif
