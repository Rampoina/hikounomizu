/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PHYSICS_ROUTINES
#define DEF_PHYSICS_ROUTINES

#include <algorithm>
#include <vector>

#include "PhysicsObject.hpp"

class PhysicsRoutines
{
    public:
        static void soustractVectors(std::vector<PhysicsObject*> &dst, std::vector<PhysicsObject*> &src);
        
        static void sortObjectsByX(std::vector<PhysicsObject*> &objects, int checkedSide);
        static void sortObjectsByY(std::vector<PhysicsObject*> &objects, int checkedSide);
        
        static bool containsFixedObject(std::vector<PhysicsObject*> &objects);
};

    //PhysicsObjects sorting functors
struct SortByXPos
{
    SortByXPos(int checkedSide = 0) : side(checkedSide)
    {
        
    }
    
    bool operator() (const PhysicsObject *lhs, const PhysicsObject *rhs) const
    {
        const Box &lhsBox = lhs->getBox();
        const Box &rhsBox = rhs->getBox();
        
        if (side == 0)
            return lhsBox.left < rhsBox.left;
        else
            return lhsBox.left + lhsBox.width < rhsBox.left + rhsBox.width;
    }
    
    int side;
};

struct SortByYPos
{
    SortByYPos(int checkedSide = 0) : side(checkedSide)
    {
        
    }
    
    bool operator() (const PhysicsObject *lhs, const PhysicsObject *rhs) const
    {
        const Box &lhsBox = lhs->getBox();
        const Box &rhsBox = rhs->getBox();
        
        if (side == 0)
            return lhsBox.top < rhsBox.top;
        else
            return lhsBox.top + lhsBox.height < rhsBox.top + rhsBox.height;
    }
    
    int side;
};

struct FindFixed
{
    FindFixed()
    {
        
    }
    
    bool operator() (const PhysicsObject *obj) const
    {
        return obj->isFixed();
    }
};

#endif
