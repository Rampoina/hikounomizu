/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PhysicsRoutines.hpp"
#include "PhysicsObject.hpp"

void PhysicsRoutines::soustractVectors(std::vector<PhysicsObject*> &dst, std::vector<PhysicsObject*> &src)
{
    std::vector<PhysicsObject*>::iterator itDst = dst.begin();
    
    while (itDst != dst.end())
    {
        bool found = false;
        
        std::vector<PhysicsObject*>::iterator itSrc;
        for (itSrc = src.begin(); itSrc != src.end(); itSrc++)
        {
            if ((*itDst) == (*itSrc))
                found = true;
        }
        
        if (found)
            itDst = dst.erase(itDst);
        else
            itDst++;
    }
}

void PhysicsRoutines::sortObjectsByX(std::vector<PhysicsObject*> &objects, int checkedSide)
{
    std::sort(objects.begin(), objects.end(), SortByXPos(checkedSide));
}

void PhysicsRoutines::sortObjectsByY(std::vector<PhysicsObject*> &objects, int checkedSide)
{
    std::sort(objects.begin(), objects.end(), SortByYPos(checkedSide));
}

bool PhysicsRoutines::containsFixedObject(std::vector<PhysicsObject*> &objects)
{
    return (std::find_if(objects.begin(), objects.end(), FindFixed()) != objects.end());
}
