/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYLIST_PLAYER
#define DEF_PLAYLIST_PLAYER

#include <pthread.h>
#include <iostream>
#include <map>
#include <vector>

#include "Audio/Playlist.hpp"
#include "Tools/Timer.hpp"

#include "GUI/PlaylistNotifier.hpp"

#define PLAYLIST_MAIN 0

namespace PlaylistPlayer
{
    //Data used by threads
    struct PlaylistData
    {
        PlaylistData() : notifier(NULL), notifierMutex(NULL),
        next_song(false), loop_next(false), fade_in(false), stop(false)
        {
            
        }
        
        PlaylistData(const Playlist &uninitedPlaylist) : //uninitedPlaylist should be uninitialized here
        playlist(uninitedPlaylist), notifier(NULL), notifierMutex(NULL),
        next_song(false), loop_next(false), fade_in(false), stop(false)
        {
            
        }
        
        Playlist playlist;
        
        
        //Graphical notifications' handling
        PlaylistNotifier *notifier; //Graphical notifier, will be drawn from the main thread and
                                    //notified from here. (must always point to a valid and alive notifier)
        
        pthread_mutex_t *notifierMutex; //Mutex to safely handle the multithreading
        
        bool next_song; //Set to true by caller to switch to next song (fade out)
        bool loop_next; //Set to true by caller to loop the next song which will start
        bool fade_in; //Set to true to start a fade in effect
        bool stop; //Set to true by caller to terminate the playlist
    };
    
    //(*) : Uninitialized playlist, starts if not already playing
    bool startPlaylist(const Playlist &playlist, unsigned int requestedId, PlaylistNotifier *notifier=NULL, pthread_mutex_t *notifierMutex=NULL); // (*)
    void stopPlaylist(unsigned int playlistId);
    
    void playlistNextSong(unsigned int playlistId, bool loop=false);
    void playlistSetVolume(unsigned int playlistId, int musicVolume);
    void playlistSetNotifier(unsigned int playlistId, PlaylistNotifier *notifier, pthread_mutex_t *notifierMutex);
    
    bool isPlaying(unsigned int playlistId);
    void destroy();
}

#endif
