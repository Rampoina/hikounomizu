/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SoundEffectsList.hpp"

SoundEffectsList::SoundEffectsList()
{
    
}

bool SoundEffectsList::load(const std::string &xmlRelPath)
{
    m_soundEffects.clear();
    
    //TinyXML initialization
    std::string xmlPath = BuildValues::data(xmlRelPath); //Locate absolute path to xml data
    
    TiXmlDocument xmlFile;
    if (!xmlFile.LoadFile(xmlPath.c_str()))
        return false;
    
    TiXmlHandle hdl(&xmlFile);
    TiXmlElement *xmlElement = hdl.FirstChildElement().FirstChildElement().ToElement();
    
    while (xmlElement)
    {
        //Get sound effect attributes
        std::string effectName = xmlElement->Attribute("name");
        
        std::string characterName = CHARACTER_AGNOSTIC;
        xmlElement->QueryStringAttribute("character", &characterName);
        
        //Browse associated sound paths
        TiXmlElement *soundsElement = xmlElement->FirstChildElement();
        while (soundsElement)
        {
            std::string soundPath = soundsElement->Attribute("path");
            m_soundEffects[effectName][characterName].push_back("audio/sfx/" + soundPath);
            
            soundsElement = soundsElement->NextSiblingElement();
        }
        
        xmlElement = xmlElement->NextSiblingElement();
    }
    
    return true;
}

bool SoundEffectsList::getPath(const std::string &effect, const std::string &characterName, std::string &target)
{
    if (m_soundEffects.count(effect) > 0)
    {
        if (m_soundEffects[effect].count(characterName) > 0)
        {
            //A character-specific set of sound paths is available
            chooseSoundAmong(m_soundEffects[effect][characterName], target);
            return true;
        }
        else if (m_soundEffects[effect].count(CHARACTER_AGNOSTIC) > 0)
        {
            //A character-agnostic set of sound paths is available
            chooseSoundAmong(m_soundEffects[effect][CHARACTER_AGNOSTIC], target);
            return true;
        }
    }
    
    return false;
}

bool SoundEffectsList::chooseSoundAmong(const SoundPaths &paths, std::string &target)
{
    if (paths.size() > 0)
    {
        target = paths[ rand() % paths.size() ];
        return true;
    }
    
    return false;
}
