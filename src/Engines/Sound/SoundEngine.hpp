/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_ENGINE
#define DEF_SOUND_ENGINE

#include "Audio/Sound.hpp"
#include "Engines/Resources/SoundBufferManager.hpp"
#include "SoundEffectsList.hpp"

#include <list>
#include <string>
#include <cstdlib>

//Class managing sounds.
//It can store and play simultaneously an unlimited number of sounds.
class SoundEngine
{
    public:
        SoundEngine();
        
        void playSound(const std::string &soundPath, float pitch=1.f);
        void update();
        
        void playSoundEffect(const std::string &effect, const std::string &character, float pitchVariation=.15f);
        void loadSoundEffects(const std::string &effectsPath);
        
        void setVolume(float volume);
        float getVolume() const;
        
        void setManager(SoundBufferManager &manager);
        const SoundBufferManager &getManager() const;
    
    private:
        bool loadSound(const std::string &soundPath, Sound &target);
        
        std::list<Sound> m_playingSounds;
        float m_volume;
        
        SoundEffectsList m_effects;
        SoundBufferManager *m_manager;
};

#endif
