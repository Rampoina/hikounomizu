/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_SOUND_EFFECTS_LIST
#define DEF_SOUND_EFFECTS_LIST

#include <map>
#include <vector>
#include <string>
#include <cstdlib>

#include <tinyxml.h>
#include "Tools/BuildValues.hpp" ///Generated at build time

#define CHARACTER_AGNOSTIC ""

///SoundPaths: A List of paths to sounds matching a sound effect key.
typedef std::vector<std::string> SoundPaths;

///SoundEffect: A map of SoundPaths objects,
/// separated by character name, or character-agnostic.
typedef std::map<std::string, SoundPaths> SoundEffect;

///SoundEffectsList: A class to associate abstract sound effect names
/// with one or several character-specific sound paths.
class SoundEffectsList
{
    public:
        SoundEffectsList();
        
        bool load(const std::string &xmlRelPath);
        bool getPath(const std::string &effect, const std::string &characterName, std::string &target);
    
    private:
        bool chooseSoundAmong(const SoundPaths &paths, std::string &target);
        
        std::map<std::string, SoundEffect> m_soundEffects;
};

#endif
