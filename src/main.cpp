/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "Tools/BuildValues.hpp" ///Generated at build time

#include "Audio/Audio.hpp"
#include "Engines/GameEngine.hpp"
#include "Engines/Resources/JoystickManager.hpp"

int main()
{
    // Hikou no mizu
    std::cout << "Hikou no mizu " << BuildValues::VERSION << std::endl;
    
    // Initialization: Audio & Video
    if (!Audio::init())
    {
        std::cout << "OpenAL Initiation failed" << std::endl;
        return 1;
    }

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0)
    {
        std::cout << "SDL Initiation failed: " << SDL_GetError() << std::endl;
        Audio::close();
        return 1;
    }
    IMG_Init(IMG_INIT_PNG);
    
    // Configuration & Window
    Configuration confManager;
    confManager.load();
    
    Window window("Hikou no mizu");
    if (!window.create(confManager.getWindowWidth(), confManager.getWindowHeight(), confManager.getFullscreen()))
    {
        Audio::close();
        IMG_Quit();
        SDL_Quit();
        return 1;
    }
    
    window.setIcon( BuildValues::data("gfx/icon.png") );
    window.setFrameRate(confManager.getFrameRate());
    
    window.initView(confManager.getWindowWidth(), confManager.getWindowHeight());
    
    // Joystick
    JoystickManager joyManager;
    joyManager.loadJoysticks();
    
    // Game engine loop
    GameEngine gameEngine(window, confManager, joyManager);
    gameEngine.initMainMenu();
    
    while (!gameEngine.wasExited())
        gameEngine.launchNextScreen();
    
    // Save & free resources
    confManager.save();
    joyManager.free();
    
    PlaylistPlayer::destroy();
    Audio::close();
    
    window.destroy();
    
    IMG_Quit();
    SDL_Quit();
    
    return 0;
}
