/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_MOVES
#define DEF_PLAYER_MOVES

#define STATE_MAX 6

#define STATE_DEFAULT 0
#define STATE_JUMP_TRANSITION 1
#define STATE_JUMPING 2
#define STATE_MOVING 3
#define STATE_CROUCHED 4
#define STATE_FALLING 5
#define STATE_KO 6

#define ACTION_DEFAULT 0
#define ACTION_PUNCH 1
#define ACTION_KICK 2
#define ACTION_TAKEAHIT 3

#define RECOVERY_TIME 100.f //100ms between each attack
#define ANIMATION_SCALE .8f

#include <map>
#include <tinyxml.h>

#include "Engines/Resources/TextureManager.hpp"
#include "Animation/CallbackAnimation.hpp"
#include "Tools/Timer.hpp"
#include "Attack.hpp"

#include "Tools/BuildValues.hpp" ///Generated at build time

struct State
{
    State()
    {
        
    }
    
    CallbackAnimation animation;
    std::map<unsigned int, Attack> attacks;
};

class PlayerMoves : public Drawable
{
    public:
        PlayerMoves();
        void draw();
        
        void update(float frameTime);
        
        //Action
        bool mayAttack();
        
        void punch();
        void kick();
        void takeAHit();
        
        //State
        void setState(unsigned int state, bool force = false);
        void setState(unsigned int state, unsigned int targetState, bool force = false);
        
        unsigned int getAction() const;
        unsigned int getState() const;
        
        float getWidth();
        float getHeight();
        
        void load(const std::string &xmlRelPath, const std::string &animationsPath, TextureManager &textureManager, Player &player);
    
    private:
        void loadAnimation(Texture &texture, const TiXmlElement *xmlData, CallbackAnimation &dst) const;
        
        const CallbackAnimation &getAnimation();
        void updateOrigin();
        
        std::map<unsigned int, State> m_statesList;
        
        unsigned int m_state; //Current state
        unsigned int m_action; //Current action
        
        int m_targetState; //Target of a transition
        
        float m_lastAttackEndTime; //Time value recorded at the end of the last launched attack
};

#endif
