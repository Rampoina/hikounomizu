/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_KEYS
#define DEF_PLAYER_KEYS

#include "Tools/Input/Key.hpp"

//Class defining methods to get and set player's keys safely.
class PlayerKeys
{
    public:
        PlayerKeys();
        PlayerKeys(const Key &punch, const Key &kick, const Key &throwWeapon, const Key &jump, const Key &moveLeft, const Key &moveRight, const Key &crouch);
        
        void setPunchKey(const Key &punchKey);
        const Key &getPunchKey() const;
        
        void setKickKey(const Key &kickKey);
        const Key &getKickKey() const;
        
        void setThrowWeaponKey(const Key &throwWeaponKey);
        const Key &getThrowWeaponKey() const;
        
        void setJumpKey(const Key &jumpKey);
        const Key &getJumpKey() const;
        
        void setMoveLeftKey(const Key &moveLeftKey);
        const Key &getMoveLeftKey() const;
        
        void setMoveRightKey(const Key &moveRightKey);
        const Key &getMoveRightKey() const;
        
        void setCrouchKey(const Key &crouchKey);
        const Key &getCrouchKey() const;
        
        static PlayerKeys undefinedKeys();
    
    private:
        Key m_punch, m_kick;
        Key m_throwWeapon;
        Key m_jump;
        Key m_moveLeft, m_moveRight;
        Key m_crouch;
};

#endif
