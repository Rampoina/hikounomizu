/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_HUMAN
#define DEF_HUMAN

#include "Player/Player.hpp"
#include "Player/Human/PlayerKeys.hpp"
#include "Engines/Resources/JoystickManager.hpp"

#include <tinyxml.h> //static loadFromXML method
#include "Tools/BuildValues.hpp" ///Generated at build time

class Human : public Player
{
    public:
        Human();
        Human(const std::string &name, float health, float strength, float speed, float jumpEnergy, float walkPeriod);
        
        void act();
        
        void keyEvent(const SDL_Event &event); /// To be called before updateKeyState().
        void updateKeyState(const SDL_Event &event);
        
        void setDeviceID(int id);
        int getDeviceID() const;
        
        void setKeys(const PlayerKeys &keys);
        const PlayerKeys &getKeys() const;
        
        //Static methods
        static Human loadFromXML(const std::string &name, const std::string &xmlRelPath);
    
    private:
        InputMonitor m_inputMonitor;
        PlayerKeys m_keys;
        int m_deviceID;
        
        bool m_moveLeftDown, m_moveRightDown, m_crouchedDown;
};

#endif
