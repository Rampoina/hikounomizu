/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_AI
#define DEF_AI

#define BEHAVIOUR_DEFENSIVE 0
#define BEHAVIOUR_OFFENSIVE 1

#include "Player/Player.hpp"
#include "Tools/Timer.hpp"

#include <vector>
#include <cstdlib>

#include <tinyxml.h> //static loadFromXML method
#include "Tools/BuildValues.hpp" ///Generated at build time

class AI : public Player
{
    public:
        AI();
        AI(const std::string &name, float health, float strength, float speed, float jumpEnergy, float walkPeriod);
        
        void act();
        void takeAHit(float strength, int side, const PhysicsObject *source=NULL);
        
        void addEnemy(const Player &enemy);
        void clearEnemies();
        
        const std::vector<const Player*> &getEnemies() const;
        
        //Static methods
        static AI loadFromXML(const std::string &name, const std::string &xmlRelPath);
    
    private:
        void randomDefensive(); //Act defensively (random actions)
        void updateEnemies(); //Remove all KO enemies from m_enemies
        
        std::vector<const Player*> m_enemies; //All enemies
        unsigned int m_targetEnemy; //The ix of the enemy the AI is focusing on
        
        unsigned int m_behaviour;
        
        float m_attackState_start; //Starting time of the last state
                                   //State on offensive close fight (crouched, default...)
        
        bool m_attackPause; //When behaving offensively, give enemies a break sometimes ;)
        float m_attackPause_start; //Starting time of the last pause
        
        bool m_defensive_turnaway; //When behaving defensively (1on1), turn away sometimes
        float m_defensive_turnaway_start; //Starting time of the last turnaway
};

#endif
