/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER
#define DEF_PLAYER

#define NOT_MOVING 0
#define MOVING_LEFT 1
#define MOVING_RIGHT 2

#include <string>
#include "Engines/Sound/SoundEngine.hpp"

#include "Weapon/WeaponMedium.hpp"

#include "Graphics/Drawable/Polygon.hpp"
#include "Structs/Vector.hpp"

#include "PlayerMoves.hpp"
#include "Engines/Physics/PhysicsObject.hpp"

//PhysicsObject's inheriting class representing a player.
//It can move, jump, attack, ...
class Player : public PhysicsObject, public AnimationCallbackReceiver
{
    public:
        Player();
        Player(const std::string &name, float health, float strength, float speed, float jumpEnergy, float walkPeriod);
        
        void initWeapons(WeaponMedium &medium);
        
        void loadGraphics(TextureManager &textureManager);
        void draw();
        
        void update(float frameTime);
        void updateGraphics(float frameTime);
        
        void lockEnnemy(const Player *ennemy);
        virtual void act() = 0;
        
        //Attacks callback
        void hit(const HitBox &hitBox);
        void playSoundEffect(const std::string &effectName);
        
        //Animations callback
        void stepImpactCallback();
        
        //Engines methods
        void setSoundEngine(SoundEngine &soundEngine);
        
        //Get and set methods
        const std::string &getName() const;
        
        void setHealth(float health);
        float getHealth() const;
        
        void setStrength(float strength);
        float getStrength() const;
        
        void setSpeed(float speed);
        float getSpeed() const;
        
        void setJumpEnergy(float jumpEnergy);
        float getJumpEnergy() const;
        
        void setDebugHits(bool debugHits);
        bool getDebugHits() const;
        
        bool getXFlip() const;
        void getState(unsigned int &state, unsigned int &action) const;
        bool getWeapons(WeaponSlots &dest) const;
        
        void collideWorld(bool /*ground*/);
        void collide(PhysicsObject& /*obj*/);
        
        void takeAHit(float strength, int side, const PhysicsObject* /*source*/);
        bool isKo() const;
        bool isHit() const;
        bool isMoving() const;
        bool movingAwayFromEnnemy() const;
        int getAheadDirection() const;
    
    protected:
        void updatePhysicsBox();
        
        void moveStep(float frameTime);
        void setMoving(int direction);
        
        void jump();
        void crouch();
        
        void punch();
        void kick();
        void throwShuriken();
        
        std::string m_name;
        
        float m_health; //Health in life (/100)
        float m_strength; //Strength in health impact
        float m_speed; //Speed in pixels / second
        float m_jumpEnergy; //Jump energy in pixels / second
        float m_walkPeriod; //Time period between each step in seconds
        
        PlayerMoves m_moves; //Animations and attacks
        
        Polygon m_shadow; //Elliptic shadow under the player
        Vector m_shadowSize; //Shadow size
        
        SoundEngine *m_soundEngine;
        WeaponMedium *m_weapons;
        
        const Player *m_lockedEnnemy;
        
        int m_requestsMove; //Whether the player wishes to move right, left, or not at all
        bool m_awaitsImpact; //Whether the player is expecting a ground impact
        bool m_debugHits; //Should red squares appear on hit points ?
};

#endif
