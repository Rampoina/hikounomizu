/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Attack.hpp"
#include "Player.hpp"

Attack::Attack() : Drawable(), m_player(NULL)
{
    
}

void Attack::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_animation.draw();
    
    Drawable::popMatrix();
}

void Attack::update(float frameTime)
{
    float baseTime = m_animation.getCurrentTime();
    m_animation.goForward(frameTime / 1000.f);
    
    if (m_player != NULL)
    {
        //Hit code
        std::vector<HitBox>::iterator it;
        for (it = m_hitBoxes.begin(); it != m_hitBoxes.end(); it++)
        {
            if (baseTime < it->time && it->time <= m_animation.getCurrentTime()) //Time reached
                m_player->hit((*it)); //Hit Callback
        }
        
        //Sound effects
        std::vector< std::pair<float, std::string> >::iterator itSfx;
        for (itSfx = m_soundEffects.begin(); itSfx != m_soundEffects.end(); itSfx++)
        {
            if (baseTime <= itSfx->first && itSfx->first < m_animation.getCurrentTime()) //Time reached
                m_player->playSoundEffect(itSfx->second); //Callback
        }
    }
    
    //Attack is ready
    if (m_animation.getCurrentTime() == m_animation.getDuration())
        m_animation.goToTime(0.f);
}

void Attack::reinit()
{
    //Reinitialize the attack (useful to stop the attack suddenly
    //e.g: in case of KO)
    
    m_animation.goToTime(0.f);
}

bool Attack::ready()
{
    return (m_animation.getCurrentTime() == 0.f);
}

void Attack::addHitBox(const HitBox &hitBox, int hitBoxIx)
{
    if (hitBoxIx == -1)
        m_hitBoxes.push_back(hitBox);
    
    else if (hitBoxIx >= 0 && hitBoxIx <= static_cast<int>(m_hitBoxes.size()) - 1)
        m_hitBoxes[hitBoxIx] = hitBox;
}

void Attack::scaleHitBoxes(float scale)
{
    std::vector<HitBox>::iterator it;
    for (it = m_hitBoxes.begin(); it != m_hitBoxes.end(); it++)
    {
        it->box.top *= scale;
        it->box.left *= scale;
        it->box.width *= scale;
        it->box.height *= scale;
    }
}

void Attack::addSoundEffect(float time, const std::string &soundEffect)
{
    m_soundEffects.push_back( std::make_pair(time, soundEffect) );
}

void Attack::setAnimation(const CallbackAnimation &animation)
{
    m_animation = animation;
}

const CallbackAnimation &Attack::getAnimation() const
{
    return m_animation;
}

void Attack::setPlayer(Player &player)
{
    m_player = &player;
}

const Player *Attack::getPlayer() const
{
    return m_player;
}
