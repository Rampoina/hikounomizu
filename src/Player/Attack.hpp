/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ATTACK
#define DEF_ATTACK

#include "Animation/CallbackAnimation.hpp"
#include "Engines/Sound/SoundEngine.hpp"
#include "Structs/Box.hpp"

class Player;

struct HitBox
{
    HitBox()
    {
        
    }
    
    HitBox(const Box &hitBox, float hitTime, float hitFactor) :
    box(hitBox), time(hitTime), factor(hitFactor)
    {
        
    }
    
    Box box;
    float time, factor; //Strength factor
};

///Attack: Association of an animation with
/// timed hitboxes and soundeffects.
class Attack : public Drawable
{
    public:
        Attack();
        void draw();
        
        void update(float frameTime);
        void reinit();
        
        bool ready();
        
        void addHitBox(const HitBox &hitBox, int hitBoxIx = -1);
        void scaleHitBoxes(float scale);
        
        void addSoundEffect(float time, const std::string &soundEffect);
        
        void setAnimation(const CallbackAnimation &animation);
        const CallbackAnimation &getAnimation() const;
        
        void setPlayer(Player &player);
        const Player *getPlayer() const;
    
    private:
        CallbackAnimation m_animation;
        std::vector<HitBox> m_hitBoxes;
        std::vector< std::pair<float, std::string> > m_soundEffects;
        
        Player *m_player;
};

#endif
