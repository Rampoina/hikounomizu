/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PauseTab.hpp"

PauseTab::PauseTab(Font &textFont, TextureManager &manager, SoundEngine &soundEngine) :
Tab(300, 300, Color(66, 112, 174, 125)), m_choice(PAUSE_NOCHOICE)
{
    Sprite ui_default( (*manager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) ),
    ui_hover( (*manager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinBox skin(240.f, 60.f, ui_default, ui_hover);
    SkinSound sounds(soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg");
    
    m_pauseText.setText("Pause");
    m_pauseText.setFont(textFont);
    m_pauseText.setColor(Color(55, 55, 55));
    m_pauseText.setPosition( (getWidth() - m_pauseText.getWidth()) / 2, 20 );
    
    //Resume button
    m_resume.setText("Resume");
    m_resume.setTextFont(textFont);
    m_resume.setTextColor(Color(255, 255, 255));
    
    m_resume.setSkin(skin);
    m_resume.setSoundSkin(sounds);
    
    //Restart button
    m_restart.setText("Restart");
    m_restart.setTextFont(textFont);
    m_restart.setTextColor(Color(255, 255, 255));
    
    m_restart.setSkin(skin);
    m_restart.setSoundSkin(sounds);
    
    //Exit button
    m_exit.setText("Main Menu");
    m_exit.setTextFont(textFont);
    m_exit.setTextColor(Color(255, 255, 255));
    
    m_exit.setSkin(skin);
    m_exit.setSoundSkin(sounds);
    
    m_resume.setXPosition( (getWidth() - m_resume.getWidth()) / 2 );
    m_resume.setYPosition(70);
    
    m_restart.setXPosition( (getWidth() - m_restart.getWidth()) / 2 );
    m_restart.setYPosition( m_resume.getYPosition() + m_resume.getHeight() + 10 );
    
    m_exit.setXPosition( (getWidth() - m_exit.getWidth()) / 2 );
    m_exit.setYPosition( m_restart.getYPosition() + m_restart.getHeight() + 10 );
}

void PauseTab::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    m_pauseText.draw();
    m_resume.draw();
    m_restart.draw();
    m_exit.draw();
    
    Drawable::popMatrix();
}

bool PauseTab::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (!chosen())
    {
        if (m_resume.mouseRelease(localMouse))
            m_choice = PAUSE_RESUME;
        else if (m_restart.mouseRelease(localMouse))
            m_choice = PAUSE_RESTART;
        else if (m_exit.mouseRelease(localMouse))
            m_choice = PAUSE_EXIT;
    }
    
    return Widget::contains(localMouse);
}


bool PauseTab::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool PauseTab::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    m_resume.mouseMove(localMouse);
    m_restart.mouseMove(localMouse);
    m_exit.mouseMove(localMouse);
    
    return Widget::contains(localMouse);
}

bool PauseTab::chosen() const
{
    return (m_choice != PAUSE_NOCHOICE);
}

unsigned int PauseTab::result() const
{
    return m_choice;
}

void PauseTab::reinitialize()
{
    m_choice = PAUSE_NOCHOICE;
}
