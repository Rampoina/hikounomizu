/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_END_TAB
#define DEF_END_TAB

#define END_NOCHOICE 0 //The user did not choose an option yet
#define END_REPLAY 1 //... choice to replay the game
#define END_EXIT 2 //... choice to exit

#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "Graphics/Drawable/Text.hpp"

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"

//EndTab : The widget that will appear after a game is finished :
//It shows the winner, and allows the user to replay the game or to exit
class EndTab : public Tab
{
    public:
        EndTab(Font &textFont, TextureManager &manager, SoundEngine &soundEngine);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        void setWinner(const std::string &winner);
        const std::string &getWinner() const;
        
        bool chosen() const; //Has the choice been made ?
        unsigned int result() const; //Result of the choice
    
    private:
        Text m_winnerText;
        TextButton m_replay, m_exit;
        
        unsigned int m_choice;
};

#endif
