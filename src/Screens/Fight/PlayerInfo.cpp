/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerInfo.hpp"

PlayerInfo::PlayerInfo() :
Drawable(), m_player(NULL), m_barsWidth(0.f), m_barsHeight(0.f), m_iconScale(1.f)
{
    m_name.setColor( Color(255,255,255) );
    m_nameBorders.setColor( Color(0,0,0) );
    
    m_shurikenText.setColor( Color(255,255,255) );
    m_shurikenTextBorders.setColor( Color(0,0,0) );
}

void PlayerInfo::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_icon.draw();
    
    m_nameBorders.draw();
    m_name.draw();
    
    m_lifeBarBack.draw();
    m_lifeBar.draw();
    m_lifeText.draw();
    
    m_shurikenIcon.draw();
    
    m_shurikenTextBorders.draw();
    m_shurikenText.draw();
    
    Drawable::popMatrix();
}

void PlayerInfo::update()
{
    if (m_player != NULL)
    {
        //Health
        float playerHealth = m_player->getHealth();
        
        if (playerHealth != m_knownState.health)
        {
            std::ostringstream healthText;
            healthText << playerHealth;
            
            m_lifeText.setText(healthText.str() + " / 100");
            m_lifeBar.setXScale(playerHealth / 100);
            
            m_knownState.health = playerHealth;
        }
        
        //Weapons
        updateWeaponsText();
    }
}

void PlayerInfo::initPositions()
{
    const Font* nameFont = m_name.getFont();
    const Font* barsFont = m_lifeText.getFont();
    
    if (m_player != NULL && nameFont != NULL && barsFont != NULL)
    {
        float xBase = m_icon.getXPosition() + m_icon.getWidth() * m_icon.getXScale() + 10;
        
        // Health bar
        m_barsHeight = (barsFont->getCharSize() / 1.3) + 10;
        m_barsWidth = m_barsHeight * 6.f;
        
        m_name.setPosition(xBase, 0);
        
        m_lifeBarBack = Polygon::rectangle(0, 0, m_barsWidth, m_barsHeight, Color(175, 175, 175, 175));
        m_lifeBarBack.setBorderSize(0);
        
        m_lifeBar = Polygon::rectangle(0, 0, m_barsWidth, m_barsHeight, Color(0, 192, 0));
        m_lifeBar.setPosition(xBase, (nameFont->getCharSize() / 1.3) + 5);
        m_lifeBar.setBorderSize(0);
        
        m_lifeText.setPosition(m_lifeBar.getXPosition() + 20, m_lifeBar.getYPosition() + 5);
        m_lifeText.setColor(Color(255, 255, 255, 200));
        
        // Weapon icon & text
        float shurikenWidth = m_shurikenIcon.getWidth() * m_shurikenIcon.getXScale();
        float shurikenHeight = m_shurikenIcon.getHeight() * m_shurikenIcon.getYScale();
        
        m_shurikenIcon.setPosition(xBase, m_lifeBar.getYPosition() + m_barsHeight + 5);
        m_shurikenText.setPosition(xBase + shurikenWidth + 8,
            m_shurikenIcon.getYPosition() + (shurikenHeight - m_shurikenText.getHeight()) / 2.f );
        
        // Height alignment
        float itemsHeight = m_shurikenIcon.getYPosition() + shurikenHeight;
        float diffY = (m_icon.getHeight() * m_icon.getYScale() - itemsHeight) / 2.f;
        
        m_name.setYPosition(m_name.getYPosition() + diffY);
        m_nameBorders.setPosition(xBase, m_name.getYPosition());
        
        m_lifeBar.setYPosition(m_lifeBar.getYPosition() + diffY);
        m_lifeBarBack.setPosition(m_lifeBar.getXPosition(), m_lifeBar.getYPosition());
        m_lifeText.setYPosition(m_lifeText.getYPosition() + diffY);
        
        m_shurikenIcon.setYPosition(m_shurikenIcon.getYPosition() + diffY);
        m_shurikenText.setYPosition(m_shurikenText.getYPosition() + diffY);
        m_shurikenTextBorders.setPosition(m_shurikenText.getXPosition(), m_shurikenText.getYPosition());
    }
}

void PlayerInfo::updateWeaponsText()
{
    if (m_player != NULL)
    {
        unsigned int shurikenAmount = 0;
        
        WeaponSlots weapons;
        if (m_player->getWeapons(weapons))
            shurikenAmount = weapons.slots[WEAPON_SHURIKEN];
        
        if (shurikenAmount != m_knownState.shurikenAmount)
        {
            std::ostringstream shurikenStr;
            shurikenStr << shurikenAmount;
            
            m_shurikenText.setText(shurikenStr.str());
            m_shurikenTextBorders.setText(shurikenStr.str());
            
            m_knownState.shurikenAmount = shurikenAmount;
        }
    }
}

//Get and set methods
void PlayerInfo::setPlayer(Player &player, TextureManager &textureManager)
{
    m_player = &player;
    
    m_icon.setTexture( (*textureManager.getTexture("gfx/characters/reduced.png")) );
    m_icon.setSubRect( Box( ((player.getName() == "Hikou") ? 0.f : 0.5), 0.f, 0.5, 1.f) );
    m_icon.setScale(BASE_ICON_SCALE * m_iconScale);
    
    m_name.setText(player.getName());
    m_nameBorders.setText(player.getName());
    
    m_shurikenIcon.setTexture( (*textureManager.getTexture("gfx/weapons/shuriken.png")) );
    m_shurikenIcon.setScale(WEAPON_ICON_SCALE);
    updateWeaponsText();
    
    initPositions();
}

const Player &PlayerInfo::getPlayer() const
{
    return *m_player;
}

void PlayerInfo::setFonts(Font &nameFont, Font &weaponsFont, Font &barsFont)
{
    m_name.setFont(nameFont);
    m_nameBorders.setFont(nameFont);
    
    m_shurikenText.setFont(weaponsFont);
    m_shurikenTextBorders.setFont(weaponsFont);
    
    m_lifeText.setFont(barsFont);
    
    initPositions();
}

void PlayerInfo::setIconScale(float scale)
{
    if (scale > 0 && scale < 1 / BASE_ICON_SCALE)
    {
        m_iconScale = scale;
        m_icon.setScale(BASE_ICON_SCALE * m_iconScale);
        
        initPositions();
    }
}

float PlayerInfo::getIconScale() const
{
    return m_iconScale;
}

float PlayerInfo::getWidth() const
{
    if (m_barsWidth > m_name.getWidth())
        return m_lifeBar.getXPosition() + m_barsWidth;
    
    return m_name.getXPosition() + m_name.getWidth();
}

float PlayerInfo::getHeight() const
{
    return m_lifeBar.getYPosition() + m_barsHeight;
}
