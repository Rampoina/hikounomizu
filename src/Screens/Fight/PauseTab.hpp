/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PAUSE_TAB
#define DEF_PAUSE_TAB

#define PAUSE_NOCHOICE 0 //The user did not choose an option yet
#define PAUSE_RESUME 1 //... choice to resume the game
#define PAUSE_RESTART 2 //... choice to restart the game
#define PAUSE_EXIT 3 //... choice to exit to main menu

#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "Graphics/Drawable/Text.hpp"

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"

class PauseTab : public Tab
{
    public:
        PauseTab(Font &textFont, TextureManager &manager, SoundEngine &soundEngine);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        bool chosen() const; ///Has the choice been made ?
        unsigned int result() const; ///Result of the choice.
        
        void reinitialize(); ///Reinitialize the chooser.

    private:
        Text m_pauseText;
        TextButton m_resume, m_restart, m_exit;
        
        unsigned int m_choice;
};

#endif // DEF_PAUSE_TAB
