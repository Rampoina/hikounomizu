/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_PLAYER_INFO
#define DEF_PLAYER_INFO

#define BASE_ICON_SCALE 0.4
#define WEAPON_ICON_SCALE 0.15

#include "Graphics/Drawable/Text.hpp"
#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Polygon.hpp"
#include "Player/Player.hpp"

#include <string>
#include <sstream>

///NameBorder: Borders for the name text.
struct NameBorder
{
    public:
        NameBorder()
        {
            for (unsigned int i = 0; i < 4; i++)
                shadows.push_back(Text());
        }
        
        void draw()
        {
            for (unsigned int i = 0; i < shadows.size(); i++)
                shadows[i].draw();
        }
        
        void setColor(const Color &color)
        {
            for (unsigned int i = 0; i < shadows.size(); i++)
                shadows[i].setColor(color);
        }
        
        void setText(const std::string &text)
        {
            for (unsigned int i = 0; i < shadows.size(); i++)
                shadows[i].setText(text);
        }
        
        void setFont(Font &font)
        {
            for (unsigned int i = 0; i < shadows.size(); i++)
                shadows[i].setFont(font);
        }
        
        void setPosition(float x, float y)
        {
            float space = 1.f;
            shadows[0].setPosition(x - space, y - space);
            shadows[1].setPosition(x - space, y + space);
            shadows[2].setPosition(x + space, y - space);
            shadows[3].setPosition(x + space, y + space);
        }
    
    private:
        std::vector<Text> shadows;
};

/// KnownState: Stores the last known state of the player
/// (health, weapon amount...)
struct KnownState
{
    KnownState() : health(-1.f), shurikenAmount(0) {}
    
    float health;
    unsigned int shurikenAmount;
};

/// PlayerInfo: Interface item showing a player's information.
class PlayerInfo : public Drawable
{
    public:
        PlayerInfo();
        void draw();
        
        void update();
        
        //Get and set methods
        void setPlayer(Player &player, TextureManager &textureManager);
        const Player &getPlayer() const;
        
        void setFonts(Font &nameFont, Font &weaponsFont, Font &barsFont);
        void setIconScale(float scale);
        float getIconScale() const;
        
        float getWidth() const;
        float getHeight() const;
    
    private:
        void initPositions();
        void updateWeaponsText();
        
        Player *m_player;
        
        Text m_name, m_lifeText;
        NameBorder m_nameBorders;
        
        Sprite m_icon;
        Polygon m_lifeBar, m_lifeBarBack;
        
        Sprite m_shurikenIcon;
        Text m_shurikenText;
        NameBorder m_shurikenTextBorders;
        
        KnownState m_knownState;
        
        float m_barsWidth, m_barsHeight;
        float m_iconScale;
};

#endif
