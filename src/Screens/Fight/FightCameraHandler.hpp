/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FIGHT_CAMERA_HANDLER
#define DEF_FIGHT_CAMERA_HANDLER

#define PADDING 175.f

#define MODE_DYNAMIC 0
#define MODE_STATIC 1

#include "Graphics/Camera.hpp"
#include "Structs/Box.hpp"

#include "Player/Player.hpp"
#include <vector>
#include <algorithm>

///FightCameraHandler : Fighting scene camera handler, optimizes view while
///ensuring that every player is always visible.
class FightCameraHandler
{
    public:
        FightCameraHandler(const Box &arenaBox, float groundLevel, const std::vector<const Player*> &players,
            unsigned int mode);
        
        void look(float viewWidth, float viewHeight, float frameTime, float arenaMinCameraWidth);
    
    private:
        void computeView(Box &view, float minWidth) const;
        
        void emcompass(Box &box) const;
        void extend(Box &box, float minWidth) const;
        void adjust(Box &box) const;
        void center(Box &box) const;
        
        std::vector<const Player*> m_players;
        
        Box m_targetBox; /// The latest known exact camera box, emcompassing all players.
        Box m_currentBox; /// The current camera box, continuously fades to smoothly match m_targetBox.
        
        float m_arenaRatio; ///< Width / Height ratio of the arena.
        Box m_arenaBox;
        float m_groundLevel; /// Ground level relative to the top of the arena.
        
        unsigned int m_mode;
};

#endif
