/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "EndTab.hpp"

EndTab::EndTab(Font &textFont, TextureManager &manager, SoundEngine &soundEngine) :
Tab(300, 200, Color(66, 112, 174)), m_choice(END_NOCHOICE)
{
    Sprite ui_default( (*manager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) ),
    ui_hover( (*manager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinBox skin(240.f, 60.f, ui_default, ui_hover);
    SkinSound sounds(soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg");
    
    m_winnerText.setFont(textFont);
    m_winnerText.setYPosition(5);
    
    m_replay.setText("Replay");
    m_replay.setTextFont(textFont);
    m_replay.setTextColor(Color(255, 255, 255));
    
    m_replay.setSkin( skin );
    m_replay.setSoundSkin( sounds );
    
    m_exit.setText("Main Menu");
    m_exit.setTextFont(textFont);
    m_exit.setTextColor(Color(255, 255, 255));
    
    m_exit.setSkin( skin );
    m_exit.setSoundSkin( sounds );
    
    m_replay.setXPosition( (getWidth() - m_replay.getWidth()) / 2 );
    m_replay.setYPosition(55);
    
    m_exit.setXPosition( (getWidth() - m_exit.getWidth()) / 2 );
    m_exit.setYPosition( m_replay.getYPosition() + m_replay.getHeight() + 10 );
}

void EndTab::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    m_winnerText.draw();
    m_replay.draw();
    m_exit.draw();
    
    Drawable::popMatrix();
}

bool EndTab::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (!chosen())
    {
        if (m_replay.mouseRelease(localMouse))
            m_choice = END_REPLAY;
        else if (m_exit.mouseRelease(localMouse))
            m_choice = END_EXIT;
    }
    
    return Widget::contains(localMouse);
}

bool EndTab::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool EndTab::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    m_replay.mouseMove(localMouse);
    m_exit.mouseMove(localMouse);
    
    return Widget::contains(localMouse);
}

void EndTab::setWinner(const std::string &winner)
{
    m_winnerText.setText(winner);
    m_winnerText.setXPosition( (getWidth() - m_winnerText.getWidth()) / 2 );
}

const std::string &EndTab::getWinner() const
{
    return m_winnerText.getText();
}

bool EndTab::chosen() const
{
    return (m_choice != END_NOCHOICE);
}

unsigned int EndTab::result() const
{
    return m_choice;
}
