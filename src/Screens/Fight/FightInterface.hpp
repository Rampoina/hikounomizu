/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FIGHT_INTERFACE
#define DEF_FIGHT_INTERFACE

#include "PlayerInfo.hpp"
#include <vector>

class FightInterface : public Drawable
{
    public:
        FightInterface();
        void draw();
        void update();
        
        void viewWidth(float width); ///Give view size to the Fight Interface for PlayerInfo positioning.
        
        void addPlayer(Player &player, TextureManager &textureManager, int playerIx = -1);
        void removePlayer(int playerIx);
        
        void setFonts(Font &namesFont, Font &weaponsFont, Font &barsFont);
    
    private:
        void updatePositions();
        
        std::vector<PlayerInfo> m_playerInfos;
        Font *m_namesFont, *m_weaponsFont, *m_barsFont;
        
        float m_viewWidth, m_iconsScale;
};

#endif
