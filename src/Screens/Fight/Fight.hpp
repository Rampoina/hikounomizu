/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_FIGHT
#define DEF_FIGHT

#define FIGHT_MODE_DEFAULT 0
#define FIGHT_MODE_VERSUS 1

#include "Graphics/Window.hpp"
#include "Graphics/Camera.hpp"

#include "Configuration/Configuration.hpp"

#include "Engines/Physics/PhysicsWorld.hpp"
#include "Engines/Sound/SoundEngine.hpp"
#include "Engines/Resources/FontManager.hpp"

#include "FightCameraHandler.hpp"
#include "PauseTab.hpp"
#include "EndTab.hpp"
#include "FightInterface.hpp"
#include "Arena/Arena.hpp"
#include "Player/AI/AI.hpp"
#include "Player/Human/Human.hpp"

#include "Weapon/WeaponMedium.hpp"

#include "Tools/Timer.hpp"
#include "Structs/Vector.hpp"

#include <sstream>
#include <iostream>

#include "Engines/Resources/JoystickManager.hpp"

#include "Screens/Screen.hpp"

class GameEngine;

//Screen's inheriting class representing the fight screen
class Fight : public Screen
{
    public:
        Fight(GameEngine *gameEngine);
        
        void setArena(const std::string &arenaName);
        void setMode(int mode);
        
        void addPlayer(const std::string &playerName, int playerType);
        void clearPlayers();
        
        //Screen virtual method
        void run();
    
    private:
        std::vector<const Player*> toConstPlayers(const std::vector<Player*> &players);
        
        GameEngine *m_gameEngine;
        
        Window *m_window;
        Configuration *m_configuration;
        
        std::vector< std::pair<int, std::string> > m_playersDataList;
        std::string m_arenaPath;
        int m_mode;
};

#endif
