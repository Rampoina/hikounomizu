/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FightInterface.hpp"

FightInterface::FightInterface() :
Drawable(), m_namesFont(NULL), m_weaponsFont(NULL), m_barsFont(NULL), m_viewWidth(-1.f), m_iconsScale(1.f)
{
    
}

void FightInterface::draw()
{
    //UpdateMatrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    for (unsigned int i = 0; i < m_playerInfos.size(); i++)
        m_playerInfos[i].draw();
    
    Drawable::popMatrix();
}

void FightInterface::update()
{
    for (unsigned int i = 0; i < m_playerInfos.size(); i++)
        m_playerInfos[i].update();
}

void FightInterface::viewWidth(float width)
{
    m_viewWidth = width;
}

void FightInterface::addPlayer(Player &player, TextureManager &textureManager, int playerIx)
{
    PlayerInfo playerInfo;
    playerInfo.setFonts(*m_namesFont, *m_weaponsFont, *m_barsFont);
    playerInfo.setPlayer(player, textureManager);
    playerInfo.setIconScale(m_iconsScale);
    
    if (playerIx == -1)
        m_playerInfos.push_back(playerInfo);
    else if (playerIx >= 0 && playerIx <= static_cast<int>(m_playerInfos.size()) - 1)
        m_playerInfos[playerIx] = playerInfo;
    
    updatePositions();
}

void FightInterface::removePlayer(int playerIx)
{
    if (playerIx >= 0 && playerIx <= static_cast<int>(m_playerInfos.size()) - 1)
    {
        m_playerInfos.erase(m_playerInfos.begin() + playerIx);
        updatePositions();
    }
}

void FightInterface::updatePositions()
{
    //Compute the space between each PlayerInfo.
    float space = 50.f;
    if (m_viewWidth > 0.f)
    {
        //Compute content width
        float contentWidth = 0.f;
        for (unsigned int i = 0; i < m_playerInfos.size(); i++)
            contentWidth += m_playerInfos[i].getWidth();
        
        //Reduce size of items if they overflow
        for (unsigned int resizeAttempts = 0; resizeAttempts < 2; resizeAttempts++)
        {
            //Break if size is sufficient
            if (contentWidth <= m_viewWidth)
                break;
            
            //Resize the player info items
            m_iconsScale *= 0.75;
            for (unsigned int i = 0; i < m_playerInfos.size(); i++)
                m_playerInfos[i].setIconScale(m_iconsScale);
            
            //Recompute contentWidth
            contentWidth = 0.f;
            for (unsigned int i = 0; i < m_playerInfos.size(); i++)
                contentWidth += m_playerInfos[i].getWidth();
        }
        
        space = (m_viewWidth - contentWidth) / (m_playerInfos.size() + 1);
    }
    
    //Position each PlayerInfo
    float currentPos = space;
    for (unsigned int i = 0; i < m_playerInfos.size(); i++)
    {
        m_playerInfos[i].setXPosition( currentPos );
        currentPos += m_playerInfos[i].getWidth() + space;
    }
}

//Get and set methods
void FightInterface::setFonts(Font &namesFont, Font &weaponsFont, Font &barsFont)
{
    m_namesFont = &namesFont;
    m_weaponsFont = &weaponsFont;
    m_barsFont = &barsFont;
    
    for (unsigned int i = 0; i < m_playerInfos.size(); i++)
        m_playerInfos[i].setFonts(*m_namesFont, *m_weaponsFont, *m_barsFont);
}
