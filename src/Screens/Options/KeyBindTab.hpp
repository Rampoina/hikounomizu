/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEY_BIND_TAB
#define DEF_KEY_BIND_TAB

#include "MultipleKeyBinder.hpp"

#include "Configuration/Configuration.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/JoystickManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/Chooser.hpp"
#include "GUI/Widget/KeyBinder.hpp"
#include "GUI/Widget/TextButton.hpp"

#include <sstream>
#include <iostream>

///KeyBindTab : tab interface to set the key bindings of a player
///(of a given index).
class KeyBindTab : public Tab
{
    public:
        KeyBindTab();
        KeyBindTab(Configuration &configuration, unsigned int playerIx, TextureManager &manager, Font &titleFont, Font &labelFont, SoundEngine &soundEngine);
        void draw();
        
        void addJoystickBinder(int joystickID, std::string joystickName);
        void removeJoystickBinder(int joystickID);
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        void keyEvent(const SDL_Event &event);
        
    private:
        void applyKeys();
        void applyActiveDevice();
        void resetChooser();
        
        //Title message
        Text m_titleText, m_activeGameDeviceText;
        
        //Chooser for joysticks
        Chooser m_deviceChooser;
        
        //Key binders
        std::map<int, int> m_selectionToDevice;
        
        int m_currentDeviceID;
        std::map<int, std::string> m_deviceNames;
        std::map<int, MultipleKeyBinder> m_keyBinders;
        
        //Resources
        Configuration *m_configuration;
        TextureManager *m_textureManager;
        Font *m_labelFont;
        SoundEngine *m_soundEngine;
        
        unsigned int m_playerIx;
};

#endif
