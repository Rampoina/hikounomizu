/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_OPTIONS_TAB
#define DEF_OPTIONS_TAB

#include "Configuration/Configuration.hpp"
#include "Engines/Resources/FontManager.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"

#include "GeneralOptions.hpp"
#include "ControlsOptions.hpp"

#define TAB_GENERAL 0
#define TAB_CONTROLS 1

class OptionsTab : public Tab
{
    public:
        OptionsTab();
        OptionsTab(Configuration &configuration, FontManager &fontManager, TextureManager &textureManager, SoundEngine &soundEngine);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        void keyEvent(const SDL_Event &event);
        
        void setDisplayModes(const std::vector<Vector> &modes);
        void addJoystickBinder(int joystickID, std::string joystickName);
        void removeJoystickBinder(int joystickID);
        
        bool displayModeApplied(); ///Whether the human user wishes to apply display mode changes
        bool volumeUpdated(); ///Whether the master, music or effects volume was updated
    
    private:
        TextButton m_generalTab, m_controlsTab;
        
        GeneralOptions m_general;
        ControlsOptions m_controls;
        
        unsigned int m_currentTab;
};

#endif
