/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CONTROLS_OPTIONS
#define DEF_CONTROLS_OPTIONS

#include "Configuration/Configuration.hpp"
#include "Engines/Resources/FontManager.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "GUI/Widget/Tab.hpp"
#include "KeyBindTab.hpp"

class ControlsOptions : public Tab
{
    public:
        ControlsOptions();
        ControlsOptions(Configuration &configuration, FontManager &fontManager, TextureManager &textureManager, SoundEngine &soundEngine);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        void keyEvent(const SDL_Event &event);
        
        void addJoystickBinder(int joystickID, std::string joystickName);
        void removeJoystickBinder(int joystickID);
    
    private:
        KeyBindTab m_keyTab_p1, m_keyTab_p2;
};

#endif
