/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Options.hpp"
#include "Engines/GameEngine.hpp"

Options::Options(GameEngine *gameEngine) :
m_gameEngine(gameEngine), m_window(gameEngine->getWindow()),
m_configuration(gameEngine->getConfiguration())
{
    
}

void Options::run()
{
    //Init variables
        //Get view size
    Vector viewSize = Vector(m_window->getViewWidth(), m_window->getViewHeight());
    
    //Background ( + Title)
    TextureManager textureManager;
    FontManager fontManager;
    SoundBufferManager soundBufferManager;
    
    Sprite background( (*textureManager.getTexture("gfx/ui/title.png")), Box(0, 0, 1920.f / 2048, 1080.f / 2048) );
    background.setScale(viewSize.x / background.getWidth(), viewSize.y / background.getHeight());
    
    Text title("Options", *fontManager.getFont("fonts/default.ttf", 64));
    title.setColor(Color(255, 255, 255));
    
    title.setXPosition((viewSize.x - title.getWidth()) / 2);
    title.setYPosition(viewSize.y * .08f);
    
    //Content
    SoundEngine soundEngine;
    soundEngine.setManager(soundBufferManager);
    soundEngine.setVolume(m_configuration->getMasterVolume() * m_configuration->getSoundVolume() / 100.f);
    
    OptionsTab optionsTab((*m_configuration), fontManager, textureManager, soundEngine);
    optionsTab.setPosition((viewSize.x - optionsTab.getWidth()) / 2, viewSize.y * .23f);
    optionsTab.setDisplayModes(m_window->getAvailableModes());
    
    std::vector< std::pair<int, std::string> > loadedJoysticks = m_gameEngine->getJoystickManager().getLoadedJoysticksData();
    for (unsigned int i = 0; i < loadedJoysticks.size(); i++)
        optionsTab.addJoystickBinder(loadedJoysticks[i].first, loadedJoysticks[i].second);
    
        //Actions
    SkinBox button_skin(240.f, 60.f);
    button_skin.inactive = Sprite( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) );
    button_skin.focused = Sprite( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinSound button_sounds(soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg");
    
    //Back
    TextButton backButton("< Back", button_skin, button_sounds);
    backButton.setTextFont(*fontManager.getFont("fonts/default.ttf", 24));
    backButton.setTextColor(Color(255, 255, 255));
    backButton.setPosition(30, 30);
    
    //Playlist handling
    pthread_mutex_t notifierMutex = PTHREAD_MUTEX_INITIALIZER;
    
    PlaylistNotifier notifier;
    notifier.setNotifyTime(2000.f);
    notifier.setFonts(*fontManager.getFont("fonts/default.ttf", 20), *fontManager.getFont("fonts/default.ttf", 14));
    
    notifier.setPosition( viewSize.x - notifier.getWidth() - 20, viewSize.y - notifier.getHeight() - 20 );
    
    PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, &notifier, &notifierMutex);
    
    //Start screen loop
    Vector mouse;
    SDL_Event event;
    while (1)
    {
        //Manage events
        while (m_window->pollEvent(event))
        {
            switch (event.type)
            {
                case SDL_QUIT: //Application closed
                    m_gameEngine->exit();
                    
                    PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, NULL, NULL);
                    pthread_mutex_destroy(&notifierMutex);
                    return;
                break;
                
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED) //Window resized
                        m_window->resizeView();
                break;
                
                case SDL_JOYDEVICEADDED:
                    {
                        int instanceID = m_gameEngine->getJoystickManager().loadJoystick(event.jdevice.which);
                        if (instanceID != JOYSTICK_LOAD_ERROR)
                        {
                            std::string name = m_gameEngine->getJoystickManager().getName(instanceID);
                            optionsTab.addJoystickBinder(instanceID, name);
                        }
                    }
                break;
                
                case SDL_JOYDEVICEREMOVED:
                    m_gameEngine->getJoystickManager().removeJoystick(event.jdevice.which);
                    m_configuration->updatePlayerDevices(event.jdevice.which);
                    optionsTab.removeJoystickBinder(event.jdevice.which);
                break;
                
                case SDL_MOUSEBUTTONDOWN: //Mouse pressed
                    mouse.x = event.button.x;
                    mouse.y = event.button.y;
                    optionsTab.mouseClick(m_window->viewCoords(mouse));
                break;
                
                case SDL_MOUSEBUTTONUP: //Mouse released
                    mouse.x = event.button.x;
                    mouse.y = event.button.y;
                    
                    optionsTab.mouseRelease(m_window->viewCoords(mouse));
                    if (optionsTab.displayModeApplied())
                    {
                        m_window->resizeWindow(m_configuration->getWindowWidth(),
                                               m_configuration->getWindowHeight(),
                                               m_configuration->getFullscreen());
                        
                        //Reload
                        m_gameEngine->initOptions();
                        
                        PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, NULL, NULL);
                        pthread_mutex_destroy(&notifierMutex);
                        return;
                    }
                    else if (optionsTab.volumeUpdated())
                    {
                        float musicVolume = m_configuration->getMasterVolume() * m_configuration->getMusicVolume() / 100.f;
                        float effectsVolume = m_configuration->getMasterVolume() * m_configuration->getSoundVolume() / 100.f;
                        
                        PlaylistPlayer::playlistSetVolume(PLAYLIST_MAIN, musicVolume);
                        soundEngine.setVolume(effectsVolume);
                    }
                    
                    if (backButton.mouseRelease(m_window->viewCoords(mouse)))
                    {
                        m_gameEngine->initMainMenu();
                        
                        PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, NULL, NULL);
                        pthread_mutex_destroy(&notifierMutex);
                        return;
                    }
                    
                break;
                
                case SDL_MOUSEMOTION: //Mouse moved
                    mouse.x = event.motion.x;
                    mouse.y = event.motion.y;
                    
                    backButton.mouseMove(m_window->viewCoords(mouse));
                    optionsTab.mouseMove(m_window->viewCoords(mouse));
                break;
                
                case SDL_KEYDOWN:
                    // Escape was pressed, back to main menu.
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                    {
                        m_gameEngine->initMainMenu();
                        
                        PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, NULL, NULL);
                        pthread_mutex_destroy(&notifierMutex);
                        return;
                    }
                
                break;
            }
            
            //Key event?
            optionsTab.keyEvent(event);
        }
        
        //Update sounds
        soundEngine.update();
        
        //Display
            //Clear
        m_window->clear();
        
            //Draw
        background.draw();
        title.draw();
        
        optionsTab.draw();
        backButton.draw();
        
            //Playlist notifier
        pthread_mutex_lock(&notifierMutex);
        
        notifier.update();
        notifier.draw();
        
        pthread_mutex_unlock(&notifierMutex);
        
            //Render
        m_window->flush();
    }
}
