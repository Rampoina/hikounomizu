/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GeneralOptions.hpp"

GeneralOptions::GeneralOptions() :
Tab(940, 380, Color(66, 112, 174)), m_displayModeApplied(false),
m_volumeUpdated(false), m_configuration(NULL)
{
    
}

GeneralOptions::GeneralOptions(Configuration &configuration, FontManager &fontManager,
TextureManager &textureManager, SoundEngine &soundEngine) :
Tab(940, 380, Color(66, 112, 174)), m_displayModeApplied(false),
m_volumeUpdated(false), m_configuration(&configuration)
{
    Font &headerFont = (*fontManager.getFont("fonts/default.ttf", 22));
    Font &labelFont = (*fontManager.getFont("fonts/default.ttf", 15));
    
    //Fullscreen mode checkbox
    m_fullscreen = Checkbox(configuration.getFullscreen());
    m_fullscreen.setColor( Color(27, 66, 119) );
    m_fullscreen.setSoundEngine(soundEngine);
    m_fullscreen.setClickSound("audio/ui/menuClick.ogg");
    
    //Dynamic camera checkbox
    m_dynamicCamera = Checkbox(configuration.isCameraDynamic());
    m_dynamicCamera.setColor( Color(27, 66, 119) );
    m_dynamicCamera.setSoundEngine(soundEngine);
    m_dynamicCamera.setClickSound("audio/ui/menuClick.ogg");
    
    //Debug mode checkbox
    m_debugMode = Checkbox(configuration.getDebugMode());
    m_debugMode.setColor( Color(27, 66, 119) );
    m_debugMode.setSoundEngine(soundEngine);
    m_debugMode.setClickSound("audio/ui/menuClick.ogg");
    
    //Resolution chooser
    Sprite chooser_default( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) ),
    chooser_hover( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) ),
    chooser_selected( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinBox skin(120.f, 30.f, chooser_default, chooser_hover, chooser_selected);
    SkinSound sounds(soundEngine, NO_SOUND, "audio/ui/menuClick.ogg");
    
    ChooserSkin chooserSkin(labelFont, Color(255, 255, 255), skin, sounds);
    m_resolution.setSkin(chooserSkin);
    
    //Restore default windowed display button
    m_restoreDefaultDisplay = TextButton("Revert to Default", skin, sounds);
    m_restoreDefaultDisplay.setTextFont(labelFont);
    m_restoreDefaultDisplay.setTextColor(Color(255, 255, 255));
    
    //Apply display mode button
    m_applyDisplayMode = TextButton("Update Display", skin, sounds);
    m_applyDisplayMode.setTextFont(labelFont);
    m_applyDisplayMode.setTextColor(Color(255, 255, 255));
    
    //Master volume
    ContinuousChooserSkin volumeSkin(labelFont, Color(255, 255, 255));
    m_masterVolume = ContinuousChooser(volumeSkin, 0.f, 100.f, static_cast<float>(configuration.getMasterVolume()));
    m_musicVolume = ContinuousChooser(volumeSkin, 0.f, 100.f, static_cast<float>(configuration.getMusicVolume()));
    m_effectsVolume = ContinuousChooser(volumeSkin, 0.f, 100.f, static_cast<float>(configuration.getSoundVolume()));
    
    //Headers
    m_headerVideo = Text("Display", headerFont);
    m_headerAudio = Text("Audio", headerFont);
    m_headerMisc = Text("Miscellaneous", headerFont);
    
    m_headerVideo.setColor(Color(255, 255, 255));
    m_headerAudio.setColor(Color(255, 255, 255));
    m_headerMisc.setColor(Color(255, 255, 255));
    
    //Label and widget position
    //Video
    float labelX = 60.f, labelWidth = 100.f, labelY = 50.f;
    m_headerVideo.setPosition(labelX - 10.f, labelY);
    positionWidget(LabelParams("Fullscreen:", labelFont, labelWidth, labelX, (labelY += 40.f)), m_fullscreen);
    positionWidget(LabelParams("Resolution:", labelFont, labelWidth, labelX, (labelY += 25.f)), m_resolution);
    m_applyDisplayMode.setPosition(labelX, (labelY += 45.f));
    m_restoreDefaultDisplay.setPosition(labelX, (labelY += 35.f));
    
    //Audio
    labelX = 350.f; labelY = 50.f; labelWidth = 120.f;
    m_headerAudio.setPosition(labelX - 10.f, labelY);
    positionWidget(LabelParams("Master Volume:", labelFont, labelWidth, labelX, (labelY += 40.f)), m_masterVolume);
    positionWidget(LabelParams("Music Volume:", labelFont, labelWidth, labelX, (labelY += 25.f)), m_musicVolume);
    positionWidget(LabelParams("Effects Volume:", labelFont, labelWidth, labelX, (labelY += 25.f)), m_effectsVolume);
    
    //Misc
    labelX = 680.f; labelY = 50.f; labelWidth = 150.f;
    m_headerMisc.setPosition(labelX - 10.f, labelY);
    positionWidget(LabelParams("Dynamic Camera:", labelFont, labelWidth, labelX, (labelY += 40.f)), m_dynamicCamera);
    positionWidget(LabelParams("Debug:", labelFont, labelWidth, labelX, (labelY += 25.f)), m_debugMode);
}

void GeneralOptions::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    //Headers
    m_headerVideo.draw();
    m_headerAudio.draw();
    m_headerMisc.draw();
    
    //Labels
    for (unsigned int i = 0; i < m_labels.size(); i++)
        m_labels[i].draw();
    
    m_fullscreen.draw();
    m_restoreDefaultDisplay.draw();
    m_applyDisplayMode.draw();
    
    m_masterVolume.draw();
    m_musicVolume.draw();
    m_effectsVolume.draw();
    
    m_dynamicCamera.draw();
    m_debugMode.draw();
    
    //Chooser: should be rendered last
    m_resolution.draw();
    
    Drawable::popMatrix();
}

bool GeneralOptions::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    bool hit = Widget::contains(localMouse);
    
    //Audio
    m_volumeUpdated = (m_masterVolume.isFocused() || m_musicVolume.isFocused() || m_effectsVolume.isFocused());
    
    m_masterVolume.mouseRelease(localMouse);
    m_musicVolume.mouseRelease(localMouse);
    m_effectsVolume.mouseRelease(localMouse);
    
    //Do not update other widgets (except ContinuousChooser) if the chooser was clicked
    //to avoid clicking on widgets "under" the chooser's options.
    if (!m_resolution.mouseRelease(localMouse))
    {
        m_fullscreen.mouseRelease(localMouse);
        m_dynamicCamera.mouseRelease(localMouse);
        m_debugMode.mouseRelease(localMouse);
        
        if (m_applyDisplayMode.mouseRelease(localMouse))
        {
            apply(true);
            m_displayModeApplied = true;
            return hit;
        }
        else if (m_restoreDefaultDisplay.mouseRelease(localMouse))
        {
            apply(true, true);
            m_displayModeApplied = true;
            return hit;
        }
    }
    
    //Apply to m_configuration
    apply();
    
    return hit;
}

bool GeneralOptions::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    m_resolution.mouseClick(localMouse);
    m_fullscreen.mouseClick(localMouse);
    m_masterVolume.mouseClick(localMouse);
    m_musicVolume.mouseClick(localMouse);
    m_effectsVolume.mouseClick(localMouse);
    m_dynamicCamera.mouseClick(localMouse);
    m_debugMode.mouseClick(localMouse);
    m_applyDisplayMode.mouseClick(localMouse);
    m_restoreDefaultDisplay.mouseClick(localMouse);
    
    return Widget::contains(localMouse);
}

bool GeneralOptions::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    bool hit = Widget::contains(localMouse);
    
    if (m_fullscreen.isChecked())
    {
        //Update only Choosers to avoid multiple simultaneous updates
        if (m_resolution.mouseMove(localMouse))
            return hit;
    }
    
    m_fullscreen.mouseMove(localMouse);
    m_applyDisplayMode.mouseMove(localMouse);
    m_restoreDefaultDisplay.mouseMove(localMouse);
    m_masterVolume.mouseMove(localMouse);
    m_musicVolume.mouseMove(localMouse);
    m_effectsVolume.mouseMove(localMouse);
    m_dynamicCamera.mouseMove(localMouse);
    m_debugMode.mouseMove(localMouse);
    
    return hit;
}

void GeneralOptions::setDisplayModes(const std::vector<Vector> &modes)
{
    if (m_configuration != NULL)
    {
        //Resolution chooser - display modes
        std::vector<std::string> items;
        
        unsigned int defaultItem = 0;
        for (unsigned int i = 0; i < modes.size(); i++)
        {
            if (modes[i].x == m_configuration->getWindowWidth() && modes[i].y == m_configuration->getWindowHeight())
                defaultItem = i;
            
            std::ostringstream itemText;
            itemText << modes[i].x << "x" << modes[i].y;
            
            items.push_back(itemText.str());
        }
        
        m_resolution.setData(items, defaultItem);
    }
}

bool GeneralOptions::displayModeApplied()
{
    if (m_displayModeApplied)
    {
        m_displayModeApplied = false;
        return true;
    }
    
    return false;
}

bool GeneralOptions::volumeUpdated()
{
    if (m_volumeUpdated)
    {
        m_volumeUpdated = false;
        return true;
    }
    
    return false;
}

void GeneralOptions::apply(bool display, bool defaultDisplay)
{
    if (m_configuration != NULL)
    {
        //Audio
        m_configuration->setMasterVolume(static_cast<int>(m_masterVolume.getValue()));
        m_configuration->setMusicVolume(static_cast<int>(m_musicVolume.getValue()));
        m_configuration->setSoundVolume(static_cast<int>(m_effectsVolume.getValue()));
        
        //Misc
        m_configuration->setCameraDynamic(m_dynamicCamera.isChecked());
        m_configuration->setDebugMode(m_debugMode.isChecked());
        
        //Display
        if (display)
        {
            //Revert to default windowed display mode
            if (defaultDisplay)
            {
                m_configuration->setFullscreen(false);
                m_configuration->setWindowWidth(1024);
                m_configuration->setWindowHeight(576);
            }
            else
            {
                m_configuration->setFullscreen(m_fullscreen.isChecked());
                
                std::string result = m_resolution.getSelectionStr();
                std::istringstream splitResult(result);
                
                std::string str_width, str_height;
                std::getline(splitResult, str_width, 'x');
                std::getline(splitResult, str_height, 'x');
                
                int width, height;
                std::istringstream convertWidth(str_width), convertHeight(str_height);
                convertWidth >> width;
                convertHeight >> height;
                
                m_configuration->setWindowWidth(width);
                m_configuration->setWindowHeight(height);
            }
        }
    }
}

void GeneralOptions::positionWidget(const LabelParams &label, Widget &widget)
{
    m_labels.push_back( Text(label.text, (*label.font)) );
    Text &labelText = m_labels.back();
    labelText.setColor(Color(200, 200, 200));
    
    labelText.setPosition(label.xPosition, label.yPosition + (widget.getHeight() - labelText.getHeight()) / 2.f);
    widget.setPosition(label.xPosition + label.width, label.yPosition);
}
