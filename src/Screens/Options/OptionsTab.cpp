/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "OptionsTab.hpp"

OptionsTab::OptionsTab() : Tab(940, 430, Color(85, 126, 182)), m_currentTab(TAB_GENERAL)
{
    
}

OptionsTab::OptionsTab(Configuration &configuration, FontManager &fontManager,
TextureManager &textureManager, SoundEngine &soundEngine) :
Tab(940, 430, Color(85, 126, 182)), m_currentTab(TAB_GENERAL)
{
    m_general = GeneralOptions(configuration, fontManager, textureManager, soundEngine);
    m_general.setPosition(0, 50);
    
    m_controls = ControlsOptions(configuration, fontManager, textureManager, soundEngine);
    m_controls.setPosition(0, 50);
    
    //Header buttons
    SkinBox button_skin(240.f, 50.f);
    button_skin.inactive = Sprite( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) );
    button_skin.focused = Sprite( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    button_skin.selected = Sprite( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinSound button_sounds(soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg");
    
    m_generalTab = TextButton("General", button_skin, button_sounds);    
    m_generalTab.setTextFont(*fontManager.getFont("fonts/default.ttf", 24));
    m_generalTab.setTextColor(Color(255, 255, 255));
    m_generalTab.select(true);
    
    m_controlsTab = TextButton("Controls", button_skin, button_sounds);    
    m_controlsTab.setTextFont(*fontManager.getFont("fonts/default.ttf", 24));
    m_controlsTab.setTextColor(Color(255, 255, 255));
    m_controlsTab.setPosition(m_generalTab.getXPosition() + m_generalTab.getWidth(), 0);
}

void OptionsTab::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    m_generalTab.draw();
    m_controlsTab.draw();
    
    if (m_currentTab == TAB_GENERAL)
        m_general.draw();
    else if (m_currentTab == TAB_CONTROLS)
        m_controls.draw();
    
    Drawable::popMatrix();
}

bool OptionsTab::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (m_generalTab.mouseRelease(localMouse))
    {
        m_currentTab = TAB_GENERAL;
        m_generalTab.select(true);
        m_controlsTab.select(false);
    }
    else if (m_controlsTab.mouseRelease(localMouse))
    {
        m_currentTab = TAB_CONTROLS;
        m_controlsTab.select(true);
        m_generalTab.select(false);
    }
    
    if (m_currentTab == TAB_GENERAL)
        m_general.mouseRelease(localMouse);
    else if (m_currentTab == TAB_CONTROLS)
        m_controls.mouseRelease(localMouse);
    
    return Widget::contains(localMouse);
}

bool OptionsTab::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (m_currentTab == TAB_GENERAL)
        m_general.mouseClick(localMouse);
    else if (m_currentTab == TAB_CONTROLS)
        m_controls.mouseClick(localMouse);
    
    return Widget::contains(localMouse);
}

bool OptionsTab::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    m_generalTab.mouseMove(localMouse);
    m_controlsTab.mouseMove(localMouse);
    
    if (m_currentTab == TAB_GENERAL)
        m_general.mouseMove(localMouse);
    else if (m_currentTab == TAB_CONTROLS)
        m_controls.mouseMove(localMouse);
    
    return Widget::contains(localMouse);
}

void OptionsTab::keyEvent(const SDL_Event &event)
{
    if (m_currentTab == TAB_CONTROLS)
        m_controls.keyEvent(event);
}

void OptionsTab::setDisplayModes(const std::vector<Vector> &modes)
{
    m_general.setDisplayModes(modes);
}

void OptionsTab::addJoystickBinder(int joystickID, std::string joystickName)
{
    m_controls.addJoystickBinder(joystickID, joystickName);
}

void OptionsTab::removeJoystickBinder(int joystickID)
{
    m_controls.removeJoystickBinder(joystickID);
}

bool OptionsTab::displayModeApplied()
{
    return m_general.displayModeApplied();
}

bool OptionsTab::volumeUpdated()
{
    return m_general.volumeUpdated();
}
