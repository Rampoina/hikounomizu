/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MultipleKeyBinder.hpp"

MultipleKeyBinder::MultipleKeyBinder() : Tab(), m_deviceID(DEVICE_KEYBOARD)
{
    
}

MultipleKeyBinder::MultipleKeyBinder(int deviceID, PlayerKeys initialKeys, TextureManager &manager, Font &labelFont, SoundEngine &soundEngine) :
Tab(250, 200, Color(40, 83, 141)), m_deviceID(deviceID)
{
    Sprite chooser_default( (*manager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) ),
    chooser_hover( (*manager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinBox button_skin(72.f, 30.f, chooser_default, chooser_hover);
    SkinSound button_sounds(soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg");
    
    //Key binders
    float xBase = 25, xBaseLabel = 100, yBase = 7;
    chooser_default.setScale(.4, .4);
    chooser_hover.setScale(.4, .4);
    
    m_punch = KeyLabel( "Punch :", labelFont, KeyBinder( initialKeys.getPunchKey() ), xBase, xBaseLabel, yBase, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
    m_kick = KeyLabel( "Kick :", labelFont, KeyBinder( initialKeys.getKickKey() ), xBase, xBaseLabel, yBase + 27, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
    m_throwWeapon = KeyLabel( "Weapon :", labelFont, KeyBinder( initialKeys.getThrowWeaponKey() ), xBase, xBaseLabel, yBase + 54, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
    m_jump = KeyLabel( "Jump :", labelFont, KeyBinder( initialKeys.getJumpKey() ), xBase, xBaseLabel, yBase + 81, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
    m_moveLeft = KeyLabel( "Move left :", labelFont, KeyBinder( initialKeys.getMoveLeftKey() ), xBase, xBaseLabel, yBase + 108, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
    m_moveRight = KeyLabel( "Move right :", labelFont, KeyBinder( initialKeys.getMoveRightKey() ), xBase, xBaseLabel, yBase + 135, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
    m_crouch = KeyLabel( "Crouch :", labelFont, KeyBinder( initialKeys.getCrouchKey() ), xBase, xBaseLabel, yBase + 162, chooser_default, chooser_hover, soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg" );
}

void MultipleKeyBinder::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    //Draw binders
    m_punch.draw();
    m_kick.draw();
    m_throwWeapon.draw();
    m_jump.draw();
    m_moveLeft.draw();
    m_moveRight.draw();
    m_crouch.draw();
    
    Drawable::popMatrix();
}

bool MultipleKeyBinder::mouseRelease(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    m_punch.binder.mouseRelease(localMouse);
    m_kick.binder.mouseRelease(localMouse);
    m_throwWeapon.binder.mouseRelease(localMouse);
    m_jump.binder.mouseRelease(localMouse);
    m_moveLeft.binder.mouseRelease(localMouse);
    m_moveRight.binder.mouseRelease(localMouse);
    m_crouch.binder.mouseRelease(localMouse);
    
    return Widget::contains(localMouse);
}

bool MultipleKeyBinder::mouseClick(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool MultipleKeyBinder::mouseMove(const Vector &mouseCoords)
{
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

void MultipleKeyBinder::keyDown(const Key &key, int deviceID)
{
    if ((key.isFromKeyboard() && m_deviceID == DEVICE_KEYBOARD) || (key.isFromJoystick() && m_deviceID == deviceID))
    {
        m_punch.binder.keyDown(key);
        m_kick.binder.keyDown(key);
        m_throwWeapon.binder.keyDown(key);
        m_jump.binder.keyDown(key);
        m_moveLeft.binder.keyDown(key);
        m_moveRight.binder.keyDown(key);
        m_crouch.binder.keyDown(key);
    }
}

PlayerKeys MultipleKeyBinder::getKeys() const
{
    return PlayerKeys(m_punch.binder.getKey(), m_kick.binder.getKey(), m_throwWeapon.binder.getKey(),
        m_jump.binder.getKey(), m_moveLeft.binder.getKey(), m_moveRight.binder.getKey(), m_crouch.binder.getKey());
}
