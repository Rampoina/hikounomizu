/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_MULTIPLE_KEY_BINDER
#define DEF_MULTIPLE_KEY_BINDER

#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/JoystickManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/KeyBinder.hpp"
#include "GUI/Widget/TextButton.hpp"

#include "Player/Human/PlayerKeys.hpp"

///A key binding label : Text label & the associated key binder
struct KeyLabel
{
    KeyLabel()
    {
        
    }
    
    KeyLabel(const std::string &labelText, Font &labelFont, const KeyBinder &keyBinder,
    float xBase, float xGap, float yBase, const Sprite &inactiveBack, const Sprite &focusedBack,
    SoundEngine &soundEngine, const std::string &focusSound, const std::string &keySetSound) :
    binder(keyBinder)
    {
        binder.setPosition(xBase + xGap, yBase);
        
        binder.setTextFont(labelFont);
        binder.setInactiveBack(inactiveBack);
        binder.setFocusedBack(focusedBack);
        
        binder.setSoundEngine(soundEngine);
        binder.setFocusSound(focusSound);
        binder.setKeySetSound(keySetSound);
        
        label.setText(labelText);
        label.setColor(Color(200, 200, 200));
        label.setFont(labelFont);
        
        label.setPosition(xBase, yBase + (binder.getHeight() - label.getHeight()) / 2);
    }
    
    void draw()
    {
        label.draw();
        binder.draw();
    }
    
    Text label; //A key label
    KeyBinder binder; //Its associated key binder
};

///MultipleKeyBinder : tab interface to set the key bindings of a player.
class MultipleKeyBinder : public Tab
{
    public:
        MultipleKeyBinder();
        MultipleKeyBinder(int deviceID, PlayerKeys initialKeys, TextureManager &manager, Font &labelFont, SoundEngine &soundEngine);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        void keyDown(const Key &key, int deviceID);
        
        PlayerKeys getKeys() const;
        
    private:
        int m_deviceID;
        
        //Key binders
        KeyLabel m_punch, m_kick, m_throwWeapon;
        KeyLabel m_jump;
        KeyLabel m_moveLeft, m_moveRight;
        KeyLabel m_crouch;
};

#endif
