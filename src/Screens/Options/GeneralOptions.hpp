/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_GENERAL_OPTIONS
#define DEF_GENERAL_OPTIONS

#include "Configuration/Configuration.hpp"
#include "Engines/Resources/FontManager.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Sound/SoundEngine.hpp"

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/Checkbox.hpp"
#include "GUI/Widget/Chooser.hpp"
#include "GUI/Widget/ContinuousChooser.hpp"

#include <vector>

struct LabelParams
{
    LabelParams() : text("?"), font(NULL), width(100.f), xPosition(0.f), yPosition(0.f) {}
    
    LabelParams(const std::string &labelText, Font &labelFont, float labelWidth, float xPos, float yPos) :
    text(labelText), font(&labelFont), width(labelWidth), xPosition(xPos), yPosition(yPos)
    {
        
    }
    
    std::string text;
    Font *font;
    float width, xPosition, yPosition;
};

class GeneralOptions : public Tab
{
    public:
        GeneralOptions();
        GeneralOptions(Configuration &configuration, FontManager &fontManager, TextureManager &textureManager, SoundEngine &soundEngine);
        void draw();
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
        
        void setDisplayModes(const std::vector<Vector> &modes);
        
        bool displayModeApplied(); ///Whether the human user wishes to apply display mode changes
        bool volumeUpdated(); ///Whether the master, music or effects volume was updated
    
    private:
        void apply(bool display = false, bool defaultDisplay = false);
        void positionWidget(const LabelParams &label, Widget &widget);
        
        Text m_headerVideo, m_headerAudio, m_headerMisc;
        std::vector<Text> m_labels;
        
        Checkbox m_fullscreen, m_dynamicCamera, m_debugMode;
        Chooser m_resolution;
        ContinuousChooser m_masterVolume, m_musicVolume, m_effectsVolume;
        TextButton m_applyDisplayMode, m_restoreDefaultDisplay;
        
        bool m_displayModeApplied;
        bool m_volumeUpdated;
        
        Configuration *m_configuration;
};

#endif
