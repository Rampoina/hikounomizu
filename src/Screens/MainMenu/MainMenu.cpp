/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MainMenu.hpp"
#include "Engines/GameEngine.hpp"

MainMenu::MainMenu(GameEngine *gameEngine) :
m_gameEngine(gameEngine), m_window(gameEngine->getWindow()),
m_configuration(gameEngine->getConfiguration())
{
    
}

void MainMenu::run()
{
    //Init variables
        //Get view size
    Vector viewSize = Vector(m_window->getViewWidth(), m_window->getViewHeight());
    
    TextureManager textureManager;
    FontManager fontManager;
    SoundBufferManager soundBufferManager;
    
        //Background ( + Title)
    Sprite background( (*textureManager.getTexture("gfx/ui/title.png")), Box(0, 0, 1920.f / 2048, 1080.f / 2048) );
    background.setScale(viewSize.x / background.getWidth(), viewSize.y / background.getHeight());
    
    Sprite title( (*textureManager.getTexture("gfx/ui/banner.png")), Box(0, 0, 1783.f / 2048, 295.f / 512) );
    title.setScale(.4);
    title.setPosition((viewSize.x - title.getWidth() * title.getXScale()) / 2, 30);
    
        //Menu
    SoundEngine soundEngine;
    soundEngine.setManager(soundBufferManager);
    soundEngine.setVolume(m_configuration->getMasterVolume() * m_configuration->getSoundVolume() / 100.f);
    
    Sprite menu_default( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 0, 240.f / 512, 60.f / 512) ),
    menu_hover( (*textureManager.getTexture("gfx/ui/ui.png")), Box(0, 64.f / 512, 240.f / 512, 60.f / 512) );
    
    SkinBox skin(240.f, 60.f, menu_default, menu_hover);
    SkinSound sounds(soundEngine, "audio/ui/menuFocus.ogg", "audio/ui/menuClick.ogg");
    
    TextButton menuTheme(skin, sounds);
    menuTheme.setTextFont(*fontManager.getFont("fonts/default.ttf", 30));
    menuTheme.setTextColor(Color(255, 255, 255));
    
    TextMenu menu(menuTheme, 20, 0, 275, 350, Color(66, 112, 174));
    menu.loadData("cfg/menus/mainMenu.xml");
    menu.launch();
    
    menu.setXPosition((viewSize.x - menu.getWidth()) / 10);
    menu.setYPosition(title.getYPosition() + title.getHeight() * title.getYScale() + viewSize.y * .08f);
    
    //Start the playlist
    Playlist playlist;
    playlist.setVolume( m_configuration->getMasterVolume() * m_configuration->getMusicVolume() / 100.f );
    playlist.addSong( SongData("Boss of Fuju", "David Kvistorf", "audio/music/boss_of_fuju.ogg") );
    playlist.addSong( SongData("Come on Dance", "David Kvistorf", "audio/music/come_on_dance.ogg") );
    playlist.addSong( SongData("Cool Cool Mountain", "David Kvistorf", "audio/music/cool_cool_mountain.ogg") );
    playlist.addSong( SongData("Underwater Battle", "David Kvistorf", "audio/music/underwater_battle.ogg") );
    
    pthread_mutex_t notifierMutex = PTHREAD_MUTEX_INITIALIZER;
    
    PlaylistNotifier notifier;
    notifier.setNotifyTime(2000.f);
    notifier.setFonts(*fontManager.getFont("fonts/default.ttf", 20), *fontManager.getFont("fonts/default.ttf", 14));
    
    notifier.setPosition( viewSize.x - notifier.getWidth() - 20, viewSize.y - notifier.getHeight() - 20 );
    
    if (!PlaylistPlayer::startPlaylist(playlist, PLAYLIST_MAIN, &notifier, &notifierMutex))
        PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, &notifier, &notifierMutex);
    
    //Start screen loop
    Vector mouse;
    SDL_Event event;
    while (1)
    {
        //Manage events
        while (m_window->pollEvent(event))
        {
            switch (event.type)
            {
                case SDL_QUIT: //Application closed
                    m_gameEngine->exit();
                    
                    PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, NULL, NULL);
                    pthread_mutex_destroy(&notifierMutex);
                    return;
                break;
                
                case SDL_WINDOWEVENT:
                    if (event.window.event == SDL_WINDOWEVENT_RESIZED) //Window resized
                        m_window->resizeView();
                break;
                
                case SDL_JOYDEVICEADDED:
                    m_gameEngine->getJoystickManager().loadJoystick(event.jdevice.which);
                break;
                
                case SDL_JOYDEVICEREMOVED:
                    m_gameEngine->getJoystickManager().removeJoystick(event.jdevice.which);
                    m_configuration->updatePlayerDevices(event.jdevice.which);
                break;
                
                case SDL_MOUSEBUTTONUP: //Mouse released
                    mouse.x = event.button.x;
                    mouse.y = event.button.y;
                    
                    menu.mouseRelease(m_window->viewCoords(mouse));
                break;
                
                case SDL_MOUSEMOTION: //Mouse moved
                    mouse.x = event.motion.x;
                    mouse.y = event.motion.y;
                    
                    menu.mouseMove(m_window->viewCoords(mouse));
                break;
                
                case SDL_KEYDOWN:
                    // Escape was pressed, reset the menu
                    if (event.key.keysym.sym == SDLK_ESCAPE)
                        menu.launch();
                
                break;
            }
        }
        
        //Update sounds
        soundEngine.update();
        
        if (menu.browsed()) //Browsing ended
        {
            std::string result = menu.result().back();
            
            if (result == "versus")
            {
                m_gameEngine->confFight_mode(FIGHT_MODE_VERSUS);
                m_gameEngine->confCharactersMenu_playersNo(2);
                m_gameEngine->initCharactersMenu();
                break;
            }
            else if (result == "2p")
            {
                m_gameEngine->confFight_mode(FIGHT_MODE_DEFAULT);
                m_gameEngine->confCharactersMenu_playersNo(2);
                m_gameEngine->initCharactersMenu();
                break;
            }
            else if (result == "3p")
            {
                m_gameEngine->confFight_mode(FIGHT_MODE_VERSUS);
                m_gameEngine->confCharactersMenu_playersNo(3);
                m_gameEngine->initCharactersMenu();
                break;
            }
            else if (result == "4p")
            {
                m_gameEngine->confFight_mode(FIGHT_MODE_VERSUS);
                m_gameEngine->confCharactersMenu_playersNo(4);
                m_gameEngine->initCharactersMenu();
                break;
            }
            else if (result == "5p")
            {
                m_gameEngine->confFight_mode(FIGHT_MODE_VERSUS);
                m_gameEngine->confCharactersMenu_playersNo(5);
                m_gameEngine->initCharactersMenu();
                break;
            }
            else if (result == "options")
            {
                m_gameEngine->initOptions();
                break;
            }
            else if (result == "exit") //Want to exit
            {
                m_gameEngine->exit();
                break;
            }
            
            //Reload menu
            menu.launch();
        }
        
        //Display
            //Clear
        m_window->clear();
        
            //Draw
        background.draw();
        title.draw();
        menu.draw();
        
            //Notifier
        pthread_mutex_lock(&notifierMutex);
        
        notifier.update();
        notifier.draw();
        
        pthread_mutex_unlock(&notifierMutex);
        
            //Render
        m_window->flush();
    }
    
    PlaylistPlayer::playlistSetNotifier(PLAYLIST_MAIN, NULL, NULL);
    pthread_mutex_destroy(&notifierMutex);
}
