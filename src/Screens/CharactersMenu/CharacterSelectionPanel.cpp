/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CharacterSelectionPanel.hpp"

CharacterSelectionPanel::CharacterSelectionPanel() :
Tab(100, 100, Color())
{
    
}

CharacterSelectionPanel::CharacterSelectionPanel(Font &textFont, unsigned int charactersNo) :
Tab(charactersNo * (CHARACTER_BOX_WIDTH + CHARACTER_BOX_MARGIN) - CHARACTER_BOX_MARGIN, CHARACTER_BOX_HEIGHT, Color(0, 0, 0, 0))
{
    for (unsigned int i = 0; i < charactersNo; i++)
    {
        m_characters.push_back( Character() );
        
        m_characterNames.push_back( Text(m_characters[i].name, textFont) );
        m_characterPreviews.push_back( CharacterPreview() );
        m_characterBackgrounds.push_back( Polygon::rectangle(0, 0, CHARACTER_BOX_WIDTH, CHARACTER_BOX_HEIGHT, Color(100, 100, 255, 75)) );
        m_characterBackgrounds[i].setBorderSize(0);
        
        updatePosition(i);
    }
    
}

void CharacterSelectionPanel::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    m_background.draw();
    
    for (unsigned int i = 0; i < m_characters.size(); i++)
    {
        m_characterBackgrounds[i].draw();
        m_characterPreviews[i].draw();
        m_characterNames[i].draw();
    }
    
    Drawable::popMatrix();
}

void CharacterSelectionPanel::update(float frameTime)
{
    std::vector<CharacterPreview>::iterator it;
    for (it = m_characterPreviews.begin(); it != m_characterPreviews.end(); it++)
        it->update(frameTime);
}

void CharacterSelectionPanel::setCharacter(unsigned int ix, const Character &character, TextureManager &textureManager)
{
    if (ix <= m_characters.size() && m_characters[ix - 1] != character)
    {
        m_characters[ix - 1] = character;
        
        m_characterNames[ix - 1].setText(character.getFullName());
        
        if (character.name == "Hikou" || character.name == "Takino")
        {
            m_characterPreviews[ix - 1] = CharacterPreview();
            m_characterPreviews[ix - 1].load(character.name, textureManager);
            m_characterPreviews[ix - 1].setScale(.53f);
        }
        
        updatePosition(ix - 1);
    }
}

void CharacterSelectionPanel::resetCharacter(unsigned int ix)
{
    if (ix <= m_characters.size())
    {
        Character defaultCharacter = Character();
        
        m_characters[ix - 1] = defaultCharacter;
        m_characterNames[ix - 1].setText(defaultCharacter.name);
        m_characterPreviews[ix - 1] = CharacterPreview();
        
        updatePosition(ix - 1);
    }
}

Character CharacterSelectionPanel::getCharacter(unsigned int ix) const
{
    if (ix <= m_characters.size())
        return m_characters[ix - 1];
    
    return Character();
}

void CharacterSelectionPanel::readyCharacter(unsigned int ix)
{
    if (ix <= m_characters.size())
    {
        m_characterPreviews[ix - 1].select();
        updatePosition(ix - 1);
    }
}

void CharacterSelectionPanel::unreadyCharacter(unsigned int ix)
{
    if (ix <= m_characters.size())
    {
        m_characterPreviews[ix - 1].unselect();
        updatePosition(ix - 1);
    }
}

unsigned int CharacterSelectionPanel::getSize() const
{
    return m_characters.size();
}

bool CharacterSelectionPanel::mouseRelease(const Vector &mouseCoords)
{
    return Widget::contains( Widget::localCoords(mouseCoords) );
}

bool CharacterSelectionPanel::mouseClick(const Vector &mouseCoords)
{
    return Widget::contains( Widget::localCoords(mouseCoords) );
}

bool CharacterSelectionPanel::mouseMove(const Vector &mouseCoords)
{
    return Widget::contains( Widget::localCoords(mouseCoords) );
}

void CharacterSelectionPanel::updatePosition(unsigned int index)
{
    if (index < m_characters.size())
    {
        float baseX = index * (CHARACTER_BOX_WIDTH + CHARACTER_BOX_MARGIN);
        
        m_characterBackgrounds[index].setXPosition(baseX);
        
        Text &name = m_characterNames[index];
        name.setPosition(baseX + (CHARACTER_BOX_WIDTH - name.getWidth()) / 2.f,
                            CHARACTER_BOX_HEIGHT - name.getHeight() - CHARACTER_BOX_PADDING);
        
        CharacterPreview &preview = m_characterPreviews[index];
        preview.setPosition(baseX + CHARACTER_BOX_PADDING * 2.f,
                            name.getYPosition() - preview.getHeight() * preview.getYScale() - CHARACTER_BOX_PADDING);

    }
}
