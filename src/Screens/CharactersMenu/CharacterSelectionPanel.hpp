/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTER_SELECTION_PANEL
#define DEF_CHARACTER_SELECTION_PANEL

#include "GUI/Widget/Tab.hpp"
#include "Graphics/Drawable/Text.hpp"
#include "CharacterPreview.hpp"

#include <string>
#include <vector>

#define CHARACTER_TYPE_HUMAN 0
#define CHARACTER_TYPE_AI 1

#define CHARACTER_BOX_WIDTH 175
#define CHARACTER_BOX_HEIGHT 305
#define CHARACTER_BOX_PADDING 10
#define CHARACTER_BOX_MARGIN 20

struct Character
{
    Character()
    {
        name = "?";
        type = CHARACTER_TYPE_HUMAN;
    }
    
    Character(std::string charName, int charType)
    {
        name = charName;
        type = charType;
    }
    
    std::string getFullName() const
    {
        return name + ((type != CHARACTER_TYPE_HUMAN) ? " - AI" : "");
    }
    
    bool operator==(const Character& other) {
        return name == other.name && type == other.type;
    }
    
    bool operator!=(const Character& other) {
        return !((*this) == other);
    }
    
    std::string name;
    int type;
};

/// CharacterSelectionPanel : Horizontal Panel of characters preview. Useful for character selection.
class CharacterSelectionPanel : public Tab
{
    public:
        CharacterSelectionPanel();
        CharacterSelectionPanel(Font &textFont, unsigned int charactersNo);
        void draw();
        
        void update(float frameTime);
        
        void setCharacter(unsigned int ix, const Character &character, TextureManager &textureManager);
        void resetCharacter(unsigned int ix);
        Character getCharacter(unsigned int ix) const;
        
        void readyCharacter(unsigned int ix);
        void unreadyCharacter(unsigned int ix);
        
        unsigned int getSize() const;
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
    
    private:
        void updatePosition(unsigned int index);
        
        std::vector<Character> m_characters;
        
        std::vector<Text> m_characterNames;
        std::vector<Polygon> m_characterBackgrounds;
        std::vector<CharacterPreview> m_characterPreviews;
    
};

#endif
