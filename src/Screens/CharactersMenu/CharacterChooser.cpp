/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CharacterChooser.hpp"

CharacterChooser::CharacterChooser(Font &textFont, const SkinBox &skin) :
Tab(skin.box.width * 2 + 5, skin.box.height * 2 + 5, Color(0, 0, 0, 20)), m_callback(NULL)
{
    m_hikou = TextButton("Hikou", skin);
    m_hikouAI = TextButton("Hikou - AI", skin);
    
    m_takino = TextButton("Takino", skin);
    m_takinoAI = TextButton("Takino - AI", skin);
    
    m_hikou.setTextFont( textFont );
    m_hikouAI.setTextFont( textFont );
    
    m_takino.setTextFont( textFont );
    m_takinoAI.setTextFont( textFont );
    
    m_hikou.setTextColor( Color(255, 255, 255) );
    m_hikouAI.setTextColor( Color(255, 255, 255) );
    
    m_takino.setTextColor( Color(255, 255, 255) );
    m_takinoAI.setTextColor( Color(255, 255, 255) );
    
    m_hikouAI.setYPosition(skin.box.height + 5);
    
    m_takino.setXPosition(skin.box.width + 5);
    m_takinoAI.setPosition(skin.box.width + 5, skin.box.height + 5);
    
}

void CharacterChooser::draw()
{
    //Update matrix
    Drawable::pushMatrix();
    Drawable::updateMatrix();
    
    //Draw
    m_background.draw();
    
    m_hikou.draw();
    m_hikouAI.draw();
    
    m_takino.draw();
    m_takinoAI.draw();
    
    Drawable::popMatrix();
}

void CharacterChooser::initialize(CharacterSelectionCallback &callback)
{
    m_callback = &callback;
}


//Widget methods
bool CharacterChooser::mouseRelease(const Vector &mouseCoords)
{
    if (m_callback == NULL)
        return false;
    
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (m_hikou.mouseRelease(localMouse) || m_hikouAI.mouseRelease(localMouse) ||
        m_takino.mouseRelease(localMouse) || m_takinoAI.mouseRelease(localMouse))
    {
        m_callback->characterSelected();
    }
    
    return Widget::contains( localMouse );
}

bool CharacterChooser::mouseClick(const Vector &mouseCoords)
{
    if (m_callback == NULL)
        return false;
    
    Vector localMouse = Widget::localCoords(mouseCoords);
    return Widget::contains(localMouse);
}

bool CharacterChooser::mouseMove(const Vector &mouseCoords)
{
    if (m_callback == NULL)
        return false;
    
    Vector localMouse = Widget::localCoords(mouseCoords);
    
    if (m_hikou.mouseMove(localMouse))
        m_callback->characterFocused(CHARACTER_HIKOU);
    
    if (m_hikouAI.mouseMove(localMouse))
        m_callback->characterFocused(CHARACTER_HIKOU_AI);
    
    if (m_takino.mouseMove(localMouse))
        m_callback->characterFocused(CHARACTER_TAKINO);
    
    if (m_takinoAI.mouseMove(localMouse))
        m_callback->characterFocused(CHARACTER_TAKINO_AI);
    
    return Widget::contains( localMouse );
}
