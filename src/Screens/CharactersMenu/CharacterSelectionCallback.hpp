/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTER_SELECTION_CALLBACK
#define DEF_CHARACTER_SELECTION_CALLBACK

#define CHARACTER_HIKOU 0
#define CHARACTER_HIKOU_AI 1

#define CHARACTER_TAKINO 10
#define CHARACTER_TAKINO_AI 11

class CharacterSelectionCallback
{
    public:
        virtual ~CharacterSelectionCallback() { }
        virtual void characterSelected() = 0;
        virtual void characterFocused(int character) = 0;
};

#endif
