/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTER_CHOOSER
#define DEF_CHARACTER_CHOOSER

#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"

#include "CharacterSelectionCallback.hpp"

class CharacterChooser : public Tab
{
    public:
        CharacterChooser(Font &textFont, const SkinBox &skin);
        void draw();
        
        void initialize(CharacterSelectionCallback &callback);
        
        //Widget methods
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);
    
    private:
        TextButton m_hikou, m_hikouAI, m_takino, m_takinoAI;
        
        CharacterSelectionCallback *m_callback; ///< Callback that will be used for preview of arenas.
};

#endif
