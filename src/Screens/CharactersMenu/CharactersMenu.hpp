/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_CHARACTERS_MENU
#define DEF_CHARACTERS_MENU

#include <iostream>

#include "Graphics/Window.hpp"

#include "Configuration/Configuration.hpp"

#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Text.hpp"
#include <sstream>

#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/FontManager.hpp"

#include "CharacterSelectionCallback.hpp"
#include "CharacterSelectionPanel.hpp"
#include "CharacterChooser.hpp"

#include "GUI/Menu/TextMenu.hpp"
#include "GUI/PlaylistNotifier.hpp"

#include "Tools/Timer.hpp"

#include "Screens/Screen.hpp"

class GameEngine;

//Screen's inheriting class representing the characters menu screen
class CharactersMenu : public Screen, public CharacterSelectionCallback
{
    public:
        CharactersMenu(GameEngine *gameEngine);
        void characterSelected();
        void characterFocused(int character);
        
        void setPlayersNo(int playersNo);
        
        //Screen virtual method
        void run();
    
    private:
        void cleanPreview();
        
        TextureManager m_textureManager;
        FontManager m_fontManager;
        
        CharacterSelectionPanel m_selectionPreview;
        int m_selectionIx;
        
        GameEngine *m_gameEngine;
        Window *m_window;
        Configuration *m_configuration;
        
        int m_playersNo;
};

#endif
