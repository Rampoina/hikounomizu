/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_CHOOSER
#define DEF_ARENA_CHOOSER

#define ROW_HEIGHT 50
#define ROW_SPACE 10
#define SCROLLING_SPEED 1.f ///px / ms

#define SCROLLING_UP 1
#define NO_SCROLLING 0
#define SCROLLING_DOWN -1

#include "Arena/Arena.hpp"
#include "GUI/Widget/Tab.hpp"
#include "GUI/Widget/TextButton.hpp"
#include "Engines/Resources/TextureManager.hpp"
#include "ArenaPreviewCallback.hpp"

#include <vector>
#include <string>

///ArenaChooser : The graphical menu used to preview (through an external ArenaPreviewCallback) and choose arenas.
class ArenaChooser : public Tab
{
    public:
        ArenaChooser(Font &textFont, TextureManager &manager, float width, unsigned int rows, const Color &color);
        void draw();
        void update(float frameTime);
        
        void initialize(const std::vector< std::pair<std::string, std::string> > &data, ArenaPreviewCallback &callback);
        
        bool mouseRelease(const Vector &mouseCoords);
        bool mouseClick(const Vector &mouseCoords);
        bool mouseMove(const Vector &mouseCoords);

    private:
        bool isShown(TextButton &button) const;
        bool isClickable(TextButton &button) const;
        
        TextButton m_upArrow, m_downArrow;
        std::vector<TextButton> m_arenaButtons;
        TextButton *m_selectedButton;
        
        ArenaPreviewCallback *m_previewCallback; ///< Callback that will be used for preview of arenas.
        TextureManager *m_textureManager;
        Font *m_font;
        
        unsigned int m_rows;
        
        int m_scrollingDirection;
        float m_achievedMove;
        int m_scrollingLevel;
};

#endif // DEF_ARENA_CHOOSER
