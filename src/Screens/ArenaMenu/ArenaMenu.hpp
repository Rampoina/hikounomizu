/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_ARENA_MENU
#define DEF_ARENA_MENU

#include "Graphics/Window.hpp"

#include "Configuration/Configuration.hpp"

#include "Graphics/Drawable/Sprite.hpp"
#include "Graphics/Drawable/Text.hpp"

#include "Engines/Resources/TextureManager.hpp"
#include "Engines/Resources/FontManager.hpp"

#include "GUI/PlaylistNotifier.hpp"

#include "ArenaChooser.hpp"
#include "ArenaPreviewCallback.hpp"

#include "Tools/Timer.hpp"

#include "Screens/Screen.hpp"

#include <vector>
#include <tinyxml.h>
#include "Tools/BuildValues.hpp" ///Generated at build time

class GameEngine;

class ArenaMenu : public Screen, public ArenaPreviewCallback
{
    public:
        ArenaMenu(GameEngine *gameEngine);
        void previewChanged(unsigned int arenaIx);
        
        //Screen virtual method
        void run();
    
    private:
        void fetchNames(const std::string &xmlRelPath);
        void cleanPreview();
        
        TextureManager m_textureManager;
        FontManager m_fontManager;
        
        std::vector< std::pair<std::string, std::string> > m_arenas; /// Arena name / Arena path name.
        unsigned int m_currentArena;
        
        Arena m_previewArena;
        PhysicsWorld m_previewWorld;
        Box m_previewBox, m_previewTargetBox;
        Polygon m_previewBackground;
        
        GameEngine *m_gameEngine;
        Window *m_window;
        Configuration *m_configuration;
};

#endif
