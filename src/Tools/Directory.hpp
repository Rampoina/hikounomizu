/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_DIRECTORY
#define DEF_DIRECTORY

#include <cstdlib>
#include <sys/stat.h>

#ifdef __HAIKU__
    #include <Path.h>
    #include <Directory.h>
    #include <FindDirectory.h>

#elif _WIN32
    #include <windows.h>
    #include <shlobj.h>

#endif

#include <string>

//Cross-platform class to manage directories
class Directory
{
    public:
        static void create(const std::string &path);
        static std::string getHNMHome(); /// Retrieve Hikou no mizu's home config directory
};

#endif
