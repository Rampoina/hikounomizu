/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEYBOARD_KEY
#define DEF_KEYBOARD_KEY

#include "AbstractKey.hpp"

#include <SDL2/SDL.h>
#include <string>
#include <sstream>

class KeyboardKey : public AbstractKey
{
    public:
        KeyboardKey();
        KeyboardKey(SDL_Scancode keyboardKey);
        
        bool keyPressed(const SDL_Event &event) const;
        bool keyReleased(const SDL_Event &event) const;
        
        std::string getName() const;
        std::string getKeyCode() const;
        
    private:
        SDL_Scancode m_key; //The keyboard key.
};

#endif
