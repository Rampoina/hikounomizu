/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYAXIS_KEY
#define DEF_JOYAXIS_KEY

#include "AbstractJoystickKey.hpp"
#include <SDL2/SDL.h>
#include <sstream>
#include <map>

#define AXIS_THRESHOLD_POS 10923
#define AXIS_THRESHOLD_NEG -10923

enum JoyAxisMove { JOY_AXIS_NEGATIVE, JOY_AXIS_POSITIVE };
enum AxisState { AXIS_STATE_NEGATIVE, AXIS_STATE_CENTERED, AXIS_STATE_POSITIVE };

class JoyAxisKey : public AbstractJoystickKey
{
    public:
        JoyAxisKey();
        JoyAxisKey(const std::string &joystickName, int axisID, JoyAxisMove axisMove);
        
        bool keyPressed(const SDL_Event &event, int deviceID, const std::map<Uint8, AxisState> &previousAxisStates) const;
        bool keyReleased(const SDL_Event &event, int deviceID, const std::map<Uint8, AxisState> &previousAxisStates) const;
        
        std::string getJoystickName() const;
        
        std::string getName() const;
        std::string getKeyCode() const;
        
        static AxisState axisStateFromSDLCode(Sint16 sdlValue);
        
        static JoyAxisMove axisMoveFromStringCode(const std::string &stringCode);
        static std::string axisMoveStringCode(const JoyAxisMove &code);
    
    private:
        std::string m_joystickName;
        int m_axisID;
        JoyAxisMove m_axisMove;
        
};
#endif
