/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_JOYHAT_KEY
#define DEF_JOYHAT_KEY

#include "AbstractJoystickKey.hpp"
#include <SDL2/SDL.h>
#include <sstream>
#include <map>

enum JoyHatMove { JOY_HAT_UP, JOY_HAT_DOWN, JOY_HAT_LEFT, JOY_HAT_RIGHT };

class HatState
{
    public:
        HatState();
        HatState(Uint8 sdlValue);
        
        void fromSDLCode(Uint8 sdlValue);
        
        void centerHat();
        bool centered() const;
        
        void setMove(const JoyHatMove &joyMove, bool value);
        bool state(const JoyHatMove &joyMove) const;
    
    private:
        std::map<JoyHatMove, bool> m_state;
};

class JoyHatKey : public AbstractJoystickKey
{
    public:
        JoyHatKey();
        JoyHatKey(const std::string &joystickName, int hatID, JoyHatMove hatMove);
        
        bool keyPressed(const SDL_Event &event, int deviceID, const std::map<Uint8, HatState> &previousHatStates) const;
        bool keyReleased(const SDL_Event &event, int deviceID, const std::map<Uint8, HatState> &previousHatStates) const;
        
        std::string getJoystickName() const;
        
        std::string getName() const;
        std::string getKeyCode() const;
        
        static bool hatMoveFromSDLCode(Uint8 sdlValue, JoyHatMove &target);
        
        static JoyHatMove hatMoveFromStringCode(const std::string &stringCode);
        static std::string hatMoveStringCode(const JoyHatMove &code);
    
    private:
        std::string m_joystickName;
        int m_hatID;
        JoyHatMove m_hatMove;
        
};
#endif
