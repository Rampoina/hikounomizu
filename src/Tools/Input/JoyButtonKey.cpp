/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JoyButtonKey.hpp"

JoyButtonKey::JoyButtonKey() : AbstractJoystickKey()
{
    
}

JoyButtonKey::JoyButtonKey(const std::string &joystickName, Uint8 joyKey) :
AbstractJoystickKey(), m_joystickName(joystickName), m_joyKey(joyKey)
{
    
}

bool JoyButtonKey::keyPressed(const SDL_Event &event, int deviceID) const
{
    return (event.type == SDL_JOYBUTTONDOWN &&
            deviceID == event.jbutton.which &&
            m_joyKey == event.jbutton.button);
}

bool JoyButtonKey::keyReleased(const SDL_Event &event, int deviceID) const
{
    return (event.type == SDL_JOYBUTTONUP &&
            deviceID == event.jbutton.which &&
            m_joyKey == event.jbutton.button);
}

std::string JoyButtonKey::getJoystickName() const
{
    return m_joystickName;
}

std::string JoyButtonKey::getName() const
{
    std::ostringstream code;
    code << "Button " << static_cast<long>(m_joyKey) + 1;
    return code.str();
}

std::string JoyButtonKey::getKeyCode() const
{
    std::ostringstream code;
    code << "jb" << static_cast<long>(m_joyKey);
    return code.str();
}
