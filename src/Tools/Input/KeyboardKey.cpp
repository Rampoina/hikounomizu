/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "KeyboardKey.hpp"

KeyboardKey::KeyboardKey() : AbstractKey()
{
    
}

KeyboardKey::KeyboardKey(SDL_Scancode keyboardKey) : AbstractKey(), m_key(keyboardKey)
{
    
}

bool KeyboardKey::keyPressed(const SDL_Event &event) const
{
    return event.type == SDL_KEYDOWN && event.key.keysym.scancode == m_key;
}

bool KeyboardKey::keyReleased(const SDL_Event &event) const
{
    return event.type == SDL_KEYUP && event.key.keysym.scancode == m_key;
}

std::string KeyboardKey::getName() const
{
    const char *keyName = SDL_GetKeyName( SDL_GetKeyFromScancode(m_key) );
    if (keyName != NULL)
        return std::string(keyName);
    else
        return "?";
}

std::string KeyboardKey::getKeyCode() const
{
    std::ostringstream code;
    code << "kb" << m_key;
    return code.str();
}
