/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DEF_KEY
#define DEF_KEY

#define DEVICE_KEYBOARD -1
#define KEYBOARD_NAME "keyboard"
#define JOYSTICK_NAME_UNSPECIFIED "joystick"
#define JOYSTICK_NAME_UNDEFINED "undefined"

#include "AbstractKey.hpp"
#include "KeyboardKey.hpp"
#include "JoyButtonKey.hpp"
#include "JoyHatKey.hpp"
#include "JoyAxisKey.hpp"

#include "InputMonitor.hpp"

enum KeyType { KEY_UNDEFINED, KEY_KEYBOARD, KEY_JOYBUTTON, KEY_JOYHAT, KEY_JOYAXIS };

class Key : public AbstractKey
{
    friend class InputMonitor;
    
    public:
        Key();
        Key(KeyboardKey keyboardKey);
        Key(JoyButtonKey joyButtonKey);
        Key(JoyHatKey joyHatKey);
        Key(JoyAxisKey joyAxisKey);
        
        std::string getName() const;
        std::string getKeyCode() const;
        
        bool isFromKeyboard() const;
        bool isFromJoystick() const;
        
        static bool pressedFromEvent(const SDL_Event &event, Key &dstKey, int &dstDeviceID);
        static Key fromKeyCode(const std::string &code, const std::string &deviceName);
        
    private:
        KeyType m_type;
        
        KeyboardKey m_keyboardKey; //The keyboard key.
        JoyButtonKey m_joyButtonKey; //The joystick key.
        JoyHatKey m_joyHatKey; //The joystick hat key.
        JoyAxisKey m_joyAxisKey; //The joystick hat key.
};

#endif
