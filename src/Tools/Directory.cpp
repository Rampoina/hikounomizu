/*Copyright (C) 2010-2021 Duncan Deveaux

This file is part of Hikou no mizu.

Hikou no mizu is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Hikou no mizu is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Hikou no mizu.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Directory.hpp"

void Directory::create(const std::string &path)
{
    #ifdef __unix__
        mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); //chmod 0755
    
    #elif __HAIKU__
        create_directory(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    
    #elif _WIN32
        CreateDirectory(path.c_str(), NULL);
    
    #endif
}

std::string Directory::getHNMHome()
{
    std::string homePath;
    
    #ifdef __unix__
        homePath = std::string(getenv("HOME"));
        homePath += "/.hikounomizu/";
    
    #elif __HAIKU__
        BPath path;
        find_directory(B_USER_SETTINGS_DIRECTORY, &path);
        
        homePath = std::string(path.Path());
        homePath += "/HikouNoMizu/";
    
    #elif _WIN32
        TCHAR szPath[MAX_PATH];
        if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szPath)))
        {
            homePath = std::string(szPath);
            homePath += "\\hikounomizu\\";
        }
    
    #endif
    
    return homePath;
}
